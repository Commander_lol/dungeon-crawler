[Defaults]
Floor = floor
Wall = wall
[Floor]
MultiDir = false
HasAlt = false
[Wall]
MultiDir = true
HasAlt = false
[Placeables]
Num = 0
[Decorations]
Num = 0
[Extras]
HasScripts = false
