﻿Type ClassyButton
Uses drawable, clickable, hoverable
Requires code.handler.ImageLoader
In code.gui

Item inactive Is Image With ImageLoader.getImage("mmButtonMed")
Item active Is Image With ImageLoader.getImage("mmButtonMedHover")
Item down Is Image With ImageLoader.getImage("mmButtonMedDown")



