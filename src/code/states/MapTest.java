package code.states;

import code.Game;

import code.tilegrid.LevelMap;

import code.gui.DebugText;
import code.gui.Drawable;

import code.handlers.FontLoader;
import code.handlers.MusicPlayer;
import code.handlers.Options;

import code.tilegrid.MapPoint;
import code.tilegrid.Pathfinder;
import code.tilegrid.TileOverlay;

import code.tilegrid.lighting.LightColour;
import code.tilegrid.lighting.LightSource;

import code.tilegrid.placeables.BasicTurret;
import code.tilegrid.placeables.LootChest;
import code.tilegrid.placeables.Placeable;
import code.tilegrid.placeables.SimpleTorch;

import java.awt.Point;

import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

import org.newdawn.slick.gui.TextField;

import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

/**
 * @author Commander lol
 */
public class MapTest extends BasicGameState{
    
    private int ID = 0;
    private LevelMap testLevel = null;
    
    private boolean help = false;
    private boolean musicStarted = false;
    private boolean IOVisible = false;
    private boolean lightVisible = false;
    private volatile boolean pathfinderStarted = false;
    private volatile boolean  startSet = false, endSet = false;
    private boolean tileRender = false;
    
    private int[][] lightMap;
    private Color[][] renderColour;
    
    private Point mouseLocation = new Point();
    private Point mouseGridLocation = new Point();
    private Point mapStart = new Point(-1,-1);
    private Point mapEnd = new Point(-1,-1);
    
//    private HashMap<String, LightSource> lightField = new HashMap<>();
    private HashMap<String, Placeable> placeables  =new HashMap<>();
    private HashMap<String, Integer> lightValue = new HashMap<>();
    
    private TextField IONameField;
    
    private volatile Point[] mapPath = null;
    
    private DebugText debugOutput;
    
    private volatile TileOverlay overlay = new TileOverlay(Color.blue);

    public MapTest(int ID) {
        this.ID = ID;
    }
    
    @Override
    public int getID() {return ID;}

    @Override
    public void init(GameContainer gc, StateBasedGame game) throws SlickException {
        
        setupNewLevel();
        
        lightMap = new int[testLevel.getHeight()][testLevel.getWidth(0)];
        renderColour = new Color[testLevel.getHeight()][testLevel.getWidth(0)];
        
        for(int i = 0; i<lightMap.length; i++){
            for(int j = 0; j<lightMap[i].length; j++){
                lightMap[i][j] = 0;
            }
        }
        
        for(int i = 0; i<lightMap.length; i++){
            for(int j = 0; j<lightMap[i].length; j++){
                renderColour[i][j] = new Color(0,0,0,0.9f);
            }
        }
        
        debugOutput = new DebugText();
        IONameField = new TextField(gc, FontLoader.i().getFont("standardGrey"), 100, 100, 300, 50);
        IONameField.setBackgroundColor(Color.gray);
    }

    @Override
    public void render(GameContainer gc, StateBasedGame game, Graphics g) throws SlickException {
        if(tileRender){
            testLevel.render(new Point(0,0), true);
        } else {
            testLevel.debugRender(g);
        }
        
        if(IOVisible){
            IONameField.render(gc, g);
        } else {
            overlay.draw(g, new Point(0,0));
            g.setColor(Color.green);
            g.drawRect(mouseGridLocation.x*32, mouseGridLocation.y*32, 32, 32);
            g.setColor(Color.yellow);
            g.drawRect(mapStart.x*32, mapStart.y*32, 32, 32);
            g.setColor(Color.red);
            g.drawRect(mapEnd.x*32, mapEnd.y*32, 32, 32);
        }
        
        for(Map.Entry<String, Placeable> source : placeables.entrySet()){
            ((Drawable)source.getValue()).draw();
        }
        if(lightVisible){
            for(int i = 0; i<renderColour.length; i++){
                for(int j = 0; j<renderColour[i].length; j++){
                    g.setColor(renderColour[i][j]);
                    g.fillRect(j*32, i*32, 32, 32);
                }
            }
        }
        
        
        debugOutput.draw();
        
        g.setColor(Color.white);
    }

    @Override
    public void update(GameContainer gc, StateBasedGame game, int delta) throws SlickException {
        Point renderOffset = new Point(0,0);
        
        if(!musicStarted){
            MusicPlayer.i().loopMusic("Pick_and_Choose");
            musicStarted = true;
        }
        if(!pathfinderStarted){
            pathfinderStarted = true;
            new Thread(new testPathfinder()).start();
        }
        
        gc.getInput().disableKeyRepeat();
        Input input = gc.getInput();
        
        mouseLocation.x = input.getMouseX();
        mouseLocation.y = input.getMouseY();
        mouseGridLocation = LevelMap.getGridPoint(mouseLocation, renderOffset);
        
        String debugString = "Playing: \"" + testLevel.getName() + "\" Tileset: " + testLevel.getTileset() + " Seed: "+testLevel.getSeed();
        debugOutput.writeToOutput(debugString);
        debugOutput.writeToOutput("Hold 'H' for help");
        
        if(input.isKeyDown(Input.KEY_H)){
            debugOutput.writeToOutput("");
            debugOutput.writeToOutput("3 - Doorway - Brown");
            debugOutput.writeToOutput("2 - Floor - Light Grey");
            debugOutput.writeToOutput("1 - Wall - Dark Grey");
            debugOutput.writeToOutput("0 - Blank Tile - N/A");
            debugOutput.writeToOutput("-= Tiles =-");
            debugOutput.writeToOutput("");
            debugOutput.writeToOutput("ALT + Mouse 2 - Set pathfinding end");
            debugOutput.writeToOutput("ALT + Mouse 1 - Set pathfinding start");
            debugOutput.writeToOutput("");
            debugOutput.writeToOutput("CTRL + S - Save level");
            debugOutput.writeToOutput("CTRL + C - Clear entire level");
            debugOutput.writeToOutput("");
            debugOutput.writeToOutput("[ - Cycle light colour");
            debugOutput.writeToOutput("] - Cycle light colour");
            debugOutput.writeToOutput("R - Remove light source");
            debugOutput.writeToOutput("L - Add light source");
            debugOutput.writeToOutput("Q - Toggle tile rendering");
            debugOutput.writeToOutput("C - Clear current pathfinding");
            debugOutput.writeToOutput("");
            debugOutput.writeToOutput("Esc - Return to menu");
            debugOutput.writeToOutput("Mouse 2 - Cycle tile forwards");
            debugOutput.writeToOutput("Mouse 1 - Cycle tile backwards");
        }
        if(input.isKeyPressed(Input.KEY_C)){
            startSet = false;
            mapStart = new Point(-1, -1);
            endSet = false;
            mapEnd = new Point(-1, -1);
            pathfinderStarted = false;
        }
        if(input.isKeyPressed(Input.KEY_L)){
            if(input.isKeyDown(Input.KEY_LCONTROL)){
                lightVisible = !lightVisible;
            } else {
                String id = "" + LevelMap.getGridPoint(mouseLocation, renderOffset).x + "|" + LevelMap.getGridPoint(mouseLocation, renderOffset).y + "";
                if(!placeables.containsKey(id)){
//                    lightField.put(id, new SimpleTorch(testLevel, LevelMap.getGridPoint(mouseLocation)));
                    placeables.put(id, new SimpleTorch(testLevel, LevelMap.getGridPoint(mouseLocation, renderOffset)));
                }
            }
        }
        if(input.isKeyPressed(Input.KEY_T)){
            String id = "" + LevelMap.getGridPoint(mouseLocation, renderOffset).x + "|" + LevelMap.getGridPoint(mouseLocation, renderOffset).y + "";
            if(!placeables.containsKey(id)){
                placeables.put(id, new BasicTurret(testLevel, LevelMap.getGridPoint(mouseLocation, renderOffset)));
            }
        }
        if(input.isKeyPressed(Input.KEY_B)){
            String id = "" + LevelMap.getGridPoint(mouseLocation, renderOffset).x + "|" + LevelMap.getGridPoint(mouseLocation, renderOffset).y + "";
            if(!placeables.containsKey(id)){
                placeables.put(id, new LootChest(LevelMap.getGridPoint(mouseLocation, renderOffset)));
            }
        }
        if(input.isKeyPressed((Input.KEY_R))){
            String id = "" + LevelMap.getGridPoint(mouseLocation, renderOffset).x + "|" + LevelMap.getGridPoint(mouseLocation, renderOffset).y + "";
//            if(lightField.containsKey(id)){
//                lightField.remove(id);
//            }
            if(placeables.containsKey(id)){
                placeables.remove(id);
            }
        }
        if(input.isKeyPressed(Input.KEY_Q)){
            tileRender = !tileRender;
        }
        if(input.isKeyPressed(Input.KEY_ESCAPE)){
            if(IOVisible){
                IOVisible = false;
            } else {
                musicStarted = false;
                pathfinderStarted = false;
                game.enterState(Game.MainMenuState);
            }
        }
        if(input.isKeyDown(Input.KEY_LCONTROL)){
            if(input.isKeyDown(Input.KEY_S)){
                IOVisible = true;
            } else if(input.isKeyDown(Input.KEY_C)){
                setupNewLevel();
            }
        }
        if(!IOVisible){
            if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)){
                if(input.isKeyDown(Input.KEY_LALT)){
                    if(testLevel.peekAtTileType(mouseGridLocation)<2){
                        startSet = false;
                    } else {
                        startSet = true;
                        mapStart = mouseGridLocation;
                    }
                } else {
                    if(testLevel.peekAtTileType(mouseGridLocation) == 3){
                        testLevel.modifyTileType(mouseGridLocation, Integer.valueOf(0).byteValue());
                    } else {
                        testLevel.modifyTileType(mouseGridLocation, Byte.parseByte(String.valueOf(testLevel.peekAtTileType(mouseGridLocation)+1)));
                    }
                }
            }
            if(input.isMousePressed(Input.MOUSE_RIGHT_BUTTON)){ 
                if(input.isKeyDown(Input.KEY_LALT)){
                    if(testLevel.peekAtTileType(mouseGridLocation)<2){
                        endSet = false;
                    } else {
                        endSet = true;
                        mapEnd = mouseGridLocation;
                    }
                }else{
                    if(testLevel.peekAtTileType(mouseGridLocation) == 0){
                        testLevel.modifyTileType(mouseGridLocation, Integer.valueOf(3).byteValue());
                    } else {
                        testLevel.modifyTileType(mouseGridLocation, Byte.parseByte(String.valueOf(testLevel.peekAtTileType(mouseGridLocation)-1)));
                    }
                }
            }
        }
        
        for(int i = 0; i<renderColour.length; i++){
            for(int j = 0; j<renderColour[i].length; j++){
                renderColour[i][j] = LightColour.Darkness;
            }
        }
        
        byte[][] lvlArray = testLevel.getTypeArray();
        int dist;
        float alpha;
        
        for(Map.Entry<String, Placeable> source : placeables.entrySet()){
            
            if(Placeable.class.isAssignableFrom(source.getValue().getClass())){
                    ((Placeable)source.getValue()).update(testLevel);
            }
            if(LightSource.class.isAssignableFrom(source.getValue().getClass())){
                Color curCol = ((LightSource)source.getValue()).getLightColour();
                dist = ((LightSource)source.getValue()).getLightStrength()/2;
                for (int i = 0; i<renderColour.length; i++){
                    for(int j = 0; j<renderColour[i].length; j++){

                    }
                }
            
//              LightMap curLM = new LightMap(testLevel, light.getPosition(), light.getLightStrength(), light.getLightColour());
//              int[][] valArray = curLM.asValueArray();
//              for(int i = 0; i<valArray.length; i++){
//                  for(int j = 0; j<valArray[i].length; j++){
//                      float alpha = (float)0.025 * valArray[i][j];
//                      Color fogCol = Color.black.addToCopy(curLM.getColour());
//                      fogCol.a = alpha;
//                      renderColour[i+curLM.getLoc().x][j+curLM.getLoc().y] = fogCol;
//                  }
//              }
            }
        }
    }
    
    private void setupNewLevel(){
        pathfinderStarted = false;
        
        startSet = false;
        mapStart = new Point(-1, -1);
        endSet = false;
        mapEnd = new Point(-1, -1);
        
        byte[][] blankLevel = new byte[Options.i().screenHeight()/32][Options.i().screenWidth()/32];
        for(int i = 0; i<blankLevel.length; i++){
            for(int j = 0; j<blankLevel[i].length; j++){
                blankLevel[i][j] = 0;
            }
        }
        
        placeables = new HashMap<>();
        
        testLevel = new LevelMap(Game.SRC+"data/levels/test.lvl");
        testLevel.resetTypeArray(blankLevel);
        testLevel.setName("New Level");
    }
    
    class testPathfinder implements Runnable {

        @Override
        public void run() {
            try{
                while(pathfinderStarted){
                    if(mapStart.getLocation().equals(mapEnd.getLocation())){
                        Point[] volatileArray = new Point[0];
                        mapPath = volatileArray;
                    } else if(startSet && endSet){
                        MapPoint[] tempPoints = Pathfinder.aStarPath(testLevel, mapStart, mapEnd).asArray();
                        Point[] volatileArray = new Point[tempPoints.length];
                        int count = -1;
                        for(MapPoint mp : tempPoints) {
                            count ++;
                            volatileArray[count] = mp.getRenderLocation();
                        }
                        mapPath = volatileArray;
                        overlay.updateOverlay(mapPath);
                    }  
                    if(!pathfinderStarted){
                        Point[] volatileArray = new Point[0];
                        mapPath = volatileArray;
                    }
                }
            } catch(Exception ex){
                pathfinderStarted = false;
            }
        }

    }
}
