package code.states;

import code.Game;
import code.handlers.Options;
import code.handlers.GuiListener;

import code.gui.ClassyButton;
import code.handlers.FontLoader;
import code.handlers.ImageLoader;
import code.handlers.MusicPlayer;
import code.util.BinaryArrayList;
import code.util.BinaryHeap;
import code.util.TestComparator;
import java.awt.Point;
import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.Input;
import org.newdawn.slick.Image;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;

import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import org.newdawn.slick.state.transition.FadeOutTransition;
import org.newdawn.slick.state.transition.FadeInTransition;

/**
 * @author Commander lol
 */
public class MainMenu extends BasicGameState{
    
    private int stateID;
    
    private GuiListener mainMenuGUI;
    
    private boolean musicStarted = false;
    private int tilesLeft = 0;
    private int tilesDown = 0;
    private Image tile;
    
    public MainMenu( int stateID ) {
       this.stateID = stateID;
    }
    
    @Override
    public int getID() {return stateID;}
    
    @Override
    public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
        mainMenuGUI = new GuiListener();
        mainMenuGUI.register(new ClassyButton("start", "New Game", new Point(Options.i().screenWidth()/6, Options.i().screenHeight()/6), new Point(150, 75)));
        mainMenuGUI.register(new ClassyButton("load", "Load Game", new Point((Options.i().screenWidth()/6), (2*(Options.i().screenHeight()/6))), new Point(150, 75)));
        mainMenuGUI.register(new ClassyButton("levelEditor", "Level Editor", new Point((Options.i().screenWidth()/6), (3*(Options.i().screenHeight()/6))), new Point(150, 75)));
        mainMenuGUI.register(new ClassyButton("credits", "Credits", new Point((Options.i().screenWidth()/6), (4*(Options.i().screenHeight()/6))), new Point(150, 75)));
        mainMenuGUI.register(new ClassyButton("exit", "Exit", new Point((Options.i().screenWidth()/6), (5*(Options.i().screenHeight()/6))), new Point(150, 75)));
        
//        mainMenuGUI.modifyElement("start", "updateClickArgs", "transition", new Integer(Game.AdventureState), sbg);
//        mainMenuGUI.modifyElement("load", "updateClickArgs", "transition", 1/*new Integer(Game.LoadGameState)*/, sbg);
//        mainMenuGUI.modifyElement("levelEditor", "updateClickArgs", "transition", new Integer(Game.MapTestState), sbg);
//        mainMenuGUI.modifyElement("credits", "updateClickArgs", "transition", 1/*new Integer(Game.NewGameState)*/, sbg);
        
        BinaryHeap<TestComparator> testHeap = new BinaryHeap<>(10);
        testHeap.add(new TestComparator(15));
        testHeap.add(new TestComparator(75));
        testHeap.add(new TestComparator(37));
        testHeap.add(new TestComparator(102));
        testHeap.add(new TestComparator(10));
        
        tile = ImageLoader.i().getImage("mosaic");
    }
    @Override
    public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
        
        for(int i = 0; i<tilesDown; i++){
            for(int j = 0; j<tilesLeft; j++){
                tile.draw(j*tile.getWidth(), i*tile.getHeight());
            }
        }
//        g.setColor(new Color(71, 58, 56, 0.6f));
//        g.fillRect(0, 0, Options.i().screenWidth(), Options.i().screenHeight());
        
        mainMenuGUI.render();
        
        FontLoader.i().getFont("standardGrey").drawString(100, FontLoader.i().getFont("standardGrey").getLineHeight() + 5, "The Cappa Rex");
                
    }
    @Override
    public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
        
        tilesLeft = 1 + Options.i().screenWidth()/tile.getWidth();
        tilesDown = 1 + Options.i().screenHeight()/tile.getHeight();
        
        if(!musicStarted){
            MusicPlayer.i().loopMusic("Our_8_bit_Ballad");
            musicStarted = true;
        }
        
        Input input = gc.getInput();
        if(((Boolean)mainMenuGUI.modifyElement("start", "hasMouseFocus", new Point(input.getMouseX(), input.getMouseY()))).booleanValue()){
            if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)){
                musicStarted = false;
                sbg.enterState(Game.AdventureState);
            }
        }
        if(((Boolean)mainMenuGUI.modifyElement("levelEditor", "hasMouseFocus", new Point(input.getMouseX(), input.getMouseY()))).booleanValue()){
            if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)){
                musicStarted = false;
                sbg.enterState(Game.MapTestState);
            }
        }
        if(((Boolean)mainMenuGUI.modifyElement("exit", "hasMouseFocus", new Point(input.getMouseX(), input.getMouseY()))).booleanValue()){
            if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)){
                musicStarted = false;
                System.exit(0);
            }
        }
        mainMenuGUI.update(input);
    }
}
