package code.states;

import code.Game;
import code.actors.Player;
import code.generators.genDungeon;
import code.gui.Inventory;
import code.handlers.ItemLoader;
import code.handlers.MusicPlayer;
import code.handlers.Options;
import code.tilegrid.LevelMap;
import code.tilegrid.placeables.LootChest;
import code.tilegrid.placeables.Placeable;
import code.tilegrid.placeables.Useable;

import java.awt.Point;

import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

/**
 * @author Commander lol
 */
public class Dungeon extends BasicGameState{

    private int stateID;
    private LevelMap curLevel = null;
    private int tileWidth, tileHeight = 0;
    private int mapScale = 3;
    private Point renderOffset = new Point(25, 25);
    private boolean debugRendering;
    private boolean displayInventory = false;
    private boolean displayMap = false;
    private boolean musicPlaying;
    private Inventory playerInv = new Inventory();
    
    
    public Dungeon(int stateID){
        this.stateID = stateID;
    }
    
    @Override
    public int getID() {
        return stateID;
    }

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        musicPlaying = false;
        curLevel = new genDungeon(0).get();
        spawnPlayer(true);
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
        g.setBackground(Color.black);
        if(debugRendering){
            curLevel.debugRender(g, renderOffset);
        } else {
            curLevel.render(renderOffset);
        }
        Player.i().draw(renderOffset);
        if(displayInventory){
            playerInv.render(container, game, g);
        }
        if(displayMap){
            curLevel.drawMinimap(g, new Point(Options.i().screenWidth() - 10 - (curLevel.getWidth(0)*mapScale), 0 + 10), renderOffset, mapScale);
        }
    }

    @Override
    public void update(GameContainer gc, StateBasedGame game, int delta) throws SlickException {
        
        if(!musicPlaying){
            MusicPlayer.i().loopMusic("The_Right_to_Live");
            musicPlaying = true;
        }
        
        curLevel.update();
        playerInv.update(gc, game, delta);
        Input input = gc.getInput();
        
        input.enableKeyRepeat();
        
        Point playerLoc = Player.i().getPosition();
        Point mouseLoc = new Point (input.getMouseX(), input.getMouseY());
        Point mouseGridLoc = LevelMap.getGridPoint(mouseLoc, renderOffset);
        Point normalisedMouseLoc = new Point(mouseGridLoc.x * 32, mouseGridLoc.y * 32);
        
        int pX = playerLoc.x, pY = playerLoc.y;
        int sW = (Options.i().screenWidth()/32) + 1;
//        curLevel.modifyTileType(new Point(0,0), Byte.parseByte("1"));
        
        if(input.isKeyDown(Input.KEY_ESCAPE)){
            musicPlaying = false;
            game.enterState(Game.MainMenuState);
        }
        if(input.isKeyDown(Input.KEY_LEFT)){
            renderOffset.translate(-1, 0);
        } 
        if(input.isKeyPressed(Input.KEY_A)){
            movePlayer(1);
        }
        if(input.isKeyDown(Input.KEY_RIGHT)){
            renderOffset.translate(1, 0);
        } 
        if(input.isKeyPressed(Input.KEY_D)){
            movePlayer(3);
        }
        if(input.isKeyDown(Input.KEY_UP)){
            renderOffset.translate(0, -1);
        } 
        if(input.isKeyPressed(Input.KEY_W)){
            movePlayer(0);
        }
        if(input.isKeyDown(Input.KEY_DOWN)){
            renderOffset.translate(0, 1);
        }
        if(input.isKeyPressed(Input.KEY_S)){
            movePlayer(2);
        }
        if(input.isKeyPressed(Input.KEY_R)){
            curLevel = new genDungeon(0).get();
            spawnPlayer(false);
        }
        if(input.isKeyPressed(Input.KEY_F)){
            debugRendering = !debugRendering;
        }
        if(input.isKeyPressed(Input.KEY_G)){
            curLevel.dropItem(ItemLoader.i().getItem("hat", "Top Hat"), mouseGridLoc);
        }
        if(input.isKeyPressed(Input.KEY_I)){
            displayInventory = !displayInventory;
        }
        if(input.isKeyPressed(Input.KEY_M)){
            displayMap = !displayMap;
        }
        if(input.isKeyPressed(Input.KEY_COMMA) && displayMap){
            mapScale --;
        }
        if(input.isKeyPressed(Input.KEY_PERIOD) && displayMap){
            mapScale++;
        }
        if(input.isKeyPressed(Input.KEY_1)){
            Player.i().give(ItemLoader.i().getItem("hat", "Fez"));
        }
        if(input.isKeyPressed(Input.KEY_2)){
            Player.i().give(ItemLoader.i().getItem("hat", "Clown Paint"));
        }
        if(input.isKeyPressed(Input.KEY_3)){
            Player.i().give(ItemLoader.i().getItem("hat", "Top Hat"));
        }
    }
    
    private void movePlayer(int dir){
        Placeable poss;
        switch(dir){
            case 0:
                if(!(curLevel.peekAtTileType(new Point(Player.i().getPosition().x, Player.i().getPosition().y - 1)) < 2)){
                    if((poss = curLevel.objectAtLocation(new Point(Player.i().getPosition().x, Player.i().getPosition().y - 1)))!=null){
                        if(poss.isPassable()){
                            Player.i().move(dir);
                        }
                        if(Useable.class.isAssignableFrom(poss.getClass())){
                            ((Useable)poss).use();
                        }
                    } else {
                        Player.i().move(dir);
                    }
                }
                break;
            case 1:
                if(!(curLevel.peekAtTileType(new Point(Player.i().getPosition().x-1, Player.i().getPosition().y)) < 2)){
                    if((poss = curLevel.objectAtLocation(new Point(Player.i().getPosition().x-1, Player.i().getPosition().y)))!=null){
                        if(poss.isPassable()){
                            Player.i().move(dir);
                        }
                        if(Useable.class.isAssignableFrom(poss.getClass())){
                            ((Useable)poss).use();
                        }
                    } else {
                        Player.i().move(dir);
                    }
                }
                break;
            case 2:
                if(!(curLevel.peekAtTileType(new Point(Player.i().getPosition().x, Player.i().getPosition().y + 1)) < 2)){
                    if((poss = curLevel.objectAtLocation(new Point(Player.i().getPosition().x, Player.i().getPosition().y + 1)))!=null){
                        if(poss.isPassable()){
                            Player.i().move(dir);
                        }
                        if(Useable.class.isAssignableFrom(poss.getClass())){
                            ((Useable)poss).use();
                        }
                    } else {
                        Player.i().move(dir);
                    }
                }
                break;
            case 3:
                if(!(curLevel.peekAtTileType(new Point(Player.i().getPosition().x + 1, Player.i().getPosition().y)) < 2)){
                    if((poss = curLevel.objectAtLocation(new Point(Player.i().getPosition().x + 1, Player.i().getPosition().y)))!=null){
                        if(poss.isPassable()){
                            Player.i().move(dir);
                        }
                        if(Useable.class.isAssignableFrom(poss.getClass())){
                            ((Useable)poss).use();
                        }
                    } else {
                        Player.i().move(dir);
                    }
                }
                break;
        }
    }
    private void spawnPlayer(boolean updateCamera){
        int width = curLevel.getWidth(0);
        int height = curLevel.getHeight();
        Random playerSpawn = new Random(curLevel.getSeed());
        
        int tempX = 0, tempY = 0;
        boolean validSpawn = false;
        
        while(!validSpawn){
            tempX = playerSpawn.nextInt(width-2);
            tempY = playerSpawn.nextInt(height-2);
            
            if(curLevel.peekAtTileType(new Point(tempX, tempY)) == 2){
                validSpawn = true;
            }
        }
        
        Player.i().setLoc(tempX, tempY);
        if(updateCamera){
            Point newOffset = Player.i().getPosition();
            // Add actual calculations to set a proper offset
            renderOffset.setLocation(newOffset);
            renderOffset.translate((-Options.i().screenWidth()/32)/2, (-Options.i().screenHeight()/32)/2);
        }
    }
    
}
