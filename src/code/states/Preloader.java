package code.states;

import code.Game;
import java.io.IOException;

import code.handlers.ImageLoader;
import code.handlers.FileLoader;
import code.handlers.Options;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.loading.DeferredResource;

import org.newdawn.slick.loading.LoadingList;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

/**
 *
 * @author Commander lol
 */
public class Preloader extends BasicGameState{
    
    private int stateID;
    private int resLeft = 0;
    private int resTotal = 0;
    private boolean finishedLoading = false;
    private String amountLoaded = "", curRes = "";
    private Color vibrantGreen = new Color(0, 150, 0);
    
    public Preloader( int stateID ) {
       this.stateID = stateID;
    }
    
    @Override
    public int getID() {return stateID;}
    
    @Override
    public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
        resTotal = LoadingList.get().getTotalResources();
        resLeft = LoadingList.get().getRemainingResources();
    }
    
    @Override
    public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
        if(!finishedLoading){
            g.setBackground(Game.Background);
            g.drawString(amountLoaded, (Options.i().screenWidth()/2) - (g.getFont().getWidth(amountLoaded)/2), (Options.i().screenHeight()/2) - (g.getFont().getHeight(amountLoaded)/2));
            g.drawString(curRes, (Options.i().screenWidth()/2) - (g.getFont().getWidth(curRes)/2), (Options.i().screenHeight()/2) + g.getFont().getHeight(amountLoaded));
        } else {
            g.setBackground(Color.black);
        }
        
    }
    
    @Override
    public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
        
        resLeft = LoadingList.get().getRemainingResources();
//        if(LoadingList.get().getTotalResources()!=0 && LoadingList.get().getRemainingResources()!=0){
//            percLoaded = (int) Math.floor(((LoadingList.get().getTotalResources()-LoadingList.get().getRemainingResources())/LoadingList.get().getTotalResources())*100);
//        } else {
//            percLoaded = 100;
//        }
        amountLoaded = (resTotal - resLeft) + " / " + resTotal + " resources loaded";
        if(LoadingList.get().getRemainingResources()!=0){
            DeferredResource next = LoadingList.get().getNext();
            System.out.println("[PRELOADER] Loading next resource");
            try{
                curRes = next.getDescription();
                next.load();
            } catch (IOException ioe){
                System.err.println(ioe);
            }
        } else {
            
            sbg.enterState(Game.MainMenuState);
        }
    }
}