package code.generators;

import code.actors.Player;
import code.tilegrid.LevelMap;
import code.tilegrid.Pathfinder;
import code.tilegrid.placeables.LootChest;
import code.tilegrid.placeables.Placeable;
import java.awt.Point;
import java.util.ArrayList;

import java.util.Random;
import java.util.Date;
/**
 * The sole purpose of this class is to randomly generate a correctly formatted
 * level object, populated with enemies and items.
 * @author Commander lol
 */
public class genDungeon {
    
    private int xSize, ySize, density;
    private int seed;
    private long runningSeed;
    
    private int tileVoid = 0;
    private int tileWall = 1;
    private int tileFloor = 2;
    private int tileDoor = 3;
    
    private int[] typeMap;
    private ArrayList<Placeable> obs;
    private ArrayList<Room> rooms;
    
    public genDungeon(int seed){
        this.seed = seed;
        runningSeed = seed;
        typeMap = new int[0];
        
        obs = new ArrayList<>();
        rooms = new ArrayList<>();
        
        createDungeon(50, 50, 50);
    }
    
    /**
     * Gets the LevelMap created by this instance of the dungeon generator
     * @return The generated LevelMap
     */
    public LevelMap get(){
        return new LevelMap(getJaggedMap(), obs);
    }
    
    /**
     * Gets a random number between two given values, using a seed that is 
     * updated with every call to this function
     * @param min The lower bound of the number that is to be generated
     * @param max The upper bound of the number that is to be generated
     * @return The generated number
     */
    private int getRand(int min, int max){
        Date now = new Date();
        long tempSeed = now.getTime() + runningSeed;
        runningSeed = tempSeed;

        Random randomizer = new Random(runningSeed);
        int n = max - min;// + 1;
        int i = randomizer.nextInt(n);

        if (i < 0){
            i = -i;
        }
        
        return min + i;
    }
    
    /**
     * Sets the value of the cell at the 2 dimensional coordinates inside the
     * 1 dimensional array
     * @param x The x location, as it would appear on a map
     * @param y The y location, as it would appear on a map
     * @param tileType The new value to set the given cell to
     */
    private void setTile(int x, int y, int tileType){
        typeMap[x + (xSize * y)] = tileType;
    }

    /**
     * Gets the value of the cell at the 2 dimensional coordinates from the
     * one dimensional array
     * @param x The x position of the target tile
     * @param y The x position of the target tile
     * @return The integer representation of the tile at the given location
     */
    private int getTile(int x, int y){
        return typeMap[x + (xSize * y)];
    }
    
    private boolean makeCorridor(int x, int y, int length, int direction){
        int len = getRand(6, length);
        int dir = 0;
        if (direction > 0 && direction < 4) {
            dir = direction;
        }

        int xTemp = 0;
        int yTemp = 0;

        switch(dir){
            case 0://north

                if (x < 0 || x > xSize) {
                    return false;
                } else {
                    xTemp = x;
                }

                for (yTemp = y; yTemp > (y-len); yTemp--){
                    if (yTemp < 0 || yTemp > ySize) {
                        return false;
                    }
                    if (getTile(xTemp, yTemp) != 0) {
                        return false;
                    }
                }

                for (yTemp = y; yTemp > (y-len); yTemp--){
                        setTile(xTemp, yTemp, tileFloor);
                        setTile(xTemp + 1, yTemp, tileWall);
                        setTile(xTemp - 1, yTemp, tileWall);
                }

                break;

            case 1://east

                if (y < 0 || y > ySize) {
                    return false;
                } else {
                    yTemp = y;
                }

                for (xTemp = x; xTemp < (x+len); xTemp++){
                    if (xTemp < 0 || xTemp > xSize) {
                        return false;
                    }
                    if (getTile(xTemp, yTemp) != tileVoid) {
                        return false;
                    }
                }

                for (xTemp = x; xTemp < (x+len); xTemp++){
                    setTile(xTemp, yTemp, tileFloor);
                    setTile(xTemp, yTemp + 1, tileWall);
                    setTile(xTemp, yTemp - 1, tileWall);
                }

                break;

            case 2://south

                if (x < 0 || x > xSize) {
                    return false;
                } else {
                    xTemp = x;
                }

                for (yTemp = y; yTemp < (y+len); yTemp++){
                    if (yTemp < 0 || yTemp > ySize) {
                        return false;
                    }
                    if (getTile(xTemp, yTemp) != tileVoid) {
                        return false;
                    }
                }

                for (yTemp = y; yTemp < (y+len); yTemp++){
                        setTile(xTemp, yTemp, tileFloor);
                        setTile(xTemp - 1, yTemp, tileWall);
                        setTile(xTemp + 1, yTemp, tileWall);
                }

                break;

            case 3://west

                if (yTemp < 0 || yTemp > ySize) {
                    return false;
                } else {
                    yTemp = y;
                }

                for (xTemp = x; xTemp > (x-len); xTemp--){
                    if (xTemp < 0 || xTemp > xSize) {
                        return false;
                    }
                    if (getTile(xTemp, yTemp) != tileVoid) {
                        return false;
                    } 
                }

                for (xTemp = x; xTemp > (x-len); xTemp--){
                    setTile(xTemp, yTemp, tileFloor);
                    setTile(xTemp, yTemp - 1, tileWall);
                    setTile(xTemp, yTemp + 1, tileWall);
                }

                break;
            default:
                return false;
        }
        
        return true;
    }
    
    private boolean makeRoom(int x, int y, int xlength, int ylength, int direction){
        
        int xlen = getRand(8, xlength);
        int ylen = getRand(8, ylength);
        
        int dir = 0;
        if (direction > 0 && direction < 4) {
            dir = direction;
        }

        switch(dir){
            case 0://north

                for (int ytemp = y; ytemp > (y-ylen); ytemp--){
                    if (ytemp < 0 || ytemp > ySize) {
                        return false;
                    }
                    for (int xtemp = (x-xlen/2); xtemp < (x+(xlen+1)/2); xtemp++){
                        if (xtemp < 0 || xtemp > xSize) {
                            return false;
                        }
                        if (getTile(xtemp, ytemp) != tileVoid) {
                            return false;
                        }
                    }
                }

                for (int ytemp = y; ytemp > (y-ylen); ytemp--){
                    for (int xtemp = (x-xlen/2); xtemp < (x+(xlen+1)/2); xtemp++){
                        if (xtemp == (x-xlen/2)) {
                            setTile(xtemp, ytemp, tileWall);
                        } else if (xtemp == (x+(xlen-1)/2)) {
                            setTile(xtemp, ytemp, tileWall);
                        } else if (ytemp == y) {
                            setTile(xtemp, ytemp, tileWall);
                        } else if (ytemp == (y-ylen+1)) {
                            setTile(xtemp, ytemp, tileWall);
                        } else {
                            setTile(xtemp, ytemp, tileFloor);
                        }
                    }
                }
                
                rooms.add(new Room(x-(xlen/2), y-(ylen/2), xlen, ylen));

                break;

            case 1://east
                for (int ytemp = (y-ylen/2); ytemp < (y+(ylen+1)/2); ytemp++){
                    if (ytemp < 0 || ytemp > ySize) {
                        return false;
                    }
                    for (int xtemp = x; xtemp < (x+xlen); xtemp++){
                        if (xtemp < 0 || xtemp > xSize) {
                            return false;
                        }
                        if (getTile(xtemp, ytemp) != tileVoid) {
                            return false;
                        }
                    }
                }

                for (int ytemp = (y-ylen/2); ytemp < (y+(ylen+1)/2); ytemp++){
                    for (int xtemp = x; xtemp < (x+xlen); xtemp++){
                        if (xtemp == x) {
                            setTile(xtemp, ytemp, tileWall);
                        } else if (xtemp == (x+xlen-1)) {
                            setTile(xtemp, ytemp, tileWall);
                        } else if (ytemp == (y-ylen/2)) {
                            setTile(xtemp, ytemp, tileWall);
                        } else if (ytemp == (y+(ylen-1)/2)) {
                            setTile(xtemp, ytemp, tileWall);
                        } else {
                            setTile(xtemp, ytemp, tileFloor);
                        }
                    }
                }
                
                rooms.add(new Room(x-(xlen/2), y-(ylen/2), xlen, ylen));
                
                break;

            case 2://south
                for (int ytemp = y; ytemp < (y+ylen); ytemp++){
                    if (ytemp < 0 || ytemp > ySize) {
                        return false;
                    }
                    for (int xtemp = (x-xlen/2); xtemp < (x+(xlen+1)/2); xtemp++){
                        if (xtemp < 0 || xtemp > xSize) {
                            return false;
                        }
                        if (getTile(xtemp, ytemp) != tileVoid) {
                            return false;
                        }
                    }
                }

                for (int ytemp = y; ytemp < (y+ylen); ytemp++){
                    for (int xtemp = (x-xlen/2); xtemp < (x+(xlen+1)/2); xtemp++){
                        if (xtemp == (x-xlen/2)) {
                            setTile(xtemp, ytemp, tileWall);
                        } else if (xtemp == (x+(xlen-1)/2)) {
                            setTile(xtemp, ytemp, tileWall);
                        } else if (ytemp == y) {
                            setTile(xtemp, ytemp, tileWall);
                        } else if (ytemp == (y+ylen-1)) {
                            setTile(xtemp, ytemp, tileWall);
                        } else {
                            setTile(xtemp, ytemp, tileFloor);
                        }
                    }
                }
                
                rooms.add(new Room(x-(xlen/2), y-(ylen/2), xlen, ylen));

                break;

            case 3://west
                for (int ytemp = (y-ylen/2); ytemp < (y+(ylen+1)/2); ytemp++){
                    if (ytemp < 0 || ytemp > ySize) {
                        return false;
                    }
                    for (int xtemp = x; xtemp > (x-xlen); xtemp--){
                        if (xtemp < 0 || xtemp > xSize) {
                            return false;
                        }
                        if (getTile(xtemp, ytemp) != tileVoid) {
                            return false;
                        } 
                    }
                }

                for (int ytemp = (y-ylen/2); ytemp < (y+(ylen+1)/2); ytemp++){
                    for (int xtemp = x; xtemp > (x-xlen); xtemp--){
                        if (xtemp == x) {
                            setTile(xtemp, ytemp, tileWall);
                        } else if (xtemp == (x-xlen+1)) {
                            setTile(xtemp, ytemp, tileWall);
                        } else if (ytemp == (y-ylen/2)) {
                            setTile(xtemp, ytemp, tileWall);
                        } else if (ytemp == (y+(ylen-1)/2)) {
                            setTile(xtemp, ytemp, tileWall);
                        } else {
                            setTile(xtemp, ytemp, tileFloor);
                        }
                    }
                }
                
                rooms.add(new Room(x-(xlen/2), y-(ylen/2), xlen, ylen));

                break;
            default:
                return false;
        }
        return true;
    }
    
    private Point getLocInRoom(Room r) {
        int newX = getRand(r.getX(), r.getX()+r.getWidth());
        int newY = getRand(r.getY(), r.getY()+r.getHeight());
        
        if(getTile(newX, newY) == 2 ){//&& Pathfinder.limitedPathCheck(getJaggedMap(), new Point(newX, newY+1), new Point(newX, newY-1), 5) && Pathfinder.limitedPathCheck(getJaggedMap(), new Point(newX + 1, newY), new Point(newX - 1, newY), 5)){
            return new Point(newX, newY);
        } else {
            return getLocInRoom(r);
        }
    }
    
    private int[][] getJaggedMap(){
        int[][] typeArray = new int[ySize][xSize];
        for(int i = 0; i<ySize; i++){
            for(int j = 0; j<xSize; j++){
                int curTile = getTile(j, i);
                if(curTile>255){
                    typeArray[i][j] = 0;
                } else {
                    typeArray[i][j] = (curTile);
                }
            }
        }
        return typeArray;
    }
    
    public final boolean createDungeon(int inX, int inY, int inObj){
        if (inObj < 1) {
            density = 10;
        } else {
            density = inObj;
        }

        if (inX < 80){
            xSize = 80;
        } else{
            xSize = inX;
        }

        if (inY < 80) {
            ySize = 80;
        } else {
            ySize = inY;
        }
        
        typeMap = new int[xSize * ySize];
        
        for (int y = 0; y < ySize; y++){
            for (int x = 0; x < xSize; x++){
                if (y == 0) {
                    setTile(x, y, tileWall);
                }
                else if (y == ySize-1) {
                    setTile(x, y, tileWall);
                }
                else if (x == 0) {
                    setTile(x, y, tileWall);
                }
                else if (x == xSize-1) {
                    setTile(x, y, tileWall);
                } else {
                    setTile(x, y, tileVoid);
                }
            }
        }
        
        makeRoom(xSize/2, ySize/2, 10, 10, getRand(0,3));

        int currentFeatures = 1;

        for (int pass = 0; pass < 10000; pass++){
            
            if (currentFeatures == density){
                break;
            }
            
            int newX = 0;
            int xMod = 0;
            int newY = 0;
            int yMod = 0;
            int validTile = -1;
            
            for (int attempt = 0; attempt < 1000; attempt++){
                
                newX = getRand(1, xSize-1);
                newY = getRand(1, ySize-1);
                
                validTile = -1;
                if (getTile(newX, newY) == tileWall || getTile(newX, newY) == tileFloor){
                    if (getTile(newX, newY+1) == tileFloor){
                        validTile = 0;
                        xMod = 0;
                        yMod = -1;
                    } else if (getTile(newX-1, newY) == tileFloor){
                        validTile = 1;
                        xMod = +1;
                        yMod = 0;
                    } else if (getTile(newX, newY-1) == tileFloor){
                        validTile = 2; 
                        xMod = 0;
                        yMod = +1;
                    } else if (getTile(newX+1, newY) == tileFloor){
                        validTile = 3;
                        xMod = -1;
                        yMod = 0;
                    }
                    

                    if (validTile > -1){
                        if (getTile(newX, newY+1) == tileDoor){
                            validTile = -1;
                        } else if (getTile(newX-1, newY) == tileDoor){
                            validTile = -1;
                        } else if (getTile(newX, newY-1) == tileDoor){
                            validTile = -1;
                        } else if (getTile(newX+1, newY) == tileDoor){
                            validTile = -1;
                        }
                    }
                    
                    if (validTile > -1) {
                        break;
                    }
                }
            }
            
            if (validTile > -1){
                int feature = getRand(0, 100);
                
                if (feature <= 75){
                    if (makeRoom((newX+xMod), (newY+yMod), 10, 10, validTile)){
                        currentFeatures++;
                        setTile(newX, newY, tileFloor);
                        setTile((newX+xMod), (newY+yMod), tileDoor);
                    }
                } else {
                    if (makeCorridor((newX+xMod), (newY+yMod), 10, validTile)){
                        currentFeatures++;
                        setTile(newX, newY, tileDoor);
                    }
                }
            }
        }
        
        for(int i = 0; i<xSize; i++){
            for(int j = 0; j<ySize; j++){
                if(getTile(i, j) == 2 || getTile(i, j) == 3){
                    if(getTile(i+1, j) == 0){
                        setTile(i+1, j, 1);
                    }
                    if(getTile(i-1, j) == 0){
                        setTile(i-1, j, 1);
                    }
                    if(getTile(i, j+1) == 0){
                        setTile(i, j+1, 1);
                    }
                    if(getTile(i, j-1) == 0){
                        setTile(i, j-1, 1);
                    }
                }
            }
        }

/*
        
        
        *******************************************************************************
                        Do more work on this part to generate objects
        *******************************************************************************
        */
        
        for(Room r : rooms){
            int chance = getRand(0, 100);
            if (chance <= 101){
                obs.add(new LootChest(getLocInRoom(r)));
            }
        }
        
        /*
        int newx = 0;
        int newy = 0;
        int ways = 0; //from how many directions we can reach the random spot from
        int state = 0; //the state the loop is in, start with the stairs
        while (state != 10){
            for (int testing = 0; testing < 1000; testing++){
                newx = getRand(1, xsize-1);
                newy = getRand(1, ysize-2); //cheap bugfix, pulls down newy to 0<y<24, from 0<y<25

                //System.out.println("x: " + newx + "\ty: " + newy);
                ways = 4; //the lower the better

                //check if we can reach the spot
                if (getCell(newx, newy+1) == tileDirtFloor || getCell(newx, newy+1) == tileCorridor){
                //north
                        if (getCell(newx, newy+1) != tileDoor)
                        ways--;
                }
                if (getCell(newx-1, newy) == tileDirtFloor || getCell(newx-1, newy) == tileCorridor){
                //east
                        if (getCell(newx-1, newy) != tileDoor)
                        ways--;
                }
                if (getCell(newx, newy-1) == tileDirtFloor || getCell(newx, newy-1) == tileCorridor){
                //south
                        if (getCell(newx, newy-1) != tileDoor)
                        ways--;
                }
                if (getCell(newx+1, newy) == tileDirtFloor || getCell(newx+1, newy) == tileCorridor){
                //west
                        if (getCell(newx+1, newy) != tileDoor)
                        ways--;
                }

                if (state == 0){
                        if (ways == 0){
                        //we're in state 0, let's place a "upstairs" thing
                                setCell(newx, newy, tileUpStairs);
                                state = 1;
                                break;
                        }
                }
                else if (state == 1){
                        if (ways == 0){
                        //state 1, place a "downstairs"
                                setCell(newx, newy, tileDownStairs);
                                state = 10;
                                break;
                        }
                }
            }
        }
*/

        return true;
    }
    
    private class Room {

        private int x, y, w, h;
        private String id;
        
        public Room(int x, int y, int w, int h) {
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
            
            id = "" + x + "|" + y + "|" + w + "|" + h + "";
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public int getWidth() {
            return w;
        }

        public int getHeight() {
            return h;
        }
        
        public int getArea() {
            return h*w;
        }
        
        public String getID(){
            return id;
        }
    }
}