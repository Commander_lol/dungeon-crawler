/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package code.generators;

import java.util.Random;
import org.newdawn.slick.Color;

/**
 *
 * @author Commnder lol
 */
public class genItem {
    
    static class Rarity{
        static int getNewRarity(Random levRand){
            int result = levRand.nextInt(1000);
            
            if(result<600){
                return 0;
            } else if(result<800){
                return 100;
            } else if(result<900){
                return 200;
            } else if(result<968){
                return 300;
            } else if(result<988){
                return 400;
            } else if(result<998){
                return 500;
            } else {
                return 600;
            }
        }
        static Color getRarityColour(int rarity){
            if(rarity<100){
                return Color.white;
            } else if(rarity<200){
                return Color.yellow;
            } else if(rarity<300){
                return Color.orange;
            } else if(rarity<400){
                return Color.red;
            } else if(rarity<500){
                return Color.magenta;
            } else if(rarity<600){
                return Color.blue;
            } else{
                return Color.green;
            }
        }
    }
}
