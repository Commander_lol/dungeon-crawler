package code.util;

/**
 * @author Commander lol
 */
public interface Comparable {
    public int getCompareValue();
}
