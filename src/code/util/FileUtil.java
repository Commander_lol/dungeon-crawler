package code.util;

import java.util.regex.*;

/**
 * @author Commander lol
 */
public class FileUtil {
    
    public static boolean isFileType(String fileName, String fileType){
        Pattern filePattern = Pattern.compile(".+" + escapeRegex(fileType) + "$");
        return filePattern.matcher(fileName).matches();
    }
    
    public static String escapeRegex(String regex){
        StringBuilder escapedRegex = new StringBuilder();
        String curChar;
        String[] metachars =  new String[]{ "<",
                                            "(",
                                            "[",
                                            "{",
// backslash should already be escaped     "\\",
                                            "^",
                                            "-",
                                            "=",
                                            "$",
                                            "!",
                                            "|",
                                            "]",
                                            "}",
                                            ")",
                                            "?",
                                            "*",
                                            "+",
                                            ".",
                                            ">"};
        boolean isMatched;
        for(int i = 0; i<regex.length(); i++){
            isMatched = false;
            curChar = String.valueOf(regex.charAt(i));
            for(int j=0; j<metachars.length; j++){
                if(curChar.equals(metachars[j])){
                    isMatched = true;
                    break;
                }
            }
            if(isMatched){
                escapedRegex.append("\\");
            }
            escapedRegex.append(curChar);
        }
        return escapedRegex.toString();
    }
}
