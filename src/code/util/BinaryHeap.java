package code.util;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @author Commander lol
 */
public class BinaryHeap<T extends Comparable>{
    private ArrayList<T> heapArray;
    
    public BinaryHeap(){
        heapArray = new ArrayList<>(1);
        heapArray.add(null);
    }
    public BinaryHeap(int expectedHeapSize){
        heapArray = new ArrayList<>(expectedHeapSize+1);
        heapArray.add(null);
    }
    
    public int add(T newItem){
        if(heapArray.size()<=1){
            heapArray.add(newItem);
            return 1;
        }
        heapArray.add(newItem);
        int curPos = heapArray.size()-1;
        int prevPos = curPos;
        while(curPos!=1){
            prevPos = curPos;
            curPos = Math.round(curPos/2);
            if(newItem.getCompareValue()<heapArray.get(curPos).getCompareValue()){
                Collections.swap(heapArray, curPos, prevPos);
                if(curPos == 1){
                    prevPos = 1;
                }
            } else {
                break;
            }
        }
        heapArray.trimToSize();
        return prevPos;
    }
    public T peek(){
        if(heapArray.size()>1){
            return heapArray.get(1);
        } else {
            return null;
        }
    }
    public T pop(){
        T returnValue;
        if(heapArray.size()>1){
           Collections.swap(heapArray, 1, heapArray.size()-1);
           returnValue = heapArray.remove(heapArray.size()-1);
           int child = 1;
           while(true){
                int parent = child;
                if((2*parent)+1 < heapArray.size()) {
                    if(heapArray.get(parent).getCompareValue() > heapArray.get(parent*2).getCompareValue()){
                        child = child*2;
                    }
                    if(heapArray.get(child).getCompareValue() > heapArray.get(child+1).getCompareValue()){
                        child = child+1;
                    }
                } else if(2*parent < heapArray.size()){
                    if(heapArray.get(parent).getCompareValue() > heapArray.get(parent*2).getCompareValue()){
                        child = child*2;
                    }
                }
               
                if(parent!=child){
                    Collections.swap(heapArray, child, parent);
                } else {
                    heapArray.trimToSize();
                    return returnValue;
                }
           }
        } else {
            return null;
        }
    }
    public boolean isEmpty(){
        return heapArray.isEmpty();
    }
}
