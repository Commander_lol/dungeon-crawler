package code.util;

import java.util.Random;

/**
 * @author Commander lol
 */
public class DicePair {
    
    public static final DicePair d4 = new DicePair(1, 4);
    public static final DicePair d6 = new DicePair(1, 6);
    public static final DicePair d8 = new DicePair(1, 8);
    public static final DicePair d12 = new DicePair(1, 12);
    
    private int noSides, noDie;
    
    public DicePair(int number, int sides){
        noDie = number;
        noSides = sides;
    }

    public int getSides() {
        return noSides;
    }

    public int getDie() {
        return noDie;
    }
    public int rollDie(){
        Random dice = new Random();
        int result = 0;
        
        for(int i = 0; i<noDie; i++){
            result += dice.nextInt(i) + 1;
        }
        
        return result;
    }
    public int rollDie(int diceSeed){
        Random dice = new Random(diceSeed);
        int result = 0;
        
        for(int i = 0; i<noDie; i++){
            result += dice.nextInt(i) + 1;
        }
        
        return result;
    }
    public int rollDie(Random preset){
        int result = 0;
        
        for(int i = 0; i<noDie; i++){
            result += preset.nextInt(i) + 1;
        }
        
        return result;
    }
        
    @Override
    public String toString(){
        return noDie + "d" + noSides;
    }
    
}
