package code.util;

import java.awt.Point;

/**
 * A utility class that creates and reverses cantor pairs of numbers; used to 
 * encode two integers into a single integer
 * @author Commander lol
 */
public class CantorPairing {
    
    public static int getPairing(int x, int y){
        return ((x + y) * (x + y + 1)) / 2 + y;
    }
    
    public static int[] reversePairing(int cantorInt){
        int[] pair = new int[2];
        
        int t = (int)Math.floor((-1 + Math.sqrt(1 + 8 * cantorInt))/2);
        int x = t * (t + 3) / 2 - cantorInt;
        int y = cantorInt - t * (t + 1) / 2;
        
        pair[0] = x;
        pair[1] = y;
        
        return pair;
    }
    public static Point reversePairingToPoint(int cantorInt){
        Point pair;
        
        int t = (int)Math.floor((-1 + Math.sqrt(1 + 8 * cantorInt))/2);
        int x = t * (t + 3) / 2 - cantorInt;
        int y = cantorInt - t * (t + 1) / 2;
        
        pair = new Point(x, y);
        
        return pair;
    }
}
