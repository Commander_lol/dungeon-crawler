/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package code.util;

import code.util.Comparable;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collection;

/**
 * An array based list that will sort items upon insertion and check order 
 * integrity on removal. The internal storage array will scale depending on the
 * number of elements inserted into it; every time the capacity is reached, it 
 * will double the number of available spaces
 * @author Commander lol
 */
public class BinaryArrayList <T extends Comparable> implements Serializable{
    /**
     * The array in which everything is stored
     */
    private ArrayList<T> internalArray;
    
    public BinaryArrayList(){
        internalArray = new ArrayList<>();
//        internalArray.add(null);
    }
    public BinaryArrayList(int initialSize){
        internalArray = new ArrayList<>(initialSize+1);
//        internalArray.add(null);
        
    }
    
    public void put(T item){
        if(internalArray.isEmpty()){
            internalArray.add(item);
            debugPrint();
        }else{
            internalArray.add(putRecursion(internalArray, item, 0, internalArray.size())-1, item);
            debugPrint();
        }
    }
    /**
     * A tail recursive binary search used to find where to put things into this list
     * @param array The array version of this array list being searched
     * @param item The item being placed into the array
     * @param min The minimum boundary value of the section being searched
     * @param max The maximum boundary value of the section being searched
     * @return The location to put the item into
     */
    private int putRecursion(ArrayList<T> array, T item, int min, int max){
        System.out.println("Min: " + min + " Max: " + max + " Current Value: " + item.getCompareValue());
        if (min >= max){
            return -1;
        }
        int middle = min + ((max-min) / 2);
        if (array.get(middle).getCompareValue() == item.getCompareValue()){
              return middle;
        } else if (array.get(middle).getCompareValue() > item.getCompareValue()){
              return putRecursion(array, item, min, middle);
        } else {
              return putRecursion(array, item, middle+1, max);
        }
    }
    
    public void putAll(Collection<T> C){
        for(T item : (T[])C.toArray()){
            put(item);
        }
    }
    /**
     * Gets the object at the specified index and returns it
     * @param index
     * @return 
     */
    public T get(int index){return internalArray.get(index);}
    public boolean remove(int index){
        internalArray.remove(index);
        return true;
    }
    public T pull(int index){return internalArray.remove(index);}
    
    private int BinarySearch(T item) throws Exception {
        int min = 0;
        int max = internalArray.size()-1;
        int mid, curVal;
        while (max >= min){
            mid = min+((max-min)/2);
            curVal = internalArray.get(mid).getCompareValue();
            if(curVal > item.getCompareValue()){
                max = mid-1;
            } else if(curVal < item.getCompareValue()){
                min = mid+1;
            } else {
                return mid;
            }
        }
        throw new Exception("Something went wrong here");
    }
    private void debugPrint(){
        ArrayList<T> valArray = (ArrayList<T>)internalArray.clone();
        valArray.trimToSize();        
        
        for(int i = 1; i<valArray.size()-1; i++){
            System.out.print(valArray.get(i).getCompareValue() + ", ");
        }
        System.out.println(valArray.get(valArray.size()-1).getCompareValue());
    }
}
