grammar PigscriptEvent;

tokens{
	THIS = 'this';
	BECOMES = 'becomes';
	AT = 'at';
	TARGETS = 'targets';
	NEAREST = 'nearest';
	FURTHEST = 'furthest';
	RANDOM = 'random';
	VISIBLE = 'visible';
	EVENT = 'EVENT';
	END = 'END';
}

DIGIT 	
	:	'0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9';
CHARACTER 
	:	UPPERCHAR | LOWERCHAR;
LOWERCHAR 
	:	'a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'g' | 'h' | 'i' | 'j' | 'k' | 'l' | 'm' | 'n' | 'o' | 'p' | 'q' | 'r' | 's' | 't' | 'u' | 'v' | 'w' | 'x' | 'y' | 'z';
UPPERCHAR 
	: 	'A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'H' | 'I' | 'J' | 'K' | 'L' | 'M' | 'N' | 'O' | 'P' | 'Q' | 'R' | 'S' | 'T' | 'U' | 'V' | 'W' | 'X' | 'Y' | 'Z';
WSSL 	
	:	(' ' | '\t')+;
NL 	
	:	'\n'+;
WS 	
	:	(WSSL | NL)+;
TERM 	
	:	LOWERCHAR (CHARACTER)*;
IDENTIFIER 
	:	TERM | THIS;
METHODBODY 
	: ;
statement 
	:	IDENTIFIER WSSL TARGETS WSSL (IDENTIFIER WSSL| (NEAREST | FURTHEST | RANDOM) WSSL (VISIBLE WSSL)?);
definition 
	:	EVENT WSSL TERM NL
		(statement NL)+ 
		END;
program 
	:	(definition NL)+
		EOF;