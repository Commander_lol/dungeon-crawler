package code.gui;

import code.handlers.FontLoader;
import java.awt.Point;

import code.handlers.ImageLoader;
import code.handlers.MusicPlayer;
import org.newdawn.slick.AngelCodeFont;

import org.newdawn.slick.Image;
import org.newdawn.slick.state.StateBasedGame;

/**
 *
 * @author Commander lol
 */
public class ClassyButton implements Clickable, Drawable{
    
    private Point position, dimensions;
    private String content, ID;
    private StringBuilder tempContent;
    private Image button, buttonHover, buttonActive;
    private Image curImage;
    private AngelCodeFont defaultFont;
    private Object[] clickArgs = new Object[0];
    private boolean playedSound;
    
    
    /**
     * Create a new button in the ClassyButton style, with text that will be 
     * wrapped and shrunk to fit onto the button, and look classy afterwards
     * @param words 
     */
    public ClassyButton(String ID, String words, Point location, Point dimensions){
        this.ID = ID;
        position = location;
        button = ImageLoader.i().getImage("mmButtonMed");
        buttonHover = ImageLoader.i().getImage("mmButtonMedHover");
        buttonActive = ImageLoader.i().getImage("mmButtonMedDown");
        content = words;
        curImage = button;
        defaultFont = FontLoader.i().getFont("standardGrey");
        playedSound = false;
    }
    
    @Override
    public String toString(){
        return ID;
    }
    
    @Override
    public boolean draw() {
        curImage.draw(position.x, position.y);
        defaultFont.drawString(position.x + 10,position.y + 10,content);/*position.x+((dimensions.x-defaultFont.getWidth(content)/2)), position.y+((dimensions.y-defaultFont.getHeight(content)/2)), content);*/
        return true;
    }
    @Override
    public boolean draw(Point pos) {
        curImage.draw(pos.x, pos.y);
        defaultFont.drawString(pos.x+((dimensions.x-defaultFont.getWidth(content)/2)), pos.y+((dimensions.y-defaultFont.getHeight(content)/2)), content);
        return true;
    }
    @Override
    public boolean draw(Point subPos, int subWidth, int subHeight) {
        curImage.getSubImage(subPos.x, subPos.y, subWidth, subHeight).draw(position.x, position.y);
        defaultFont.drawString(position.x+((dimensions.x-defaultFont.getWidth(content)/2)), position.y+((dimensions.y-defaultFont.getHeight(content)/2)), content);
        return true;
    }

    @Override
    public boolean draw(Point pos, Point subPos, int subWidth, int subHeight) {
        curImage.getSubImage(subPos.x, subPos.y, subWidth, subHeight).draw(pos.x, pos.y);
        defaultFont.drawString(pos.x+((dimensions.x-defaultFont.getWidth(content)/2)), pos.y+((dimensions.y-defaultFont.getHeight(content)/2)), content);
        return true;
    }

    @Override
    public int getHeight() {
        return curImage.getHeight();
    }

    @Override
    public int getWidth() {
        return curImage.getWidth();
    }

    @Override
    public Point getPosition() {
        return position;
    }

    @Override
    public void hover(Object... args) {
        if(curImage != buttonHover){
            curImage = buttonHover;
        }
        if(!playedSound){
            MusicPlayer.i().playSound("select");
            playedSound = true;
        }
    }

    @Override
    public void mouseIn(Object... args) {
        curImage = buttonHover;
    }

    @Override
    public void mouseOut(Object... args) {
        curImage = button;
        playedSound = false;
    }

    @Override
    public boolean hasMouseFocus(Point mouseLoc) {
        int thisRightEdge = position.x + curImage.getWidth();
        int thisBottomEdge = position.y + curImage.getHeight();
        
        if(mouseLoc.x > thisRightEdge || mouseLoc.x < position.x || mouseLoc.y > thisBottomEdge || mouseLoc.y < position.y){
            return false;
        } else {
            return true;
        }
    }
    
    @Override
    public void mouseDown(Object... args){
        curImage = buttonActive;
    }
    @Override
    public void click(){
        try{
            if(clickArgs.length<1){
                System.out.println("[Classy Button - " + ID + "] No click args found");
            } else {
                System.out.println("[Classy Button - " + ID + "] Clicking button with " + clickArgs.length + " click args");
                click(clickArgs);
            }
        }catch(NullPointerException npe){
            System.out.println("[Classy Button - " + ID + "] No click args found, possible NPE");
        }
        
    }
    @Override
    public void click(Object... args){
        System.out.print("[Classy Button - " + ID + "] Clickedy click " + args.length + ": ");
        for(int i = 0; i<args.length; i++){
            System.out.print("[" + args[i].getClass().getSimpleName() + "; " + args[i].toString() + "], ");
        }
        System.out.println();
        if(String.class.isAssignableFrom(args[0].getClass())){
            System.out.println("[[ IS AN STRING ]]");
            switch(((String)args[0])){
                case "transition":
                    System.out.println("[[ IS AN TRANSITION ]]");
                    if(Integer.class.isAssignableFrom(args[1].getClass())){
                        System.out.println("[[ IS AN INTEGER ]]");
                        if(StateBasedGame.class.isAssignableFrom(args[2].getClass())){
                            System.out.println("[[ IS AN SBG ]]");
                            ((StateBasedGame)args[2]).enterState(((Integer)args[1]).intValue());
                        }
                    }
            }
        }
    }
    @Override
    public void updateClickArgs(Object... args){
        if(args.length>0){
            System.out.println("[ClassyButton - "+ID+"] Updating click function");
            clickArgs = args;
        } else {
            System.out.println("[ClassyButton - "+ID+"] No args given to update click function");
        }
    }
}
