package code.gui;

/**
 * @author Commander lol
 */
interface Droppable {
    public void release(Draggable drag);
}
