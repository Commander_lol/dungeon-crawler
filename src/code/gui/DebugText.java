package code.gui;

import code.handlers.FontLoader;
import code.handlers.Options;
import java.awt.Point;
import java.util.ArrayDeque;
import org.newdawn.slick.AngelCodeFont;

/**
 * @author Commander lol
 */
public class DebugText implements Drawable{
    
    private ArrayDeque<String> textQueue;    
    private static AngelCodeFont font = FontLoader.i().getFont("debug");
    
    
    public DebugText(){
        textQueue = new ArrayDeque<>();
    }
    
    public void writeToOutput(String newText){
        textQueue.push(newText);
    }
    @Override
    public int getWidth() {
        return Options.i().screenWidth();
    }

    @Override
    public int getHeight() {
        return font.getLineHeight()*textQueue.size();
    }

    @Override
    public Point getPosition() {
        return new Point(0,Options.i().screenHeight()-getHeight());
    }

    @Override
    public boolean draw() {
        int textLength = textQueue.size();
        for(int i=0; i<textLength; i++){
            font.drawString(0, Options.i().screenHeight()-getHeight(), textQueue.pop());
        }
        return true;
    }

    @Override
    public boolean draw(Point pos) {
        return draw();
    }

    @Override
    public boolean draw(Point subPos, int subWidth, int subHeight) {
        return draw();
    }

    @Override
    public boolean draw(Point pos, Point subPos, int subWidth, int subHeight) {
        return draw();
    }
    
    
    
}
