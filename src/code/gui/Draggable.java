package code.gui;

import java.awt.Point;

/**
 * @author Commander lol
 */
public interface Draggable extends Hoverable{
    public void startDrag(Point mouseLocation);
    public void attachToMouse(Point mouseLocation);
    public void stopDrag(Point mouseLocation);
    public boolean isDragged();
}
