package code.gui;

import java.awt.Point;

/**
 * @author Commander lol
 */
public class SimpleButton implements Drawable, Clickable{

    
    
    public SimpleButton() {
    }

    @Override
    public int getWidth() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getHeight() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Point getPosition() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean draw() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean draw(Point pos) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean draw(Point subPos, int subWidth, int subHeight) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean draw(Point pos, Point subPos, int subWidth, int subHeight) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseDown(Object... args) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateClickArgs(Object... args) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void click() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void click(Object... args) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void hover(Object... args) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseIn(Object... args) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseOut(Object... args) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean hasMouseFocus(Point mouseLoc) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}