package code.gui;

import java.awt.Point;

/**
 *
 * @author Commander lol
 */
public interface Hoverable {
    /**
     * The function to execute when the mouse is hovering over the object
     * @param args The parameters that the function is expecting. Note that
     * each function will need run time validation to ensure that the correct
     * arguments are present
     */
    public void hover(Object... args);
    /**
     * The function to execute when the mouse moves onto the object
     * @param args The parameters that the function is expecting. Note that
     * each function will need run time validation to ensure that the correct
     * arguments are present
     */
    public void mouseIn(Object... args);
    /**
     * The function to execute when the mouse moves off of the object
     * @param args The parameters that the function is expecting. Note that
     * each function will need run time validation to ensure that the correct
     * arguments are present
     */
    public void mouseOut(Object... args);
    /**
     * Checks to see if the mouse is hovering over the item, using an already
     * known position for the mouse. Will not take into account visibility or
     * z-index of this or other objects in the same location
     * @param mouseLoc The present location of the mouse
     * @return Whether or not the object is currently underneath the mouse
     * cursor
     */
    public boolean hasMouseFocus(Point mouseLoc);
}
