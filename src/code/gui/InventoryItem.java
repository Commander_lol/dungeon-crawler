package code.gui;

import java.awt.Point;

/**
 * @author Commander lol
 */
public class InventoryItem implements Drawable, Draggable{

    @Override
    public int getWidth() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getHeight() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Point getPosition() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean draw() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean draw(Point pos) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean draw(Point subPos, int subWidth, int subHeight) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean draw(Point pos, Point subPos, int subWidth, int subHeight) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void startDrag(Point mouseLocation) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void attachToMouse(Point mouseLocation) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void stopDrag(Point mouseLocation) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isDragged() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void hover(Object... args) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseIn(Object... args) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseOut(Object... args) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean hasMouseFocus(Point mouseLoc) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
