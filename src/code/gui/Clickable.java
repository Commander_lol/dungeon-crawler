package code.gui;

/**
 * An interface for any GUI elements that perform an action upon being clicked
 * @author Commander lol
 */
public interface Clickable extends Hoverable{
    
    /**
     * The actions to execute when the mouse is pressed down on this object.
     * This action is different to the click action, as the mouse may be moved
     * off of this Clickable before it is released, which should cancel anything
     * that would otherwise happen
     * @param args The parameters that the function is expecting. Note that
     * each function will need run time validation to ensure that the correct
     * arguments are present
     */
    public void mouseDown(Object... args);
    /**
     * Updates an internal array of things to do when the object is clicked
     * @param args The args to set the internal storage to
     */
    public void updateClickArgs(Object... args);
    /**
     * The code to execute when the object is clicked. This can use an internal
     * args list or can execute a fixed code every time
     */
    public void click();
    /**
     * The code to execute when the object is clicked. Can be anything from a
     * transition to updating the gui or affecting game elements
     * @param args The parameters that the function is expecting. Note that
     * each function will need run time validation to ensure that the correct
     * arguments are present
     */
    public void click(Object... args);
}
