package code.gui;

import org.newdawn.slick.AngelCodeFont;
import org.newdawn.slick.Image;

/**
 *
 * @author Commander lol
 */
public class ImageButton {
    
    private Image button, buttonHover, buttonActive;
    private Image curImage;
    
    private boolean hasText = false;
    private String content = null;
    private AngelCodeFont defaultFont = null;
    
}
