package code.gui;

import code.Game;
import code.actors.Player;
import code.handlers.Options;
import code.items.Buff;
import code.items.Item;
import code.items.Modifier;
import code.tilegrid.lighting.RarityColour;
import java.awt.Point;

import java.util.ArrayList;
import org.newdawn.slick.Color;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.ShapeFill;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.fills.GradientFill;
import org.newdawn.slick.geom.Rectangle;

import org.newdawn.slick.state.StateBasedGame;

/**
 * @author Commander lol
 */
public class Inventory{

    private int maxEq;
    private Item[] contents;
    private ArrayList<Item> equipped;
    private int invOffset = 100;
    private int primeSelected = Integer.MAX_VALUE, selectedSlot = Integer.MAX_VALUE;
    
    public Inventory(){    }
    public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
        
        Color origCol = g.getColor();
        Color grayGrey = new Color(200,200,200);
        
        int width = 4, curHeight = 0, curWidth = 0;
//        GradientFill invGrad = new GradientFill(0, -16, Color.gray, 0, 16, Color.darkGray);
        
        g.setColor(grayGrey);
        g.fillRect(invOffset - 8, invOffset - 8, 240, 198);
        g.setColor(Color.black);
        g.drawRect(invOffset - 8, invOffset - 8, 240, 198);
        
        for (int i = 0; i<Player.i().inventorySize(); i++){
                
//            Rectangle curRect = new Rectangle(((curWidth * 2)*32) + invOffset, invOffset + ((curHeight * 2) * 32), 32, 32);
            
            g.setColor(Color.gray);
            g.fillRect(((curWidth * 2)*32) + invOffset, (invOffset + ((curHeight * 2) * 32)), 32, 16);
            g.setColor(Color.darkGray);
            g.fillRect(((curWidth * 2)*32) + invOffset, (invOffset + 16 + ((curHeight * 2) * 32)), 32, 16);
            if(primeSelected == i){
                g.setColor(Color.green);
            } else {
                g.setColor(Color.black);
            }
            g.drawRect(invOffset + ((curWidth * 2)*32), (invOffset + ((curHeight * 2) * 32)), 32, 32);
            
            curWidth ++;
            if(curWidth == 4){
                curWidth = 0;
                curHeight ++;
            }
        }
        
        g.setColor(Color.gray);
        g.fillRect(invOffset + 296, invOffset, 32, 16);
        g.setColor(Color.darkGray);
        g.fillRect(invOffset + 296, invOffset + 16, 32, 16);
        
        if(primeSelected == 12){
            g.setColor(Color.green);
        } else {
            g.setColor(Color.black);
        }
        g.drawRect(invOffset + 296, invOffset, 32, 32);
        
        
        int invX = 0, invY = 0;
        for(Item i : Player.i().getInv()){
            if(i!=null){
                i.drawIcon(new Point((invX * 2)*32 + invOffset, (invY * 2)*32 + invOffset));
            }
            invX++;
            if(invX == 4){
                invX = 0;
                invY ++;
            }
        }
        if(Player.i().getEquipped()!=null){
            Player.i().getEquipped().drawIcon(new Point(296 + invOffset, invOffset));
        }
        
        if(selectedSlot!=Integer.MAX_VALUE && selectedSlot!=12){
            if(Player.i().inventorySpace()>selectedSlot){
                renderToolTip(g, gc.getInput(), selectedSlot);
            }
        }
        
        g.setColor(origCol);
        
    }
    
    public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
        Input input = gc.getInput();
        
        selectedSlot = invSelection(new Point(input.getMouseX(), input.getMouseY()));
        
        if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)){
            if(selectedSlot == Integer.MAX_VALUE || primeSelected == Integer.MAX_VALUE){
                primeSelected = selectedSlot;
            } else if(primeSelected!=selectedSlot){
                if(primeSelected<12 && selectedSlot!=12){
                    Player.i().switchInvPos(primeSelected, selectedSlot);
                    primeSelected = Integer.MAX_VALUE;
                } else if (primeSelected == 12){
                    Item temp = Player.i().discard(selectedSlot);
                    Player.i().equip(temp, true);
                    primeSelected = Integer.MAX_VALUE;
                } else if (selectedSlot == 12){
                    Item temp = Player.i().discard(primeSelected);
                    Player.i().equip(temp, true);
                    primeSelected = Integer.MAX_VALUE;
                }
            }
        }
    }
    
    public void renderToolTip(Graphics g, Input input, int invSlot){
        Color tooltipGrey = new Color(50,50,50);
        Point mouseLoc = new Point(input.getMouseX(), input.getMouseY());
        
        
        if(Player.i().getInv()[invSlot] != null){
            int x = invSlot % 4;
            int y = invSlot / 4;
            
            Item curItem = Player.i().getInv()[invSlot];
            
            g.setColor(tooltipGrey);
            g.fillRect(mouseLoc.x + 10, mouseLoc.y + 10, 250, 75);
            
            g.setColor(RarityColour.get(curItem.getRarity()));
            g.drawString(curItem.getPrefix() + " " + curItem.getName() + " " + curItem.getSuffix(), mouseLoc.x + 12, mouseLoc.y + 12);
            g.setColor(Color.white);
            Modifier[] mods = curItem.getBuffs();
            int count = 1;
            for(Modifier m : mods){
                for(Buff b : m.getEffects()){
                    g.drawString(b.getFlatBonus() + " + " + b.getVarBonus().toString() + " " + b.getStat(), mouseLoc.x + 14, mouseLoc.y + 14 + (g.getFont().getLineHeight() * count));
                    count++;
                }
            }
        }
    }
    
    public int invSelection (Point mouseLoc){
        
        int x = mouseLoc.x, y = mouseLoc.y;
        int select = Integer.MAX_VALUE;
        
        if(x<invOffset || x>invOffset + 320 || y<invOffset || y>invOffset + 192){
            return select;
        }
        
        int invX = 0, invY = 0;
        
        if(x >= 296 + invOffset && x<= 328 + invOffset){
            if( y >= invOffset && y <= invOffset + 32){
                select = 12;
            }
        } else {
            for(int i = 0; i<Player.i().inventorySize(); i++){
                if(x >= invOffset + ((invX * 2)*32) && x <= 32 + invOffset + ((invX * 2)*32)){
                    if(y >= invOffset + ((invY * 2)*32) && y <= 32 + invOffset + ((invY * 2)*32)){
                        select = i;
                        break;
                    }
                }

                invX ++;
                if(invX == 4){
                    invY++;
                    invX = 0;
                }
            }
        }
        return select;
    }
    
}
