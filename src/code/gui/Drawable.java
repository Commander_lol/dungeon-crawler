package code.gui;

import java.awt.Point;
/**
 * An interface for any GUI elements that are drawn to the screen
 * @author Commander lol
 */
public interface Drawable {
    
    /**
     * Gets the width of the drawable object
     * @return The given value for width
     */
    public int getWidth();
    /**
     * Gets the height of the drawable object
     * @return The given value for height
     */
    public int getHeight();
    /**
     * Gets the position of the drawable object
     * @return The given value for position
     */
    public Point getPosition();
    
    /**
     * Draws the object at a predetermined position, usually an internally 
     * stored position that can be modified unless the object is intended to be
     * permanently static.
     * @return Whether or not the object was drawn successfully
     */
    public boolean draw();
    /**
     * Draws the object at a give position on the screen, on top of any 
     * existing objects that have already been drawn
     * @param pos The position at which to draw the object
     * @return Whether or not the object was drawn successfully
     */
    public boolean draw(Point pos);
    /**
     * Draws a sub-image of the drawable object at a predetermined position, 
     * usually an internally stored position that can be modified unless the 
     * object is intended to be permanently static.
     * @param subPos The position of the sub-image within the drawable object's
     * image
     * @param subWidth The width of the sub-image to draw
     * @param subHeight The height of the sub-image to draw
     * @return  Whether or not the sub-image was drawn successfully
     */
    public boolean draw(Point subPos, int subWidth, int subHeight);
    /**
     * Draws a sub-image of the drawable object at a predetermined position, 
     * usually an internally stored position that can be modified unless the 
     * object is intended to be permanently static.
     * @param pos The position to draw the object
     * @param subPos The position of the sub-image within the drawable object's
     * image
     * @param subWidth The width of the sub-image to draw
     * @param subHeight The height of the sub-image to draw
     * @return  Whether or not the sub-image was drawn successfully
     */
    public boolean draw(Point pos, Point subPos, int subWidth, int subHeight);
    
}
