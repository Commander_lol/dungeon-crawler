package code;

import code.gui.Inventory;
import code.handlers.*;

import java.util.Random;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.AppGameContainer;

import org.newdawn.slick.state.StateBasedGame;

import org.newdawn.slick.loading.LoadingList;

import code.states.*;
import java.util.HashMap;
import org.newdawn.slick.Color;
import org.newdawn.slick.state.GameState;
/**
 * A small private class that provides a title to the game window. The main 
 * purpose is to prevent cluttering of the {@link Game Game} class.
 * @author Commander lol
 */
class Title{
    private static Random rand = new Random();
    private static String[] array = {
        "This is a game",
        "100% Pre-Alpha",
        "Dungeon Crawlin'"
    // More titles go here
    };

    public static String Rand(){
        String title = array[(rand.nextInt(array.length))];
        return title;
    }
}

/**
 * The main game class, it contains important things such as state constants 
 * and global flags. The program also starts here, and the slick library is
 * initialised by it.
 * @author Commander lol
 */
public class Game extends StateBasedGame {
    
    public static final int FrameRate = 60;
    
    /**
     * The additional folder string that needs to be attached to file paths when
     * testing the program without fully building it
     */
    public static final String SRC = "src/";
    public static final Color Background = new Color(71, 58, 56);
    
    public static HashMap<String, GameState> states = new HashMap<>();
    
    public static final int PreLoadState            = 0;
    public static final int MainMenuState           = 1;
    public static final int NewGameState            = 2;
    public static final int LoadGameState           = 3;
    public static final int CharacterCreateState    = 4;
    public static final int AdventureState          = 5;
    public static final int InventoryState          = 11;
    public static final int OptionMenuState         = 10;
    public static final int MapTestState            = 404;
    
    public Game(){
        super("The Cappa Rex - " + Title.Rand());
        
        LoadingList.setDeferredLoading(true);
        
        states.put("MainMenu", new MainMenu(MainMenuState));
        states.put("Preloader", new Preloader(PreLoadState));
        states.put("LevelEditor", new MapTest(MapTestState));
        states.put("Adventure", new Dungeon(AdventureState));
        
        this.addState(states.get("MainMenu"));
        this.addState(states.get("Preloader"));
        this.addState(states.get("LevelEditor"));
        this.addState(states.get("Adventure"));
        
        this.enterState(PreLoadState);
    }
    
    /**
     * This is the main entry point for the application. It is run when the
     * program starts, and sets up the window with the appropriate presets 
     * before launching the game proper.
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SlickException {
        AppGameContainer app = new AppGameContainer(new Game());
        
        app.setTargetFrameRate(FrameRate);
        app.setDisplayMode(Options.i().screenWidth(), Options.i().screenHeight(), Options.i().fullscreen());
        app.setVSync(true);
        //app.setIcons(new String[]{"Data/Icons/icon16.png", "Data/Icons/icon32.png"});
        
        app.start();
    }
    
    @Override
    public void initStatesList(GameContainer gameContainer) throws SlickException {
        Options.i();
        ImageLoader.i();
        FontLoader.i();
        MusicPlayer.i();
        BuffLoader.i();
        ItemLoader.i();
    }
}
