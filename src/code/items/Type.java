package code.items;

import java.util.ArrayList;

/**
 * All of the elemental types, their values and their strengths/weaknesses. 
 * Each damage type has certain relationships with other types of damage:
 * <dl>
 *  <dt>No Relationship</dt>
 *  <dd>There is no special relation between these two type of damage, and so 
 *      the attack will run its course without interference at this stage of
 *      damage calculation</dd>
 *  <dt>X is Strong Against Y</dt>
 *  <dd>(x) type of damage gets a bonus when the defence type is (y). This can
 *      be anything from an improved critical chance or special effect to a
 *      flat bonus to damage dealt.  This is classed as a buff advantage for 
 *      the attacker.</dd>
 *  <dt>X is Strong To Y<dt>
 *  <dd>(x) type of defence gets a bonus when the attacking damage type is (y).
 *      Such bonuses can include an improved dodge chance or the ability to 
 *      perform a counter attack. This is classed as a buff advantage for 
 *      the defender.</dd>
 *  <dt>X is Weak Against Y</dt>
 *  <dd>(x) type of damage is less effective against (y) type of defence. This 
 *      is different to [<em>(y)</em> is Strong To <em>(x)</em>] because this effect is applied 
 *      during an earlier phase of damage calculation and so has a chance to
 *      affect the outcome of an attack in a different way. This is classed
 *      as a debuff advantage for the defender.</dd>
 *  <dt>X is Weak To Y</dt>
 *  <dd>(x) type of defence is less effective when (y) type of damage is being
 *      used. This is different from [<em>(y)</em> is Strong Against 
 *      <em>(x)</em>] because the effect is applied at a later phase of damage 
 *      calculation and as a result will affect the outcome of an attack in a
 *      different way. This is classed as a debuff advantage for the 
 *      attacker</dd>
 * </dl>
 * @author Commander lol
 */
public enum Type {
    /**
     * Damage done by physically based attacks such as the Brawler Headband
     * <br/>
     * <strong>Weak to:</strong>
     * <ul>
     *  <li>Magic</li>
     * </ul>
     * <strong>Weak against:</strong>
     * <ul>
     *  <li>Burning</li>
     * </ul>
     * <strong>Strong against:</strong>
     * <ul>
     * </ul>
     * <strong>Strong to:</strong>
     * <ul>
     * </ul>
     */
    Physical(new String[]{}, new String[]{}),
    /**
     * Damage based in the realms of magic from things like the Wizards Hat
     * <br/>
     * <strong>Weak to:</strong>
     * <ul>
     * </ul>
     * <strong>Weak against:</strong>
     * <ul>
     * </ul>
     * <strong>Strong against:</strong>
     * <ul>
     * </ul>
     * <strong>Strong to:</strong>
     * <ul>
     * </ul>
     */
    Magic(new String[]{}, new String[]{}),
    /**
     * Everything has a price, even damage. Money-grabbing items like the 
     * Top-Hat & Monocle do Capitalist damage
     * <br/>
     * <strong>Weak to:</strong>
     * <ul>
     * </ul>
     * <strong>Weak against:</strong>
     * <ul>
     * </ul>
     * <strong>Strong against:</strong>
     * <ul>
     * </ul>
     * <strong>Strong to:</strong>
     * <ul>
     * </ul>
     */
    Capitalist(new String[]{}, new String[]{}),
    /**
     * Straight from the French coast, this damage is caused by items like the 
     * beret
     * <br/>
     * <strong>Weak to:</strong>
     * <ul>
     * </ul>
     * <strong>Weak against:</strong>
     * <ul>
     * </ul>
     * <strong>Strong against:</strong>
     * <ul>
     * </ul>
     * <strong>Strong to:</strong>
     * <ul>
     * </ul>
     */
    French(new String[]{}, new String[]{}),
    /**
     * With a pencil in one hand and some squared paper in the other, an actor
     * with an item such as N3rd Glasses will deal Logical damage
     * <br/>
     * <strong>Weak to:</strong>
     * <ul>
     * </ul>
     * <strong>Weak against:</strong>
     * <ul>
     * </ul>
     * <strong>Strong against:</strong>
     * <ul>
     * </ul>
     * <strong>Strong to:</strong>
     * <ul>
     * </ul>
     */
    Logical(new String[]{}, new String[]{}),
    /**
     * Throw all reason to the wind and don an item like an Ushanka to deal
     * drunk damage to anybody that isn't your "besht friend" by the end of the 
     * night.
     * <br/>
     * <strong>Weak to:</strong>
     * <ul>
     * </ul>
     * <strong>Weak against:</strong>
     * <ul>
     * </ul>
     * <strong>Strong against:</strong>
     * <ul>
     * </ul>
     * <strong>Strong to:</strong>
     * <ul>
     * </ul>
     */
    Drunk(new String[]{}, new String[]{}),
    /**
     * Feeling a bit hot? Must be using an item like the Redneck Barbecue to 
     * deal burning damage to an enemy. 
     * <br/>
     * <strong>Weak to:</strong>
     * <ul>
     * </ul>
     * <strong>Weak against:</strong>
     * <ul>
     * </ul>
     * <strong>Strong against:</strong>
     * <ul>
     * </ul>
     * <strong>Strong to:</strong>
     * <ul>
     * </ul>
     */
    Burning(new String[]{}, new String[]{}),
    /**
     * <br/>
     * <strong>Weak to:</strong>
     * <ul>
     * </ul>
     * <strong>Weak against:</strong>
     * <ul>
     * </ul>
     * <strong>Strong against:</strong>
     * <ul>
     * </ul>
     * <strong>Strong to:</strong>
     * <ul>
     * </ul>
     */
    Wet(new String[]{}, new String[]{}),
    /**
     * <br/>
     * <strong>Weak to:</strong>
     * <ul>
     * </ul>
     * <strong>Weak against:</strong>
     * <ul>
     * </ul>
     * <strong>Strong against:</strong>
     * <ul>
     * </ul>
     * <strong>Strong to:</strong>
     * <ul>
     * </ul>
     */
    Secular(new String[]{}, new String[]{}),
    /**
     * <br/>
     * <strong>Weak to:</strong>
     * <ul>
     * </ul>
     * <strong>Weak against:</strong>
     * <ul>
     * </ul>
     * <strong>Strong against:</strong>
     * <ul>
     * </ul>
     * <strong>Strong to:</strong>
     * <ul>
     * </ul>
     */
    Sneaky(new String[]{}, new String[]{}),
    /**
     * <br/>
     * <strong>Weak to:</strong>
     * <ul>
     * </ul>
     * <strong>Weak against:</strong>
     * <ul>
     * </ul>
     * <strong>Strong against:</strong>
     * <ul>
     * </ul>
     * <strong>Strong to:</strong>
     * <ul>
     * </ul>
     */
    Divine(new String[]{}, new String[]{}),
    /**
     * <br/>
     * <strong>Weak to:</strong>
     * <ul>
     * </ul>
     * <strong>Weak against:</strong>
     * <ul>
     * </ul>
     * <strong>Strong against:</strong>
     * <ul>
     * </ul>
     * <strong>Strong to:</strong>
     * <ul>
     * </ul>
     */
    Fear(new String[]{}, new String[]{}),
    /**
     * Just as nothing can touch you, you can touch nothing. However, since that
     * would be boring, you instead corporealise as a completely neutral entity
     * that provides no advantages and gets none in return
     */
    Void(new String[]{}, new String[]{});
    
    private Type[] weakness;
    private Type[] strength;
    
    Type(String[] strengths, String[] weaknesses){
        ArrayList<Type> strengthList = new ArrayList<>();
        ArrayList<Type> weaknessList = new ArrayList<>();
        
        if(strengths.length>0){
            for(String typeName : strengths){
                strengthList.add(Type.valueOf(typeName));
            }
            strength = (Type[])strengthList.toArray();
        } else {
            strength = new Type[0];
        }
        
        if(weaknesses.length>0){
            for(String typeName : weaknesses){
                weaknessList.add(Type.valueOf(typeName));
            }
            weakness = (Type[])weaknessList.toArray();
        } else {
            weakness = new Type[0];
        }
    }
    
    public Type[] getWeakness(){return weakness;}
    public Type[] getStrength(){return strength;}
    
}
