package code.items;

/**
 * @author Commander lol
 */
public interface Modifier {
    public String getName();
    public int getValue();
    public Buff[] getEffects();
}
