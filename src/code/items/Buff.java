package code.items;

import code.util.DicePair;

/**
 * @author Commander lol
 */
public class Buff {
    private String id;
    private String stat;
    private int flatBonus;
    private DicePair varBonus;

    public Buff(String id, Type buffType, String stat, int flatBonus, DicePair varBonus) {
        this.id = id;
        this.stat = stat;
        this.flatBonus = flatBonus;
        this.varBonus = varBonus;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public int getFlatBonus() {
        return flatBonus;
    }

    public void setFlatBonus(int flatBonus) {
        this.flatBonus = flatBonus;
    }

    public DicePair getVarBonus() {
        return varBonus;
    }

    public void setVarBonus(DicePair varBonus) {
        this.varBonus = varBonus;
    }
}