package code.items;

import code.actors.BasicActor;
import java.awt.Point;
import org.newdawn.slick.Image;

/**
 * @author Commander lol
 */
public interface Item {
    public Type getType();
    public boolean use();
    public String getImageName();
    public String getName();
    public String getPrefix();
    public String getSuffix();
    public String getRarity();
    public Image getImage();
    public Modifier[] getBuffs();
    public boolean draw(BasicActor target);
    public boolean draw(BasicActor target, Point offset);
    public boolean drawIcon(Point location);
    public boolean drawIcon(Point location, Point offset);
    public boolean drawDrop(Point location, Point offset);
}
