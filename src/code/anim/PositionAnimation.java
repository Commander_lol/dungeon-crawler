package code.anim;

import java.awt.Point;
/**
 * @author Commander lol
 */
public class PositionAnimation implements PAnimation, Anim{
    
    private Point startPoint, endPoint;
    private int duration, curTime;
    
    public PositionAnimation(Point start, Point end, int duration){
        startPoint = start;
        endPoint = end;
        this.duration = duration;
        curTime = 0;
    }
    
    @Override
    public Point getStart() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Point getEnd() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean update(int delta) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getWidth() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getHeight() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Point getPosition() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean draw() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean draw(Point pos) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean draw(Point subPos, int subWidth, int subHeight) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean draw(Point pos, Point subPos, int subWidth, int subHeight) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
