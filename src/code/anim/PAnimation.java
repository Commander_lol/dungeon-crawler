package code.anim;

import code.gui.Drawable;
import java.awt.Point;

/**
 * @author Commander lol
 */
public interface PAnimation extends Drawable{
    public Point getStart();
    public Point getEnd();
    public boolean update(int delta);
    
}
