package code.actors;

import code.gui.Drawable;
import code.handlers.ImageLoader;
import code.items.Item;
import java.awt.Point;
import java.util.ArrayList;
import org.newdawn.slick.Image;

/**
 *
 * @author Commander lol
 */
public class Player implements BasicActor, Drawable{
    /**
     * Usage of a precompiled string will speed up certain functions like getID
     * and getName, since it remains constant for the player. It also saves on
     * memory used.
     */
    private final String identifier = "Player";
    private Image pImg = null;
    private int dir = 0;
    private Point location = new Point(0, 0);
    private int level = 0;
    private int invSize = 12;
    private int spareSpace = 12;
    private Item[] inventory = new Item[invSize];
    private Item equipped = null;
    private double viewDist = 16;
    
    private Player() {
        pImg = ImageLoader.i().getImage("player");
        for(int i = 0; i<inventory.length; i++){
            inventory[i] = null;
        }
    }
    
    public int level(){return level;}
    
    public static Player i() {
        return PlayerHolder.INSTANCE;
    }
    public boolean give(Item newItem){
        if(spareSpace == 0){
            return false;
        } else {
            for(int i = 0; i<invSize; i++){
                if(inventory[i] == null){
                    inventory[i] = newItem;
                    spareSpace --;
                    return true;
                }
            }
            return false;
        }
    }
    public boolean equip(Item newItem, boolean dequip){
        if(equipped != null){
            if(!dequip){
                return false;
            } else {
                if(!give(equipped)){
                    return false;
                }
            }
        }
        equipped = newItem;
        return true;
    }
    public Item discard(int itemLoc){
        Item trash = inventory[itemLoc];
        inventory[itemLoc] = null;
        spareSpace ++;
        return trash;
    }
    public boolean switchInvPos(int pos1, int pos2){
        Item temp = inventory[pos2];
        inventory[pos2] = inventory[pos1];
        inventory[pos1] = temp;
        return true;
    }
    public int inventorySpace(){return spareSpace;}
    public int inventorySize(){return invSize;}
    
    public Item[] getInv(){
        return inventory;
    }
    
    public Item getEquipped(){
        return equipped;
    }

    public double getViewDist() {
        return viewDist;
    }
    
    @Override
    public String getID(){
        return identifier;
    }
    @Override
    public String getName(){
        return identifier;
    }
    @Override
    public String getType(){
        return null;
    }
    public void setLoc(int x, int y){
        location = new Point(x, y);
    }
    public void setLoc(Point newLoc){
        location = newLoc;
    }
    @Override
    public int getWidth() {
        return 32;
    }

    @Override
    public int getHeight() {
        return 32;
    }

    @Override
    public Point getPosition() {
        return location;
    }

    @Override
    public boolean draw() {
        pImg.getSubImage(dir*32, 0, 32, 32).draw(location.x*32, location.y*32);
        if(equipped != null){
            return equipped.draw(this);
        }
        return true;
    }

    @Override
    public boolean draw(Point pos) {
        pImg.getSubImage(dir*32, 0, 32, 32).draw((location.x - pos.x)*32, (location.y - pos.y)*32);
        if(equipped != null){
            return equipped.draw(this, pos);
        }
        return true;
    }

    @Override
    public boolean draw(Point subPos, int subWidth, int subHeight) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean draw(Point pos, Point subPos, int subWidth, int subHeight) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean move(int dir) {
        switch(dir){
            case 0:
                location.translate(0, -1);
                break;
            case 1:
                location.translate(-1, 0);
                this.dir = 1;
                break;
            case 2:
                location.translate(0, 1);
                break;
            case 3:
                location.translate(1, 0);
                this.dir = 0;
                break;
            default:
                return false;
        }
        return true;
    }

    @Override
    public int getFacing() {
        return dir;
    }

    @Override
    public int getBodyType() {
        return 0;
    }
    
    private static class PlayerHolder {

        private static final Player INSTANCE = new Player();
    }
}
