package code.actors;

import java.awt.Point;

/**
 * @author Commander lol
 */
public interface BasicActor {
    public String getID();
    public String getName();
    public String getType();
    public Point getPosition();
    public int getFacing();
    public int getBodyType();
    public boolean move(int dir);
}
