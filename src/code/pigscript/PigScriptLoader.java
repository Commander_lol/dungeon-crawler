package code.pigscript;

import code.Game;
import code.pigscript.tiles.PigTileParser;
import code.util.FileUtil;

import java.io.IOException;

import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * @author Commander lol
 */
public class PigScriptLoader {
    
    private HashMap<String, String> pigFiles = new HashMap<>();
    
    private PigScriptLoader() {
        try{
            pigFiles = getFromDirWithExt(Game.SRC + "data/", ".pig");
        } catch(IOException ioe){
            System.out.println("[PIGSCRIPT LOADER] Encountered an error while trying to find files");
            System.err.println(ioe);
            System.exit(40416);
        }
    }
    
    public static PigScriptLoader i() {
        return PigScriptLoaderHolder.INSTANCE;
    }
    
    public void parseTileScript (String scriptName){
        
    }
    
    private HashMap<String, String> getFromDirWithExt(String directory, String extension) throws IOException{

        String escapedExtension = FileUtil.escapeRegex(".pig");
        HashMap<String, String> returnMap = new HashMap<>();
        Pattern fileExt = Pattern.compile(".+" + escapedExtension + "$");

        DirectoryStream<Path> dirStream = Files.newDirectoryStream(Paths.get("data/"));
        for(Path file : dirStream){
            if(Files.isDirectory(file)){
                if(Files.exists(file.resolve(file.getFileName()+".ts"))){
                    PigTileParser.parse(file);
                } else{
                    returnMap.putAll(getFromDirWithExt(file.toString(), extension));
                }
            } else if(fileExt.matcher(file.getFileName().toString()).matches()){
                String fileName = file.getFileName().toString().replaceAll(escapedExtension, "");
                returnMap.put(fileName, file.toString());
                System.out.println("[FILE LOADER] Added reference for new file: " + fileName + ", " + file.toString());
            }
        }
        return returnMap;
    }
    
//    private static void readHeaderAndSend(String scriptPath){
//        FileReader fileStream = new FileReader("");
//        try{
//            fileStream = new FileReader(scriptPath);
//        } catch (IOException ioe){
//            System.out.println("[PIGSCRIPT LOADER] Encountered an error while trying to read a header");
//            System.err.println(ioe);
//            System.exit(40417);
//        }
//    }
    
    private static class PigScriptLoaderHolder {
        private static final PigScriptLoader INSTANCE = new PigScriptLoader();
    }
}
