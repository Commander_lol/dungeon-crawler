package code.pigscript.tiles;

import code.util.FileUtil;

import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;

import java.util.HashMap;
import java.util.Map.Entry;

import org.ini4j.Ini;
import org.ini4j.Profile.Section;
//import org.ini4j.Profile.Section;

/**
 * @author Commander lol
 */
public class PigTileParser {
    public static void parse(Path tileSetLoc){
        TileSetData tileSet = null;
        
        for(Path file : tileSetLoc){
            if(Files.isDirectory(file)){
                System.out.println("[PigTileParser] Found misplaced directory " + file.getFileName() + " in the folder " + tileSetLoc.toString());
            } else if(FileUtil.isFileType(file.toString(), ".ts")){
                    tileSet = new TileSetData(file);
            }
        }
        for(Entry<String, String> tile : tileSet.pathList().entrySet()){
            
        }
    }
    
    private static class TileSetData{
        private HashMap<String, String> tileList = new HashMap();
        private Ini thisFile = null;
        
        public TileSetData(Path tileFile){
            try{
                thisFile = new Ini(tileFile.toFile());
            } catch (IOException ioe){
                System.out.println("[Tile Parsing] You seriously fucked up to get this error");
                System.err.println(ioe);
                System.exit(40417);
            }
            
            Section defaults = thisFile.get("Defaults");
            Section floor = thisFile.get("Floor");
            Section walls = thisFile.get("Walls");
            Section placeables = thisFile.get("Placeables");
            Section decorations = thisFile.get("Decorations");
            Section extras = thisFile.get("Extras");
        }
        
        public HashMap<String, String> pathList(){return tileList;}
        
    }
}
