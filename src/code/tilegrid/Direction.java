package code.tilegrid;

/**
 * @author Commander lol
 */
public enum Direction {
    Up(0x0001),
    Down(0x0010),
    Left(0x0100),
    Right(0x1000),
    UpLeft(0x0101),
    UpRight(0x1001),
    DownLeft(0x0110),
    DownRight(0x1010),
    Centre(0x1111);
    
    private int dirValue;
    
    Direction(int value){
        dirValue = value;
    }
    int getValue(){
        return dirValue;
    }
    /**
     * Performs a bitwise comparison between the current instance of Direction and
     * another value. More efficient than using .equals() since this method functions
     * at the bit level
     * @param otherValue The other direction that is being compared to the current
     * direction
     * @return Whether or not the two direction objects represent the same direction
     */
    boolean compare(Direction otherValue){
        int val = ~(this.getValue()^otherValue.getValue());
        return val==0x0000;
    }
}
