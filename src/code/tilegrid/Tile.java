package code.tilegrid;

/**
 * Represents a background or decorative tile that can do, but is not limited 
 * to, the following; be walked upon, be inaccessible, affect NPC pathfinding.
 * @author Commander lol
 */
public class Tile {
    
    /**
     * The name of the Tileset the the Tile should use to get it's image from.
     * Tilesets are used to reduce the number of images that need to be loaded,
     * and to speed up rendering time
     */
    private String tileset = null;
    
    /**
     * The number of the tile image to be used, should translate to something
     * roughly similar across various tilesets
     */
    private byte tileNum;
    
    /**
     * Whether or not an {@link Actor actor} can enter this tile, used 
     * primarily in the pathfinding algorithm for NPCs
     */
    private boolean canEnter;
    
    /**
     * The weight to apply to the tile when determining the path for an NPC.
     * A higher number will be reflected as a higher number of steps needed to
     * traverse the tile
     */
    private byte weight;
    
    /**
     * The meta value for this tile. Has a variety of uses; for example, wall
     * tiles store their border state as a meta value
     */
    private byte meta;
    
    public Tile(String tileset, byte tileNum, boolean enterable, byte weight){
        this.tileset = tileset;
        this.tileNum = tileNum;
        this.weight = weight;
        this.meta = 0;
    }
    public Tile(String tileset, byte tileNum, boolean enterable, byte weight, byte meta){
        this.tileset = tileset;
        this.tileNum = tileNum;
        this.weight = weight;
        this.meta = meta;
    }
    
    public boolean canEnter(){return canEnter;}
}
