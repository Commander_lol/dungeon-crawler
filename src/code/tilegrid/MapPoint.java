package code.tilegrid;

import java.awt.Point;

/**
 * @author Commander lol
 */
public interface MapPoint {
    public Point setLocation(Point newLoc);
    public Point getLocation();
    public Point getRenderLocation();
    public boolean isWall();
    public boolean isEnterable();
    public int getTile();
    public int getCost();
    public int setCost(int newCost);
}
