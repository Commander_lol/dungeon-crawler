package code.tilegrid;
/**
 * @author Commander lol
 */
public enum Accessibility {
    /**
     * No blocking tiles
     */
    n(0b0000),
    /**
     * Top route is blocked
     */
    t(0b0001),
    /**
     * Left route is blocked
     */
    l(0b0010),
    /**
     * Bottom route is blocked
     */
    d(0b0100),
    /**
     * Right route is blocked
     */
    r(0b1000),
    /**
     * Top and left routes are blocked
     */
    tl(0b0011),
    /**
     * Top and right routes are blocked
     */
    tr(0b1001),
    /**
     * Top and bottom routes are blocked
     */
    td(0b0101),
    /**
     * Top, left and right routes are blocked
     */
    tlr(0b1011),
    /**
     * Top, left and bottom routes are blocked
     */
    tld(0b0111),
    /**
     * Top, right and bottom routes are blocked
     */
    tdr(0b1101),
    /**
     * Left and right routes are blocked
     */
    lr(0b1010),
    /**
     * Left and bottom routes are blocked
     */
    ld(0b0110),
    /**
     * Left, right and bottom routes are blocked
     */
    ldr(0b1110),
    /**
     * Bottom and right routes are blocked
     */
    dr(0b1100),
    /**
     * All routes are blocked
     */
    a(0b1111),
    /**
     * All routes are blocked.
     * <p>Alternative to 'a'</p>
     */
    tldr(0b1111);
    
    private int value = 0;
    
    Accessibility(int value){
        this.value = value;
    }
    
    public int getValue(){
        return value;
    }
    
    public static int[] getArrayOfCardinals(Accessibility acc){
        switch(acc){
            case t:
                return new int[]{0};
            case l:
                return new int[]{1};
            case d:
                return new int[]{2};
            case r:
                return new int[]{3};
            case tl:
                return new int[]{0,1};
            case tr:
                return new int[]{0,3};
            case td:
                return new int[]{0,2};
            case tlr:
                return new int[]{0,1,3};
            case tld:
                return new int[]{0,1,2};
            case tdr:
                return new int[]{0,2,3};
            case lr:
                return new int[]{1,3};
            case ld:
                return new int[]{1,2};
            case ldr:
                return new int[]{1,3,2};
            case dr:
                return new int[]{2,3};
            case a:
            case tldr:
                return new int[]{0,1,2,3};
            case n:
                return new int[0];
            default:
                return null;
        }
    }
    
}
