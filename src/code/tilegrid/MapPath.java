package code.tilegrid;

import java.util.Set;

/**
 * @author Commander lol
 */
public interface MapPath {
    public MapPoint[] asArray();
    public Set<MapPoint> asSet();
    public boolean setPath(MapPoint[] newData);
    public boolean addPoint(MapPoint newPoint);
}
