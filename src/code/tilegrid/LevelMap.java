package code.tilegrid;

import code.Game;
import code.actors.Player;
import code.gui.Drawable;
import code.handlers.Options;
import code.handlers.TileHandler;
import code.items.Item;
import code.tilegrid.placeables.Placeable;
import code.util.CantorPairing;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Line2D;
import java.io.IOException;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

/**
 * @author Commander lol
 */

public class LevelMap {
    private String campaign;
    private String name;
    private String tileset;
    private int tHeight = 32, tWidth = 32;
    private int seed;
    private volatile boolean updating = false;
    private byte[][] levelArray;
    private byte[][] typeArray;
    private volatile boolean[][] visited;
    private ArrayList<Placeable> objectList;
    private HashMap<Integer, Item> droppedItems;
    
    /**
     * A new level map of width and height 0 and no objects present
     */
    public LevelMap(){
        levelArray = new byte[0][0];
        typeArray = new byte[0][0];
        visited = new boolean[0][0];
        objectList = new ArrayList<>();
        droppedItems = new HashMap<>();
    }
    /**
     * A new level map with the given type data and no objects present
     * @param typeData an integer array that will be converted down into a byte
     * array. Values over 255 will be set to 0
     */
    public LevelMap(int[][] typeData){
        typeArray = new byte[typeData.length][];
        visited = new boolean[typeData.length][];
        for(int i = 0; i<typeData.length; i++){
            typeArray[i] = new byte[typeData[i].length];
            visited[i] = new boolean[typeData[i].length];
            for(int j = 0; j<typeData[i].length; j++){
                typeArray[i][j] = ((byte)typeData[i][j]);
                visited[i][j] = false;
            }
        }
        levelArray = new byte[0][0];
        objectList = new ArrayList<>();
        droppedItems = new HashMap<>();
    }
    public LevelMap(int[][] typeData, ArrayList<Placeable> obs){
        typeArray = new byte[typeData.length][];
        visited = new boolean[typeData.length][];
        for(int i = 0; i<typeData.length; i++){
            typeArray[i] = new byte[typeData[i].length];
            visited[i] = new boolean[typeData[i].length];
            for(int j = 0; j<typeData[i].length; j++){
                typeArray[i][j] = ((byte)typeData[i][j]);
                visited[i][j] = false;
            }
        }
        levelArray = new byte[0][0];
        objectList = obs;
        droppedItems = new HashMap<>();
    }
    /**
     * A new level map with the given type data and no objects present
     * @param typeData a byte array defining the type data of the level map
     */
    public LevelMap(byte[][] typeData){
        typeArray = typeData;
        
        visited = new boolean[typeData.length][];
        for(int i = 0; i<typeData.length; i++){
            visited[i] = new boolean[typeData[i].length];
            for(int j = 0; j<typeData[i].length; j++){
                visited[i][j] = false;
            }
        }
        
        levelArray = new byte[0][0];
        droppedItems = new HashMap<>();
    }
    /**
     * A new level map with data taken from the file located at the
     * given path
     * @param path The location of the file that defines this Level Map
     */
    public LevelMap(String path){
        
        List<String> level = null;
        Path tileFile = Paths.get(path);
        try{
            level = Files.readAllLines(tileFile, Charset.forName("UTF-8"));
        } catch (IOException ioe){
            System.out.println("[Level Map] Couldn't read level map, exiting now");
            System.err.println(ioe);
            System.exit(50200);
        }
        
        String curNum = null;
        StringBuilder curSent = new StringBuilder();
        int curX = 0;
        int curY = 0;
        boolean readLevel = false;
        
        boolean finished = false;
        ArrayList<ArrayList<Byte>> tempList = new ArrayList<>();
        tempList.add(new ArrayList<Byte>());
        for(String line : level){
            if(line.startsWith("[data]")){
                        readLevel = true;
            }
            if(!readLevel){
                for(String s : line.split(",")){
                    if(s.endsWith(";")){
                        s = s.replaceAll(";", "");
                        tempList.get(tempList.size()-1).add(Byte.valueOf(s));
                        tempList.add(new ArrayList<Byte>());
                    } else {
                        tempList.get(tempList.size()-1).add(Byte.valueOf(s));
                    }
                }
            } else {

            }
        } 
        
        typeArray = new byte[tempList.size()][0];
        for(int i = 0; i<typeArray.length; i++){
            typeArray[i] = new byte[tempList.get(i).size()];
            for(int j = 0; j<typeArray[i].length; j++){
                typeArray[i][j] = tempList.get(i).get(j);
            }
        }
    }
    
    public void saveToFile(String campaign, String levelName){
        Path savePath = Paths.get(Game.SRC, "data/campaigns/", campaign, levelName, ".lvl");
        if(Files.exists(savePath)){
            try {
                Files.delete(savePath);
            } catch (IOException ioe) {
                System.err.println(ioe);
                return;
            }
        }
        try {
            savePath = Files.createFile(savePath);
        } catch (IOException ioe) {
            System.err.println(ioe);
            return;
        }
//        Files.newBufferedWriter(savePath, null);
    }
    
    public void debugRender(Graphics g){
        Color initialColour = g.getColor();
        for(int i = 0; i<typeArray.length; i++){
            for (int j = 0; j<typeArray[i].length; j++){
                Color thisColour;
                switch(typeArray[i][j]){
                    case 0:
                        thisColour = Color.black;
                        break;
                    case 1:
                        thisColour = Color.darkGray;
                        break;
                    case 2:
                        thisColour = Color.gray;
                        break;
                    case 3:
                        thisColour = new Color(86, 47, 0);
                        break;
                    default:
                        thisColour = Color.red;
                        break;
                }
                g.setColor(thisColour);
                g.fillRect(j*tWidth, i*tHeight, tWidth, tHeight);
            }
        }
        g.setColor(initialColour);
    }
    public void debugRender(Graphics g, Point offset){
        Color initialColour = g.getColor();
        for(int i = 0; i<typeArray.length; i++){
            for (int j = 0; j<typeArray[i].length; j++){
                Color thisColour;
                switch(typeArray[i][j]){
                    case 0:
                        thisColour = Color.black;
                        break;
                    case 1:
                        thisColour = Color.darkGray;
                        break;
                    case 2:
                        thisColour = Color.gray;
                        break;
                    case 3:
                        thisColour = new Color(86, 47, 0);
                        break;
                    default:
                        thisColour = Color.red;
                        break;
                }
                g.setColor(thisColour);
                g.fillRect((j-offset.x)*tWidth, (i-offset.y)*tHeight, tWidth, tHeight);
            }
        }
        g.setColor(initialColour);
    }
    
    public void drawMinimap(Graphics g, Point pos, Point offset, int scale){
        Color initialColour = g.getColor();
        for(int i = 0; i<typeArray.length; i++){
            for (int j = 0; j<typeArray[i].length; j++){
                if(visited[i][j]){
                   Color thisColour;
                    switch(typeArray[i][j]){
                        case 1:
                            thisColour = Color.darkGray;
                            break;
                        case 2:
                            thisColour = Color.gray;
                            break;
                        case 3:
                            thisColour = new Color(86, 47, 0);
                            break;
                        default:
                            thisColour = Color.black;
                            break;
                    }
                    g.setColor(thisColour);
                    g.fillRect(((j*scale)+pos.x), ((i*scale)+pos.y), scale, scale); 
                } else {
                    g.setColor(Color.black);
                    g.fillRect(((j*scale)+pos.x), ((i*scale)+pos.y), scale, scale); 
                }
                
            }
        }
        g.setColor(Color.white);
        g.drawRect(pos.x, pos.y, typeArray[0].length*scale, typeArray.length*scale);
        
        g.setColor(Color.red);
        g.fillRect((Player.i().getPosition().x*scale) + pos.x, (Player.i().getPosition().y*scale) + pos.y, scale, scale);
        
        g.setColor(Color.cyan);
        for(Placeable p : objectList){
            if(visited[p.getPosition().y][p.getPosition().x]){
                g.fillRect((p.getPosition().x*scale) + pos.x, (p.getPosition().y*scale) + pos.y, scale, scale);  
            }
        }
        
        g.setColor(Color.green);
        
        int height = Options.i().screenHeight()/32, width = Options.i().screenWidth()/32;
        
        if(offset.x<0){
            if(offset.y<0){
                g.drawLine(pos.x, pos.y, pos.x + (width*scale), pos.y + (height*scale));
            } else {
                g.drawLine(pos.x, pos.y + (offset.y*scale), pos.x + (width*scale), pos.y + ((offset.y + height)*scale));
            }
        } else if(offset.x>width){
            
        } else {
            if(offset.y<0){
                g.drawLine(pos.x + (offset.x*scale), pos.y, pos.x + ((offset.x + width)*scale), pos.y + (height*scale));
            } else {
                g.drawLine(pos.x + (offset.x*scale), pos.y + (offset.y*scale), pos.x + ((offset.x + width)*scale), pos.y + ((offset.y + height)*scale));
            }
        }
        
        g.setColor(initialColour);
    }
    
    public byte[][] regenerateTiles(){
        return levelArray;
        /*
        Random regenerator = new Random(seed);
        
        for(int i = 0; i<levelArray.length; i++){
            for(int j = 0; j<levelArray[i].length;j++){
                
            }
        }
        
        return null;
        */
    }
    
    public void modifyTileType(Point tileLoc, byte newType){
        try{
            typeArray[tileLoc.y][tileLoc.x] = newType;
        }catch (ArrayIndexOutOfBoundsException aib){}
    }
    public ArrayList<Point> getAdjacent(Point startPoint){
        ArrayList<Point> adjacentPoints = new ArrayList<>(4);
        
        if(startPoint.x>0){
            adjacentPoints.add(new Point(startPoint.x - 1, startPoint.y));
        }
        if(startPoint.y>0){
            adjacentPoints.add(new Point(startPoint.x, startPoint.y - 1));
        }
        if(startPoint.x<typeArray[startPoint.y].length-1){
            adjacentPoints.add(new Point(startPoint.x+1, startPoint.y));
        }
        if(startPoint.y<typeArray.length-1){
            adjacentPoints.add(new Point(startPoint.x, startPoint.y+1));
        }
        
        adjacentPoints.trimToSize();
        return adjacentPoints;
    }
    public Placeable objectAtLocation(Point location){
        for(Placeable p : objectList){
            if(p.getPosition().equals(location)){
                return p;
            }
        }
        return null;
    }
    public static Point getGridPoint(Point mouseLoc, Point offset){
        int gridX = (int) Math.floor(mouseLoc.x/32);
        int gridY = (int) Math.floor(mouseLoc.y/32);
        return new Point(gridX + offset.x, gridY + offset.y);
    }
    public boolean dropItem(Item newItem, Point location){
        droppedItems.put(new Integer(CantorPairing.getPairing(location.x, location.y)), newItem);
        return true;
    }
    public static int getHeuristic(Point startPoint, Point endPoint){
        return (Math.abs(endPoint.x - startPoint.x) + Math.abs(endPoint.y-startPoint.y));
    }
    public Accessibility getTileAccess(Point gridLoc){
        StringBuilder directions = new StringBuilder();
        
        int x = gridLoc.x, y = gridLoc.y;
        
        if(y<1 || typeArray[y-1][x]<2){
            directions.append("t");
        }
        if(x<1 || typeArray[y][x-1]<2){
            directions.append("l");
        }
        if(y>typeArray.length-2 || typeArray[y+1][x]<2){
            directions.append("d");
        }
        if(x>typeArray[y].length-2 || typeArray[y][x+1]<2){
            directions.append("r");
        }
        if(directions.length()<1){
            directions.append("n");
        }
        
        return Accessibility.valueOf(directions.toString());
    }
    
    public void update() {
        
        if(!updating){
            updating = true;
            new Thread(new UpdateThread()).start();
        }
        
    }
    
    public void render(Point offset){
        for (int i = 0; i<typeArray.length; i++){
            for(int j = 0; j<typeArray[i].length; j++){
                if(visited[i][j]){
                     TileHandler.tileMeta.getTileImage(typeArray[i][j], Math.abs(Player.i().getPosition().x - j) + Math.abs(Player.i().getPosition().y - i)).draw((j-offset.x)*32, (i-offset.y)*32);
                }
            }
        }
        for(Placeable p : objectList){
            if(visited[p.getPosition().y][p.getPosition().x]){
                p.draw(offset);
            }
        }
        for(Map.Entry<Integer, Item> e : droppedItems.entrySet()){
            Point loc = CantorPairing.reversePairingToPoint(e.getKey().intValue());
            if(visited[loc.y][loc.x]){
                e.getValue().drawDrop(loc, offset);
            }
        }
    }
    public void render(Point offset, boolean fullBright){
        if(!fullBright){
            render(offset);
        } else {
            for (int i = 0; i<typeArray.length; i++){
                for(int j = 0; j<typeArray[i].length; j++){
                    TileHandler.tileMeta.getTileImage(typeArray[i][j], 0).draw((j-offset.x)*32, (i-offset.y)*32);
                }
            }
        }
    }

    public byte peekAtTileType(Point gridPoint){
        try{
            return typeArray[gridPoint.y][gridPoint.x];
        } catch (ArrayIndexOutOfBoundsException aib){
            return 0;
        }
    }
    public byte[][] getTypeArray(){return typeArray;}
    
    public void resetTypeArray(byte[][] newArray){
        typeArray = newArray;
    }
    
    public String getTileset() {
        return tileset;
    }

    public void setTileset(String tileset) {
        this.tileset = tileset;
    }

    public int getSeed() {
        return seed;
    }

    public void setSeed(int seed) {
        this.seed = seed;
    }
     public String getCampaign() {
        return campaign;
    }
    public void setCampaign(String campaign) {
        this.campaign = campaign;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getHeight(){
        return typeArray.length;
    }
    public int getWidth(int yLevel){
        return typeArray[yLevel].length;
    }
    
    /**
     * A data structure for easily creating a usable level array from a level
     * file, storing interim data independent of position
     */
    private class GridPoint{
        /**
         * The given value of this point on the grid
         */
        private byte value;
        /**
         * Whether or not this point is the last one on a given line
         */
        private boolean eol;
        
        /**
         * Creates a new grid point with the specified data
         * @param value The particular value of this grid point - normally type
         * data
         * @param eol If this is the last point on a line, set to true otherwise
         * false
         */
        public GridPoint (byte value, boolean eol){
            this.value = value;
            this.eol = eol;
        }
        /**
         * @return Whether or not this is the last point in a given arbitrary line
         */
        public boolean isEndOfLine(){return eol;}
        /**
         * @return The value that has been assigned to this point on the grid
         */
        public byte getValue(){return value;}
    }
    
    private class UpdateThread implements Runnable {

        @Override
        public void run() {
            
            ArrayList<Rectangle> blockingWalls = new ArrayList<>();
            ArrayList<Line2D> lines = new ArrayList<>();
            ArrayList<String> wallPos = new ArrayList<>();
            boolean[][] tempVisited = visited;

            for(int i = 0; i<typeArray.length; i++){
                for(int j = 0; j<typeArray[i].length; j++){
                    if(typeArray[i][j] == 1){
                        blockingWalls.add(new Rectangle(j*tWidth, i*tHeight, tWidth, tHeight));
                        wallPos.add("" + i + "|" + j + "");
                    }
                }
            }

            for(int i = 0; i<typeArray.length; i++){
                for(int j = 0; j<typeArray[i].length; j++){
                    if(!tempVisited[i][j]){
                        Line2D curLine = new Line2D.Double((j*32) + 16, (i*32) + 16, (Player.i().getPosition().x*32) + 16, (Player.i().getPosition().y*32) + 16);
                        boolean hasIntersected = false;
                        if(curLine.getP1().distance(curLine.getP2())/32 <= Player.i().getViewDist()){
                            int curWall = 0;
                            for(Rectangle r : blockingWalls){
                                if(r.intersectsLine(curLine) && !wallPos.get(curWall).equals("" + i + "|" + j + "")){
                                    hasIntersected = true;
                                    break;
                                }
                                curWall++;
                            }
                            if(!hasIntersected){
                                if(typeArray[i+1][j] == 1){
                                    tempVisited[i+1][j] = true;
                                }
                                if(typeArray[i][j+1] == 1){
                                    tempVisited[i][j+1] = true;
                                }
                                if(typeArray[i-1][j] == 1){
                                    tempVisited[i-1][j] = true;
                                }
                                if(typeArray[i][j-1] == 1){
                                    tempVisited[i][j-1] = true;
                                }
                            }
                            tempVisited[i][j] = !hasIntersected;
                        }
                    }
                }
            }
            
            visited = tempVisited;
            updating = false;
        }
    }
}
