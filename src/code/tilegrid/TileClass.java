package code.tilegrid;

/**
 * @author Commander lol
 */
public enum TileClass {
    EMPTY,
    WALL,
    FLOOR,
    PLACEABLE
}
