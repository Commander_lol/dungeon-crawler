package code.tilegrid;

import java.awt.Point;
import java.util.Set;
import code.util.Comparable;
import code.handlers.TileHandler;
import code.util.BinaryHeap;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Commander lol
 */
public class Pathfinder {

    
    /**
     * Provides an implementation of A* the search algorithm to find the optimal
     * route from one point in a closed space to another point within the same 
     * connected closed space
     * @param curMap The level that will provide map data for the pathfinder to
     * use
     * @param startPoint The start of the desired path (Exclusive)
     * @param endPoint The end of the desired path (Inclusive)
     * @return The fastest path from the start to the end
     */
    public static MapPath aStarPath (LevelMap curMap, Point startPoint, Point endPoint){
        AStarPath returnPath = new AStarPath();
        
        BinaryHeap<GridPoint> openList = new BinaryHeap<>();
        HashMap<String, GridPoint> closedList = new HashMap<>();
        ArrayList<Point> tempPoints;
        
        boolean endFound = false;
        
        GridPoint gEvalPoint;
        GridPoint gCurPoint;
        
        String evalID;
        
        Point evalPoint;
        
        if(curMap.peekAtTileType(startPoint)<2 || curMap.peekAtTileType(endPoint)<2){
            return returnPath;
        } else {
            openList.add(new GridPoint("START", getIdOfLoc(startPoint), false, (int)curMap.peekAtTileType(startPoint), 0, LevelMap.getHeuristic(startPoint, endPoint), startPoint));
            
            while(!closedList.containsKey(getIdOfLoc(endPoint))){
                gCurPoint = openList.pop();
                
                tempPoints = curMap.getAdjacent(gCurPoint.getPosition());
                
                while(!tempPoints.isEmpty()){
                    
                    evalPoint = tempPoints.remove(0);
                    evalID = getIdOfLoc(evalPoint);
                    
                    if(!closedList.containsKey(evalID)){
                        if(curMap.peekAtTileType(evalPoint)<2){
                            closedList.put(evalID, new GridPoint(gCurPoint.getID(), evalID, true, (int)curMap.peekAtTileType(evalPoint), Integer.MAX_VALUE, Integer.MAX_VALUE, evalPoint));
                        } else {
                            gEvalPoint = new GridPoint(gCurPoint.getID(), evalID, false, (int)curMap.peekAtTileType(evalPoint), gCurPoint.getCost()+1, LevelMap.getHeuristic(evalPoint, endPoint), evalPoint);
                            openList.add(gEvalPoint);
                        }
                    } else {
//                        if(closedList.get(evalID).getCost()>gCurPoint.getCost()){
//                            closedList.get(evalID).updateCost(gCurPoint.getID(), gCurPoint.getCost()+1);
//                        }
                    }
                }
                
                closedList.put(gCurPoint.getID(), gCurPoint);
            }
            
            gCurPoint = closedList.get(getIdOfLoc(endPoint));
            
            while(!gCurPoint.getPreviousID().equals("START")){
                returnPath.addPoint(gCurPoint);
                gCurPoint = closedList.get(gCurPoint.getPreviousID());
            }
            
        }
        
        return returnPath;
    }
    
    /**
     * Provides an implementation of A* the search algorithm to find the optimal
     * route from one point in a closed space to another point within the same 
     * connected closed space, up to a certain specified length. The path will
     * terminate either at the end point or at the point along the optimal route
     * that correlates to the distance given
     * @param curMap The level that will provide map data for the pathfinder to
     * use
     * @param startPoint The start of the desired path (Exclusive)
     * @param endPoint The end of the desired path (Inclusive)
     * @param length The maximum length for the path to be
     * @return The fastest path from the start to the end
     */
    public static MapPath limitedPath(LevelMap curMap, Point startPoint, Point endPoint, int length){
        AStarPath returnPath = new AStarPath();
        
        BinaryHeap<GridPoint> openList = new BinaryHeap<>();
        HashMap<String, GridPoint> closedList = new HashMap<>();
        ArrayList<Point> tempPoints;
        
        boolean endFound = false, distanceReached = false;
        
        
        GridPoint gEvalPoint;
        GridPoint gCurPoint;
        
        String evalID;
        
        Point evalPoint;
        
        if(curMap.peekAtTileType(startPoint)<2 || curMap.peekAtTileType(endPoint)<2){
            return returnPath;
        } else {
            openList.add(new GridPoint("START", getIdOfLoc(startPoint), false, (int)curMap.peekAtTileType(startPoint), 0, LevelMap.getHeuristic(startPoint, endPoint), startPoint));
            
            while(!closedList.containsKey(getIdOfLoc(endPoint)) && !distanceReached){
                gCurPoint = openList.pop();
                
                tempPoints = curMap.getAdjacent(gCurPoint.getPosition());
                
                while(!tempPoints.isEmpty()){
                    
                    evalPoint = tempPoints.remove(0);
                    evalID = getIdOfLoc(evalPoint);
                    
                    if(!closedList.containsKey(evalID)){
                        if(curMap.peekAtTileType(evalPoint)<2){
                            closedList.put(evalID, new GridPoint(gCurPoint.getID(), evalID, true, (int)curMap.peekAtTileType(evalPoint), Integer.MAX_VALUE, Integer.MAX_VALUE, evalPoint));
                        } else {
                            gEvalPoint = new GridPoint(gCurPoint.getID(), evalID, false, (int)curMap.peekAtTileType(evalPoint), gCurPoint.getCost()+1, LevelMap.getHeuristic(evalPoint, endPoint), evalPoint);
                            openList.add(gEvalPoint);
                        }
                    } else {
//                        if(closedList.get(evalID).getCost()>gCurPoint.getCost()){
//                            closedList.get(evalID).updateCost(gCurPoint.getID(), gCurPoint.getCost()+1);
//                        }
                    }
                }
                
                closedList.put(gCurPoint.getID(), gCurPoint);
                if(gCurPoint.getCost() == length){
                    distanceReached = true;
                    endPoint = gCurPoint.getPosition();
                }
            }
            
            gCurPoint = closedList.get(getIdOfLoc(endPoint));
            
            while(!gCurPoint.getPreviousID().equals("START")){
                returnPath.addPoint(gCurPoint);
                gCurPoint = closedList.get(gCurPoint.getPreviousID());
            }
            
        }
        
        return returnPath;
    }
    
    /**
     * Performs a check to see if there is a path between two points that is up
     * to the specified length
     * @param curMap The level that will provide map data for the pathfinder to
     * use
     * @param startPoint The start of the desired path (Exclusive)
     * @param endPoint The end of the desired path (Inclusive)
     * @param limit The maximum length for the path to be
     * @return Whether or not you can get from the start to the end within the
     * given number of single cost movements
     */
    public static boolean limitedPathCheck(LevelMap curMap, Point startPoint, Point endPoint, int limit) {
        BinaryHeap<GridPoint> openPoints = new BinaryHeap<>();
        HashMap<String, GridPoint> closedPoints = new HashMap<>();
        ArrayList<Point> curPoints;
        
        byte curX = Integer.valueOf(startPoint.x).byteValue();
        byte curY = Integer.valueOf(startPoint.y).byteValue();
        boolean finished = false;
        int iterations = -1;
        
        String curID =  "" + startPoint.x + "" + startPoint.y + "";
        
        Point curPoint = startPoint;
        GridPoint evalPoint = new GridPoint("START", curID, false, curMap.peekAtTileType(startPoint), 0, Math.abs(endPoint.x-curPoint.x) + Math.abs(endPoint.y-curPoint.y), startPoint);
        
        curPoints = curMap.getAdjacent(startPoint);
        
        while(!curPoints.isEmpty()){
            iterations ++;
            curPoint = curPoints.remove(0);
            if(curMap.peekAtTileType(curPoint)<2){
                closedPoints.put("" + curPoint.x + "" + curPoint.y + "", new GridPoint(curID, "" + curPoint.x + "" + curPoint.y + "", true, curMap.peekAtTileType(curPoint), Integer.MAX_VALUE, Integer.MAX_VALUE, curPoint));
            } else if(!closedPoints.containsKey("" + curPoint.x + "" + curPoint.y + "")) {
                openPoints.add(new GridPoint(curID,
                                             "" + curPoint.x + "" + curPoint.y + "",
                                             false,
                                             curMap.peekAtTileType(curPoint),
                                             TileHandler.tileMeta.getTileCost(curMap.peekAtTileType(curPoint)),
                                             LevelMap.getHeuristic(curPoint, endPoint),
                                             curPoint));
            }
        }
        curPoint = evalPoint.getPosition();
        
        closedPoints.put("" + startPoint.x + "" + startPoint.y + "", evalPoint);
        
        while(!finished){
            
            evalPoint = openPoints.pop();
            
            curID = evalPoint.getID();
            
            if(curMap.peekAtTileType(curPoint)>1){
                closedPoints.put(curID, evalPoint);
                curPoints = curMap.getAdjacent(curPoint);
                while(!curPoints.isEmpty()){
                    curPoint = curPoints.remove(0);
                    if(curMap.peekAtTileType(curPoint)<2){
                        if(!closedPoints.containsKey("" + curPoint.x + "" + curPoint.y + "")){
                           closedPoints.put("" + curPoint.x + "" + curPoint.y + "", new GridPoint(curID, "" + curPoint.x + "" + curPoint.y + "", true, curMap.peekAtTileType(curPoint), Integer.MAX_VALUE, Integer.MAX_VALUE, curPoint));
                        }
                    } else {
                        openPoints.add(new GridPoint(curID,
                                                     "" + curPoint.x + "" + curPoint.y + "",
                                                     false,
                                                     curMap.peekAtTileType(curPoint),
                                                     TileHandler.tileMeta.getTileCost(curMap.peekAtTileType(curPoint)),
                                                     Math.abs(endPoint.x-curPoint.x) + Math.abs(endPoint.y-curPoint.y),
                                                     curPoint));
                    }
                }
                curPoint = evalPoint.getPosition();
                if(curPoint == endPoint || iterations == limit){
                    finished = true;
                }
            }
        }
        
        if(closedPoints.containsKey("" + endPoint.x + "" + endPoint.y + "")){
            return true;
        } else {
            return false;
        }
    }

    public static boolean limitedPathCheck(int[][] curMap, Point startPoint, Point endPoint, int limit) {
        BinaryHeap<GridPoint> openPoints = new BinaryHeap<>();
        HashMap<String, GridPoint> closedPoints = new HashMap<>();
        ArrayList<Point> curPoints = new ArrayList<>();
        
        byte curX = Integer.valueOf(startPoint.x).byteValue();
        byte curY = Integer.valueOf(startPoint.y).byteValue();
        boolean finished = false;
        int iterations = -1;
        
        String curID =  "" + startPoint.x + "" + startPoint.y + "";
        
        Point curPoint = startPoint;
        GridPoint evalPoint = new GridPoint("START", curID, false, curMap[startPoint.y][startPoint.x], 0, Math.abs(endPoint.x-curPoint.x) + Math.abs(endPoint.y-curPoint.y), startPoint);
        
        if(startPoint.x>0){
            curPoints.add(new Point(startPoint.x - 1, startPoint.y));
        }
        if(startPoint.y>0){
            curPoints.add(new Point(startPoint.x, startPoint.y - 1));
        }
        if(startPoint.x<curMap[startPoint.y].length-1){
            curPoints.add(new Point(startPoint.x+1, startPoint.y));
        }
        if(startPoint.y<curMap.length-1){
            curPoints.add(new Point(startPoint.x, startPoint.y+1));
        }
        
        while(!curPoints.isEmpty()){
            iterations ++;
            curPoint = curPoints.remove(0);
            if(curMap[curPoint.y][curPoint.x]<2){
                closedPoints.put("" + curPoint.x + "" + curPoint.y + "", new GridPoint(curID, "" + curPoint.x + "" + curPoint.y + "", true, curMap[curPoint.y][curPoint.x], Integer.MAX_VALUE, Integer.MAX_VALUE, curPoint));
            } else if(!closedPoints.containsKey("" + curPoint.x + "" + curPoint.y + "")) {
                openPoints.add(new GridPoint(curID,
                                             "" + curPoint.x + "" + curPoint.y + "",
                                             false,
                                             curMap[curPoint.y][curPoint.x],
                                             TileHandler.tileMeta.getTileCost(curMap[curPoint.y][curPoint.x]),
                                             LevelMap.getHeuristic(curPoint, endPoint),
                                             curPoint));
            }
        }
        curPoint = evalPoint.getPosition();
        
        closedPoints.put("" + startPoint.x + "" + startPoint.y + "", evalPoint);
        
        while(!finished){
            
            evalPoint = openPoints.pop();
            
            curID = evalPoint.getID();
            
            if(curMap[curPoint.y][curPoint.x]>1){
                closedPoints.put(curID, evalPoint);
                
                curPoints.clear();
                
                if(curPoint.x>0){
                    curPoints.add(new Point(curPoint.x - 1, curPoint.y));
                }
                if(curPoint.y>0){
                    curPoints.add(new Point(curPoint.x, curPoint.y - 1));
                }
                if(curPoint.x<curMap[curPoint.y].length-1){
                    curPoints.add(new Point(curPoint.x+1, curPoint.y));
                }
                if(curPoint.y<curMap.length-1){
                    curPoints.add(new Point(curPoint.x, curPoint.y+1));
                }
                
                while(!curPoints.isEmpty()){
                    curPoint = curPoints.remove(0);
                    if(curMap[curPoint.y][curPoint.x]<2){
                        if(!closedPoints.containsKey("" + curPoint.x + "" + curPoint.y + "")){
                           closedPoints.put("" + curPoint.x + "" + curPoint.y + "", new GridPoint(curID, "" + curPoint.x + "" + curPoint.y + "", true, curMap[curPoint.y][curPoint.x], Integer.MAX_VALUE, Integer.MAX_VALUE, curPoint));
                        }
                    } else {
                        openPoints.add(new GridPoint(curID,
                                                     "" + curPoint.x + "" + curPoint.y + "",
                                                     false,
                                                     curMap[curPoint.y][curPoint.x],
                                                     TileHandler.tileMeta.getTileCost(curMap[curPoint.y][curPoint.x]),
                                                     Math.abs(endPoint.x-curPoint.x) + Math.abs(endPoint.y-curPoint.y),
                                                     curPoint));
                    }
                }
                curPoint = evalPoint.getPosition();
                if(curPoint == endPoint || iterations == limit){
                    finished = true;
                }
            }
        }
        
        if(closedPoints.containsKey("" + endPoint.x + "" + endPoint.y + "")){
            return true;
        } else {
            return false;
        }
    }
    
    private static String getIdOfLoc(Point loc){
        StringBuilder string = new StringBuilder();
        string.append(loc.x);
        string.append("|");
        string.append(loc.y);
        
        return string.toString();
    }
    
    private static class GridPoint implements Comparable {

        private boolean isWall = false;
        private boolean closed = false;
        
        private String previous = "";
        private String ID = "";
        
        private int cost = 0;
        private int heuristic = 0;
        private int tileType = 0;
        
        private Point loc = null;

        public GridPoint(String previousPoint, String newID, boolean isAWall, int tileType, int pathCost, int hCost, Point position) {
            previous = previousPoint;
            ID = newID;
            isWall = isAWall;
            this.tileType = tileType;
            cost = pathCost;
            heuristic = hCost;
            loc = position;
        }

        public boolean isWall() {
            return isWall;
        }

        public String getPreviousID() {
            return previous;
        }

        public String getID() {
            return ID;
        }

        public Point getPosition() {
            return loc;
        }

        public void close(){
            closed = true;
        }
        
        public void updateCost(String newPrevious, int newCost) {
            previous = newPrevious;
            cost = newCost;
        }
        public int getCost(){return cost;}
        
        @Override
        public int getCompareValue() {
//            if(closed){
//                return cost;
//            } else {
                return heuristic + cost;
//            }
        }
    }

    private static class AStarPath implements MapPath {

        private ArrayList<MapPoint> pathMap;

        public AStarPath() {
            pathMap = new ArrayList<>();
        }

        @Override
        public boolean setPath(MapPoint[] newMap) {
            return false;
        }

        @Override
        public boolean addPoint(MapPoint newPoint) {
            return pathMap.add(newPoint);
        }
        public boolean addPoint(GridPoint newPoint){
            return pathMap.add(new PathMapPoint(newPoint));
        }

        @Override
        public MapPoint[] asArray() {
            return pathMap.toArray(new MapPoint[0]);
        }

        @Override
        public Set<MapPoint> asSet() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    private static class PathMapPoint implements MapPoint {

        private int thisTile;
        private Point location;
        private int pointCost;

        public PathMapPoint(GridPoint sourcePoint) {
            location = sourcePoint.loc;
            thisTile = sourcePoint.tileType;
            pointCost = TileHandler.tileMeta.getTileCost(thisTile);
        }

        @Override
        public Point getLocation() {
            return location;
        }

        @Override
        public int getTile() {
            return thisTile;
        }

        @Override
        public boolean isWall() {
            return false;
        }

        @Override
        public boolean isEnterable() {
            return false;
        }

        @Override
        public int setCost(int newCost) {
            return pointCost = newCost;
        }

        @Override
        public int getCost() {
            return pointCost;
        }

        @Override
        public Point setLocation(Point newLoc) {
            return location = newLoc;
        }

        @Override
        public Point getRenderLocation() {
            return new Point(location.x*32, location.y*32);
        }
    }
}
