package code.tilegrid.placeables;

/**
 * @author Commander lol
 */
public interface Useable {
    /**
     * The actions that are performed when the object is used by the player
     * @return Returns true if the use was successful, otherwise false
     */
    public boolean use();
}
