package code.tilegrid.placeables;

import code.handlers.ImageLoader;
import code.tilegrid.LevelMap;
import java.awt.Point;
import org.newdawn.slick.Image;
import org.newdawn.slick.SpriteSheet;

/**
 * @author Commander lol
 */
public class LootChest implements Placeable, Useable{

    private Point location;
    private SpriteSheet chest;
    private int state;
    private boolean used;
    
    public LootChest(Point pos){
        used = false;
        state = 0;
        location = pos;
        chest = new SpriteSheet(ImageLoader.i().getImage("chest"), 32, 32);
    }
    
    @Override
    public boolean isPassable() {
        return used;
    }

    @Override
    public void move(Point newLocation) {
        location = newLocation;
    }

    @Override
    public void update(LevelMap level) {
        
    }

    @Override
    public int getWidth() {
        return 32;
    }

    @Override
    public int getHeight() {
        return 32;
    }

    @Override
    public Point getPosition() {
        return location;
    }

    @Override
    public boolean draw() {
        chest.getSprite(state, 0).draw(location.x*32, location.y*32);
        return true;
    }

    @Override
    public boolean draw(Point pos) {
        chest.getSprite(state, 0).draw((location.x - pos.x)*32, (location.y - pos.y)*32);
        return true;
    }

    @Override
    public boolean draw(Point subPos, int subWidth, int subHeight) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean draw(Point pos, Point subPos, int subWidth, int subHeight) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean use() {
        state = 1;
        return used = true;
    }
    
}
