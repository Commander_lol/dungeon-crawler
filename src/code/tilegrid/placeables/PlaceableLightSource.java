package code.tilegrid.placeables;

import code.tilegrid.lighting.LightSource;

/**
 * @author Commander lol
 */
public interface PlaceableLightSource extends LightSource, Placeable {}
