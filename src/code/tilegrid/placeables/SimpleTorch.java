package code.tilegrid.placeables;

import code.handlers.ImageLoader;
import code.tilegrid.Accessibility;
import code.tilegrid.LevelMap;
import code.tilegrid.lighting.LightColour;
import java.awt.Point;

import org.newdawn.slick.Color;
import org.newdawn.slick.SpriteSheet;

/**
 * @author Commander lol
 */
public class SimpleTorch implements PlaceableLightSource{
    
    private int strength = 16;
    private Accessibility wallMounts;
    private SpriteSheet torchImage;
    private Color lightColour;
    private Point pos;

    public SimpleTorch(LevelMap curLevel, Point position){
        torchImage = new SpriteSheet(ImageLoader.i().getImage("torches"), 32, 32);
        lightColour = LightColour.GlowingYellow;
        pos = position;
        update(curLevel);
    }
    
    @Override
    public final void update(LevelMap level){
        wallMounts = level.getTileAccess(pos);
    }
    
    @Override
    public int getLightStrength() {
        return strength;
    }

    @Override
    public int setLightStrength(int newStrength) {
        return strength = newStrength;
    }

    @Override
    public Color getLightColour() {
        return lightColour;
    }

    @Override
    public Color setLightColour(Color newColour) {
        return lightColour = newColour;
    }

    @Override
    public Color addLightColour(Color additionalColour) {
        lightColour.add(additionalColour);
        return lightColour;
    }

    @Override
    public boolean isPassable() {
        return true;
    }

    @Override
    public void move(Point newLocation) {
        pos = newLocation;
    }

    @Override
    public int getWidth() {
        return 32;
    }

    @Override
    public int getHeight() {
        return 32;
    }

    @Override
    public Point getPosition() {
        return pos;
    }

    @Override
    public boolean draw() {
        int[] walls = Accessibility.getArrayOfCardinals(wallMounts);
        if(walls.length>0){
            for(int i : walls){
                torchImage.getSprite(i, 0).draw(pos.x*32, pos.y*32);
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean draw(Point pos) {
        int[] walls = Accessibility.getArrayOfCardinals(wallMounts);
        if(walls.length>0){
            for(int i : walls){
                torchImage.getSprite(i, 0).draw(pos.x*32, pos.y*32);
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean draw(Point subPos, int subWidth, int subHeight) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean draw(Point pos, Point subPos, int subWidth, int subHeight) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
