/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package code.tilegrid.placeables;

import code.handlers.ImageLoader;
import code.tilegrid.Accessibility;
import code.tilegrid.LevelMap;
import java.awt.Point;
import org.newdawn.slick.SpriteSheet;

/**
 *
 * @author Commander lol
 */
public class BasicTurret implements Placeable{

    private Accessibility access;
    private Point location, curSprite;
    private SpriteSheet sprites;
    
    public BasicTurret(LevelMap level, Point pos){
        access = level.getTileAccess(pos);
        location = pos;
        sprites = new SpriteSheet(ImageLoader.i().getImage("turrets"), 32, 32);
        curSprite = new Point(1, 0);
    }
    
    @Override
    public boolean isPassable() {
        return false;
    }

    @Override
    public void move(Point newLocation) {
        location = newLocation;
    }

    @Override
    public void update(LevelMap level) {
        access = level.getTileAccess(location);
        switch(access){
            case t:
                curSprite.move(0, 0);
                break;
            case l:
                curSprite.move(1, 0);
                break;
            case d:
                curSprite.move(2, 0);
                break;
            case r:
                curSprite.move(3, 0);
                break;
            case tl:
                curSprite.move(0, 1);
                break;
            case tr:
                curSprite.move(3, 1);
                break;
            case td:
                curSprite.move(3, 0);
                break;
            case tlr:
                curSprite.move(0, 0);
                break;
            case tld:
                curSprite.move(1, 0);
                break;
            case tdr:
                curSprite.move(3, 0);
                break;
            case lr:
                curSprite.move(1, 0);
                break;
            case ld:
                curSprite.move(1, 1);
                break;
            case ldr:
                curSprite.move(2, 0);
                break;
            case dr:
                curSprite.move(2, 1);
                break;
            case a:
            case tldr:
            case n:
            default:
                curSprite.move(0, 0);
        }
    }

    @Override
    public int getWidth() {
        return 32;
    }

    @Override
    public int getHeight() {
        return 32;
    }

    @Override
    public Point getPosition() {
        return location;
    }

    @Override
    public boolean draw() {
        sprites.getSprite(curSprite.x, curSprite.y).draw(location.x*32, location.y*32);
        return true;
    }

    @Override
    public boolean draw(Point pos) {
        sprites.getSprite(curSprite.x, curSprite.y).draw(pos.x*32, pos.y*32);
        return true;
    }

    @Override
    public boolean draw(Point subPos, int subWidth, int subHeight) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean draw(Point pos, Point subPos, int subWidth, int subHeight) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
