package code.tilegrid.placeables;

import code.gui.Drawable;
import code.tilegrid.LevelMap;

import java.awt.Point;

/**
 * @author Commander lol
 */
public interface Placeable extends Drawable{
    public boolean isPassable();
    public void move(Point newLocation);
    public void update(LevelMap level);
}
