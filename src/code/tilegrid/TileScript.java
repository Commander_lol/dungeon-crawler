package code.tilegrid;

/**
 * 
 * @author Commander lol
 */
public interface TileScript {
    /**
     * The command to be executed when the player
     * @param args 
     */
    public void onEnter(Object... args);
    public void onExit(Object... args);
    public void whileIn(Object... args);
    public void whileOut(Object... args);
}
