package code.tilegrid;

import java.awt.Point;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

/**
 * @author Commander lol
 */
public class TileOverlay {
    
    private Point[] tiles = null;
    private Color drawColour = null;
    
    public TileOverlay(Color overlayColour){
        drawColour = overlayColour;
        drawColour.a = 0.1f;
        tiles = new Point[0];
    }
    
    public TileOverlay(Point[] locations, Color overlayColour){
        tiles = locations;
        drawColour = overlayColour;
        drawColour.a = 0.25f;
    }
    
    public boolean draw(Graphics g, Point offset) {
        Color initialColour = g.getColor();
        g.setColor(drawColour);
        for(Point p : tiles){
            g.fillRect(p.x + offset.x, p.y + offset.y, 32, 32);
        }
        g.setColor(initialColour);
        return true;
    }
    public void updateOverlay(Point[] locations){
        tiles = locations;
    }
}
