package code.tilegrid.lighting;

import java.util.Random;
import org.newdawn.slick.Color;

/**
 * @author Commander lol
 */
public class LightColour extends Color{
    
    public static final Color Darkness = new Color(0,0,0,0.7f);
    public static final Color NeutralWhite = new LightColour(255, 255, 255);
    public static final Color GlowingYellow = new LightColour(235, 255, 18);
    public static final Color TardisBlue = new LightColour(16, 53, 114);
    public static final Color EtherealPurple = new LightColour(199, 64, 207);
    public static final Color SlimerGreen = new LightColour(50, 227, 62);
    
    public LightColour(int r, int g, int b){
        super(r, g, b, 0.5f);
    }
    
    public static Color getRandomColour(){
        Random colourPicker = new Random(1337);
        int colour = colourPicker.nextInt(5);
        
        switch(colour){
            case 0:
                return NeutralWhite;
            case 1:
                return GlowingYellow;
            case 2:
                return TardisBlue;
            case 3:
                return EtherealPurple;
            case 4:
                return SlimerGreen;
            default:
                return Darkness;
        }
    }
    
}
