package code.tilegrid.lighting;

import java.awt.Point;
import org.newdawn.slick.Color;

/**
 * @author Commander lol
 */
public interface LightSource {
    public Point getPosition();
    
    public int getLightStrength();
    public int setLightStrength(int newStrength);
    
    public Color getLightColour();
    public Color setLightColour(Color newColour);
    public Color addLightColour(Color additionalColour);
}
