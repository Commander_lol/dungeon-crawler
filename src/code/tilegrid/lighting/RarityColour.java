package code.tilegrid.lighting;

import org.newdawn.slick.Color;

/**
 * @author Commander lol
 */
public class RarityColour {
    public static Color common = new Color(255, 255, 255);
    public static Color uncommon = new Color(200, 200, 0);
    public static Color almostRare = new Color(255, 127, 0);
    public static Color rare = new Color(200, 0, 0);
    public static Color superRare = new Color(128, 0, 128);
    public static Color legendary = new Color(0, 0, 200);
    public static Color unique = new Color(0, 200, 0);
    
    
    public static Color get(String colourName){
        switch(colourName){
            case "Common":
                return common;
            case "Uncommon":
                return uncommon;
            case "Almost Rare":
                return almostRare;
            case "Rare":
                return rare;
            case "Super Rare":
                return superRare;
            case "Legendary":
                return legendary;
            case "Unique":
                return unique;
            default:
                return common;
        }
    }
}
