package code.tilegrid.lighting;

import code.tilegrid.LevelMap;
import code.util.BinaryHeap;
import code.util.Comparable;

import java.awt.Point;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.Color;

/**
 * @author Commander lol
 */
public class LightMap {
    private int[][] lightValue = null;
    private int strength = 0;
    private Color colour = Color.white;
    private Point location = null;
    
//    public static int[][] get(LevelMap curMap, Point loc, int strength, int levels){
//        int lvSize = strength/levels;
//        
//    }
    
    public LightMap(LevelMap curMap, Point curLoc, int lightStrength, Color lightColour){
        strength = lightStrength;
        colour = lightColour;
        
        update(curMap, curLoc);
    } 
    
    public final void update(LevelMap curMap, Point curLoc){
        byte[][] typeArray = curMap.getTypeArray();
        int distance = strength/2;
   
        BinaryHeap<LightPoint> openPoints = new BinaryHeap<>();
        
        HashMap<String, LightPoint> closedPoints = new HashMap<>();
        
        LightPoint curPoint;
        
}
    
    public final void update(LevelMap curMap, Point curLoc, boolean chk){
        int[][] levelSlice;
        
        int maxDistance = strength/2;
        int xHeight, yHeight, xDepth, yDepth;
        
        if(maxDistance>curLoc.x){
            xDepth = curLoc.x;
        } else {
            xDepth = maxDistance;
        }
        if(maxDistance>curLoc.y){
            yDepth = curLoc.y;
        } else {
            yDepth = maxDistance;
        }
        if(maxDistance>curMap.getWidth(curLoc.y)-curLoc.x-1){
            xHeight = curMap.getWidth(curLoc.y)-1;
        } else {
            xHeight = maxDistance;
        }
        if(maxDistance>curMap.getHeight()-curLoc.y-1){
            yHeight = curMap.getHeight()-1;
        } else {
            yHeight = maxDistance;
        }
        
        location = new Point(curLoc.x-xDepth, curLoc.y-yDepth);
        
        levelSlice = new int[yDepth + 2 + yHeight][xDepth + 2 + xHeight];
        lightValue = levelSlice;
        for(int i = 0; i<lightValue.length; i++){
            for(int j = 0; j<lightValue[i].length; j++){
                lightValue[i][j] = 0;
            }
        }
//        boolean[][] checked = new boolean[lightValue.length][lightValue[0].length];
        
        int countX = 0, countY = 0;
        
        for(int i = curLoc.y-yDepth; i<curLoc.y+yHeight; i++){
            for(int j = curLoc.x-xDepth; j<curLoc.x+xHeight; j++){
                System.out.println(countX + ", " + countY + " :: " + j + ", " + i);
                levelSlice[countY][countX] = curMap.peekAtTileType(new Point(j, i));
                countX++;
            }
            countX = 0;
            countY++;
        }
        
        curLoc.move(xDepth, yDepth);
        
        boolean finished;
        Point prevLoc;
        
        BinaryHeap<LightPoint> openPoints = new BinaryHeap<>();
        HashMap<String, LightPoint> closedPoints = new HashMap<>();
        ArrayList<Point> adjacent;
        
        String curID = "" + curLoc.x + "|" + curLoc.y + "";
        
        openPoints.add(new LightPoint(curID, "START", curLoc, strength, 0));
        
        LightPoint curPoint;
        LightPoint evalPoint;
        
        int travelCost;
        
        while(!openPoints.isEmpty()){
            curPoint = openPoints.pop();
            System.out.println(curPoint.toString());
            adjacent = getAdjacent(levelSlice, curPoint.getLocation());
            while(!adjacent.isEmpty()){
                curLoc = adjacent.remove(0);
                curID = "" + curLoc.x + "|" + curLoc.y + "";
                
                if(isStraightLine(curPoint.getLocation(), curLoc)){
                    travelCost = 2;
                } else {
                    travelCost = 4;
                }
                
                if(closedPoints.containsKey(curID)){
                    /*if(closedPoints.get(curID).getValue()<curPoint.getValue()-travelCost){
                        evalPoint = closedPoints.remove(curID);
                        evalPoint.setPrevID(curPoint.getID());
                        evalPoint.setCost(curPoint.getCost()+1);
                        
                        if(curPoint.getValue()-travelCost<0){
                            evalPoint.setValue(0);
                        }else{
                            evalPoint.setValue(curPoint.getValue()-travelCost);
                        }
                        
                        openPoints.add(evalPoint);
                    }*/
                } else {
                    if(curMap.peekAtTileType(curLoc)<2){
                        closedPoints.put(curID, new LightPoint(curID, curPoint.getID(), curLoc, curPoint.getValue(), Integer.MAX_VALUE));
                    } else {
                        evalPoint = new LightPoint(curID, curPoint.getID(), curLoc, curPoint.getValue()-travelCost, curPoint.getCost()+1);
                        openPoints.add(evalPoint);
                    }
                }
            }
            closedPoints.put(curPoint.getID(), curPoint);
        }
        
        for(Map.Entry<String, LightPoint> entry : closedPoints.entrySet()){
            LightPoint entryPoint = entry.getValue();
            if(entryPoint.getValue()<0){
                lightValue[entryPoint.getLocation().y][entryPoint.getLocation().x] = 0;
            } else {
                lightValue[entryPoint.getLocation().y][entryPoint.getLocation().x] = entryPoint.getValue();
            }
        }
    }
    
    private boolean isStraightLine(Point start, Point end){
        
        if((start.x == end.x+1 || start.x == end.x-1) && start.y == end.y){
            return true;
        } else if ((start.y == end.y+1 || start.y == end.y-1) && start.x == end.x) {
            return true;
        }
        
        return false;
    }
    
    private ArrayList<Point> getAdjacent(int[][] curSection, Point curPoint){
        ArrayList<Point> returnList = new ArrayList<>(4);
        
        if(curPoint.x>0){
            returnList.add(new Point(curPoint.x-1, curPoint.y));
            if(curPoint.x<curSection[curPoint.y].length-1){
                returnList.add(new Point(curPoint.x+1, curPoint.y));
            }
        } else {
            returnList.add(new Point(curPoint.x+1, curPoint.y));
        }
        if(curPoint.y>0){
            returnList.add(new Point(curPoint.x, curPoint.y-1));
            if(curPoint.y<curSection.length-1){
                returnList.add(new Point(curPoint.x, curPoint.y+1));
            }
        } else {
            returnList.add(new Point(curPoint.x, curPoint.y+1));
        }
        
        returnList.trimToSize();
        return returnList;
    }
    
    public Color getColour(){
        return colour;
    }
    public Point getLoc(){
        return location;
    }
    public int[][] asValueArray(){
        return lightValue;
    }
    public double[][] asAlphaArray(){
        return null;
    }
    
    private class LightPoint implements Comparable{

        private int distCost;
        private int lightValue;
        private String ID;
        private Point location;
        private String prevID;
        
        public LightPoint(String ID, String prevID, Point loc, int lightVal, int cost){
            this.ID = ID;
            location = loc;
            this.prevID = prevID;
            lightValue = lightVal;
            distCost = cost;
        }
        @Override
        public int getCompareValue() {
            return distCost;
        }
        
        public int getCost() {
            return distCost;
        }

        public void setCost(int distCost) {
            this.distCost = distCost;
        }

        public int getValue() {
            return lightValue;
        }

        public void setValue(int lightValue) {
            this.lightValue = lightValue;
        }

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }

        public Point getLocation() {
            return location;
        }

        public void setLocation(Point location) {
            this.location = location;
        }
        
        public String getPrevID(){
            return prevID;
        }
        
        public void setPrevID(String prevID){
            this.prevID = prevID;
        }
        
    }
}
