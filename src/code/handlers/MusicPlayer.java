package code.handlers;

import code.Game;
import java.io.File;

import java.io.IOException;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.ini4j.Ini;
import org.ini4j.InvalidFileFormatException;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.Music;

/**
 * @author Commander lol
 */
public class MusicPlayer {
    
    private HashMap<String, String> soundPaths = new HashMap<>();
    private HashMap<String, String> defPaths = new HashMap<>();
    private HashMap<String, Sound> soundEffects = new HashMap<>();
    private HashMap<String, Music> musicFiles = new HashMap<>();
    
    private HashMap<String, String> typeMap = new HashMap<>();
    
    private ArrayList<Sound> activeSounds = new ArrayList<>();
    private volatile Music curMusic = null;
    
    private volatile int musicVolume = 100;
    private volatile int soundVolume = 100;
    
    private MusicPlayer(){
        Ini curDef = null;
        
        try {
            soundPaths = FileLoader.i().getFromDirWithExt(Game.SRC+"data/", ".ogg", false);
        } catch (IOException ioe) {
            System.err.println(ioe);
            System.out.println("[MUSIC PLAYER] Quitting the program now to prevent further errors");
            System.exit(40474);
        }
        try {
            defPaths = FileLoader.i().getFromDirWithExt(Game.SRC+"data/", ".aud", false);
        } catch (IOException ioe) {
            System.err.println(ioe);
            System.out.println("[MUSIC PLAYER] Quitting the program now to prevent further errors");
            System.exit(40474);
        }
        for(Map.Entry<String, String> entry : defPaths.entrySet()){
            try {
                curDef = new Ini(new File(entry.getValue()));
            } catch (IOException ioe) {
                System.out.println("[Music Player] Couldn't load the audio definition file " + entry.getKey() + " from the path " + entry.getValue());
                System.err.println(ioe);
            }
            for(Map.Entry<String, String> def : curDef.get("Type").entrySet()){
                if(def.getValue().equalsIgnoreCase("sound")){
                    try {
                        soundEffects.put(def.getKey(), new Sound(soundPaths.get(def.getKey())));
                    } catch (SlickException se) {
                        System.out.println("[MUSIC PLAYER] Couldn't create the sound " + def.getKey() + " from the path " + soundPaths.get(def.getKey()));
                        System.err.println(se);
                    }
                } else if(def.getValue().equalsIgnoreCase("music")) {
                    try {
                        musicFiles.put(def.getKey(), new Music(soundPaths.get(def.getKey())));
                    } catch (SlickException se) {
                        System.out.println("[MUSIC PLAYER] Couldn't create the sound " + def.getKey() + " from the path " + soundPaths.get(def.getKey()));
                        System.err.println(se);
                    }
                }
            }
        }
    }
    
    public void fadeOut(){
        curMusic.fade(1000, 0, true);
    }
    
    public void loopMusic(String musicFile){
        if(musicFiles.containsKey(musicFile)){
            if(curMusic!=null){
                fadeOut();
            }
            curMusic = musicFiles.get(musicFile);
            curMusic.loop(1, 0);
            curMusic.fade(1500, musicVolume, false);
        } else {
            System.out.println("[Music Player] Unable to find the music file " + musicFile);
        }
    }
    
    public void loopSound(String soundFile){
        if(soundEffects.containsKey(soundFile)){
            activeSounds.add(soundEffects.get(soundFile));
            activeSounds.get(activeSounds.size()-1).loop(1, getFloatVolumeSound());
        } else {
            System.out.println("[Music Player] Unable to play the sound " + soundFile);
        }
    }
    public void playSound(String soundFile){
        if(soundEffects.containsKey(soundFile)){
            activeSounds.add(soundEffects.get(soundFile));
            activeSounds.get(activeSounds.size()-1).play(1, getFloatVolumeSound());
        } else {
            System.out.println("[Music Player] Unable to play the sound " + soundFile);
        }
    }
    
    public void update(){
        if(curMusic.getVolume()!=getFloatVolumeMusic()){
            curMusic.fade(5000, musicVolume, false);
        }
        for(int i = 0; i<activeSounds.size(); i++){
            if(!activeSounds.get(i).playing()){
                activeSounds.remove(i);
            }
        }
    }
    private float getFloatVolumeMusic(){
        Float value = Integer.valueOf(musicVolume).floatValue();
        if(value.floatValue() <= 0){
            return 0f;
        } else {
            return value.floatValue()/100;
        }
    }
    private float getFloatVolumeSound(){
        Float value = Integer.valueOf(soundVolume).floatValue();
        if(value.floatValue() <= 0){
            return 0f;
        } else {
            return value.floatValue()/100;
        }
    }
    public static MusicPlayer i() {
        return MusicPlayerHolder.INSTANCE;
    }
    
    private static class MusicPlayerHolder {
        private static final MusicPlayer INSTANCE = new MusicPlayer();
    }
}
