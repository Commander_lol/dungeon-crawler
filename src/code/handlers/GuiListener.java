package code.handlers;

import code.gui.Drawable;
import code.gui.Hoverable;
import code.gui.Clickable;
import java.awt.Point;

import java.util.ArrayList;                                                                                           

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.newdawn.slick.Input;

/**
 * @author Commander lol
 */
public class GuiListener {
    private Drawable[] itemsToDraw;
    private Hoverable lastHoverFocus;
    private Clickable currentFocus;
    
    
    public GuiListener(){
        itemsToDraw = new Drawable[0];
        lastHoverFocus = null;
    }
    public GuiListener(int initSize){
        itemsToDraw = new Drawable[initSize];
        lastHoverFocus = null;
    }
    /**
     * Draw all the elements that have been registered to this listener at 
     * their default given locations
     */
    public void render(){
        for(Drawable d : itemsToDraw){
            d.draw();
        }
    }
    public void update(Input input){
        Point mouseLoc = new Point(input.getMouseX(), input.getMouseY());
        boolean hasFocused;
        boolean superExists;
        Class curSuper;
        for(Drawable d : itemsToDraw){
            for (Class i : d.getClass().getInterfaces()){
                if(Hoverable.class.isAssignableFrom(i)){
                    if(((Hoverable)d).hasMouseFocus(mouseLoc)){
                        ((Hoverable)d).hover();
                        if(Clickable.class.isAssignableFrom(i)){
                            if(input.isMouseButtonDown(Input.MOUSE_LEFT_BUTTON)){
                                ((Clickable)d).mouseDown();
                            }
                            if(input.isMousePressed(Input.MOUSE_LEFT_BUTTON)){
                                ((Clickable)d).click();
                            }
                        }
                    } else {
                        ((Hoverable)d).mouseOut();
                    }
                }
            }
        }
    }
    
    
    /**
     * Gets an item from this listener that is stored at the specified index
     * @param index
     * @return 
     */
    public Drawable getElement(int index){
        return itemsToDraw[index];
    }
    public Drawable getElement(String ID){
        return itemsToDraw[findElement(ID)];
    }
    public int findElement(String ID){
        int start = 0, end = itemsToDraw.length, middle = end/2;
        for(int i = 0; i<itemsToDraw.length; i++){
            if (itemsToDraw[i].toString().equals(ID)){
                return i;
            }
        }
        return itemsToDraw.length;
    }
    /**
     * Adds a new drawable element to this GUI listener object by expanding the
     * storage array
     * @param newElement The new object that will be handled by this listener
     */
    public void register(Drawable newElement){
        ArrayList<Drawable> tempList = new ArrayList<>(itemsToDraw.length);
        if(itemsToDraw.length>0){
            for(Drawable d : itemsToDraw){
                tempList.add(d);
            }
        }
        tempList = insert(newElement, tempList);
        itemsToDraw = tempList.toArray(itemsToDraw);
    }
    
    /**
     * Allows an external class to call the methods of a contained drawable
     * object without having to create wrapped methods in the guiListener
     * @param ID The ID of the drawable object being affected
     * @param methodName The case sensitive name of the method that needs to be
     * called. If it does not match an existing method, this function will throw
     * an exception
     * @param args The arguments that the method will be expecting. Must be
     * the exact number, type and order of the original method, otherwise this
     * function will throw an exception
     */
    public Object modifyElement(String ID, String methodName, Object... args){
        Object returnValue = null;
        Drawable curDraw = getElement(findElement(ID));
        Method[] methods = curDraw.getClass().getMethods();
        boolean hasParams;
        for(Method m : methods){
            if(m.getParameterTypes().length > 0){
                hasParams = true;
            } else {
                hasParams = false;
            }
            if((m.getParameterTypes().length == args.length || (hasParams && Object[].class.isAssignableFrom(m.getParameterTypes()[m.getParameterTypes().length-1]))) &&  m.getName().equalsIgnoreCase(methodName)){
                try{
                    Object[] allArgs = new Object[m.getParameterTypes().length];
                    if(hasParams && Object[].class.isAssignableFrom(m.getParameterTypes()[m.getParameterTypes().length-1])){
                        int argsBeforeArray = m.getGenericParameterTypes().length-1;
                        int count = 0;
                        Object[] varArgs = new Object[args.length-argsBeforeArray];
                        for(int i = 0; i<argsBeforeArray; i++){
                            allArgs[i] = args[i];
                        }
                        for(int i = argsBeforeArray; i<args.length; i++){
                            varArgs[count] = args[i];
                            count++;
                        }
                        allArgs[allArgs.length-1] = varArgs;
                    } else {
                        allArgs = args;
                    }
                    return m.invoke(itemsToDraw[findElement(ID)], allArgs);
                } catch(InvocationTargetException | IllegalAccessException ex){
                    System.out.println("[GUI Listener] Couldn't invoke the target method");
                    System.err.println(ex);
                    System.exit(50200);
                } catch(IllegalArgumentException iae){
                    System.out.println("[GUI Listener] Incorrect number of arguments: " + args.length + " when expecting " + m.getParameterTypes().length);
                    System.err.println(iae);
                }
            }
        }
        return null;
    }
    
    /**
     * Inserts a drawable element into this guiListener in such a way that the
     * main array will still be ordered and therefore able to be searched in a
     * more efficient manner
     * @param newElement The element that is getting inserted
     * @param list An ArrayList representation of the Array that is being 
     * modified
     * @return The new, still ordered ArrayList containing newElement
     */
    private ArrayList<Drawable> insert(Drawable newElement, ArrayList<Drawable> list){
        ArrayList<Drawable> listCopy = list;
        boolean finished = false;
        int start = 0, end = list.size()-1, middle = 0;
        if(end > 0){
            middle = end/2;
            while(!finished){
                int lex = listCopy.get(middle).toString().compareToIgnoreCase(newElement.toString());
                if(start == end || lex == 0){
                    finished = true;
                } else if(lex>0){
                    start = middle+1;
                    middle = (start+end)/2;
                } else if(lex<0){
                    end = middle;
                    middle = (start+end)/2;
                }
            }
        } else {
            middle = 0;
        }
        listCopy.add(middle, newElement);
        
//        for(int i = 0; i<listCopy.size(); i++){
//            System.out.println("[Gui Listener] [" + i + "] " + listCopy.get(i));
//        }
        
        return listCopy;
    }
}
