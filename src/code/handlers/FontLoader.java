package code.handlers;

import code.Game;
import java.io.IOException;

import java.util.HashMap;

import org.newdawn.slick.AngelCodeFont;
import org.newdawn.slick.SlickException;

/**
 * @author Commander lol
 */
public class FontLoader {
    
    private HashMap<String, String> fntList;
    private String[] fntKeys = new String[0];
    private HashMap<String, String> imgList;
    private String[] imgKeys = new String[0];
    
    private HashMap<String, AngelCodeFont> AngelFonts;
    
    private FontLoader() {
        AngelFonts = new HashMap<>();
        try{
            fntList = FileLoader.i().getFromDirWithExt(Game.SRC + "data/fonts", ".fnt", true);
            imgList = FileLoader.i().getFromDirWithExt(Game.SRC + "data/fonts", ".png", true);
        } catch(IOException ioe){
            System.out.println("[Font Loader] Could not load some fonts");
            System.err.println(ioe);
            System.exit(40416);
        }
        
        fntKeys = fntList.keySet().toArray(fntKeys);
        imgKeys = imgList.keySet().toArray(imgKeys);
        
        for (int i = 0; i < fntKeys.length; i++){
            try{
                if(fntList.containsKey(fntKeys[i])){
                    AngelFonts.put(fntKeys[i], new AngelCodeFont(fntList.get(fntKeys[i]), ImageLoader.i().getImage(imgKeys[i])));
                } else {
                    System.out.println("[Font Loader] Something seriously fucked up");
                }
            }catch(SlickException se){
                System.out.println("[Font Loader] Failed to create " + fntKeys[i]);
                System.err.println(se);
                System.exit(50213);
            }
        }
    }
    
    public AngelCodeFont getFont(String fontName){
        fontName += "FNT";
        return AngelFonts.get(fontName);
    }
    
    public static FontLoader i() {
        return FontLoaderHolder.INSTANCE;
    }
    
    private static class FontLoaderHolder {

        private static final FontLoader INSTANCE = new FontLoader();
    }
}
