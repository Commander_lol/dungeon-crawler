package code.handlers;

import code.anim.Anim;
import code.anim.PAnimation;
import java.util.ArrayList;

/**
 * @author Commander lol
 */
public class AnimHandler {
    
    private ArrayList<Anim> animList = new ArrayList<>();
    private boolean isBlocking;
    
    public AnimHandler(){
        isBlocking = false;
    }
    
    public void register(Anim newAnim){
        animList.add(newAnim);
    }
    
    public boolean update(int delta){
        return false;
    }
    
//    public boolean render(Graphics g){
//        
//    }

    public boolean shouldBlock(){
        return isBlocking;
    }
}
