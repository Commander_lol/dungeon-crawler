package code.handlers;

import code.tilegrid.LevelMap;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Commander lol
 */

public class LevelLoader {
    private HashMap<String, String> levelPaths = new HashMap<>();
    private HashMap<String, HashMap<String, LevelMap>> campaigns = new HashMap<>();

    private LevelLoader() {
        try{
            levelPaths = FileLoader.i().getFromDirWithExt("data/levels/", ".lvl", true);
        } catch (IOException ioe) {
            System.out.println("[Level Loader] Couldn't get a list of level files");
            System.err.println(ioe);
            System.out.println("[Level Loader] Exiting now to prevent further errors");
            System.exit(40437);
        }
        
        for(Map.Entry<String, String> entry : levelPaths.entrySet()){
            LevelMap curLevel = new LevelMap(entry.getValue());
            if(campaigns.containsKey(curLevel.getCampaign())){
                campaigns.get(curLevel.getCampaign()).put(entry.getKey(), curLevel);
            } else {
                HashMap<String, LevelMap> newCampaign = new HashMap<>();
                newCampaign.put(entry.getKey(), curLevel);
                campaigns.put(curLevel.getCampaign(), newCampaign);
            }
        }
    }

    public static LevelLoader i() {
        return LevelLoaderHolder.INSTANCE;
    }

    private static class LevelLoaderHolder {
        private static final LevelLoader INSTANCE = new LevelLoader();
    }
 }
