package code.handlers;

import code.tilegrid.Tile;

import java.util.HashMap;
import java.util.Set;

import java.io.IOException;
import org.newdawn.slick.Image;
import org.newdawn.slick.SpriteSheet;

/**
 * @author Commander lol
 */
public class TileHandler {
    
    String[] tilesets = new String[0];
    HashMap<String, String> paths = new HashMap<>();
    HashMap<String, Tile[][]> tileSetValues = new HashMap<>();
    
    /*
     * */
    
    static SpriteSheet dungeonTiles;
    
    /*
     * */
    
    private TileHandler() {
//        try{
//            paths = FileLoader.i().getFromDirWithExt("data/tilesets", ".ts", false);
//        } catch (IOException ioe){
//            System.out.println("[Tile Handler] Encountered an error while loading tileset files");
//            System.err.println(ioe);
//            System.exit(40416);
//        }  
//        String[] keys = paths.keySet().toArray(new String[0]);
//        String[] values = paths.values().toArray(new String[0]);
//        
//        for(int i = 0; i<keys.length; i++){
//            Tile[][] curSet;
//            
//        }
        
        dungeonTiles = new SpriteSheet(ImageLoader.i().getImage("dungeonBasicTiles"), 32, 32);
    }
    
    public static TileHandler i() {
        return TileHandlerHolder.INSTANCE;
    }
    public Tile getTile(int tileReg){
        
        return null;
    }
    public Tile[][] getTileSet(){
        return null;
    }
    
    public SpriteSheet getDungeonTiles(){
        return dungeonTiles;
    }
    
    private static class TileHandlerHolder {

        private static final TileHandler INSTANCE = new TileHandler();
    }
    public static class tileMeta {
        public static int getTileCost(int tileType){
            switch(tileType){
                case 0:
                case 1: return Integer.MAX_VALUE;
                case 2: return 1;
                case 3: return 3;
                default: return Integer.MAX_VALUE;
            }
        }
        public static Image getTileImage(int tileType, int distance){
            switch(tileType){
                case 0:
                    if(distance<4){
                        return TileHandler.i().getDungeonTiles().getSprite(0, 1);
                    }
                    if(distance<8){
                        return TileHandler.i().getDungeonTiles().getSprite(1, 1);
                    }
                    if(distance<12){
                        return TileHandler.i().getDungeonTiles().getSprite(2, 1);
                    }
                    if(distance<16){
                        return TileHandler.i().getDungeonTiles().getSprite(3, 1);
                    }
                    if(distance<20){
                        return TileHandler.i().getDungeonTiles().getSprite(4, 1);
                    }
                    else {
                        return TileHandler.i().getDungeonTiles().getSprite(5, 1);
                    }
                case 1: 
                    if(distance<4){
                        return TileHandler.i().getDungeonTiles().getSprite(0, 0);
                    }
                    if(distance<8){
                        return TileHandler.i().getDungeonTiles().getSprite(1, 0);
                    }
                    if(distance<12){
                        return TileHandler.i().getDungeonTiles().getSprite(2, 0);
                    }
                    if(distance<16){
                        return TileHandler.i().getDungeonTiles().getSprite(3, 0);
                    }
                    if(distance<20){
                        return TileHandler.i().getDungeonTiles().getSprite(4, 0);
                    }
                    else {
                        return TileHandler.i().getDungeonTiles().getSprite(5, 0);
                    }
                case 2: 
                    if(distance<4){
                        return TileHandler.i().getDungeonTiles().getSprite(0, 2);
                    }
                    if(distance<8){
                        return TileHandler.i().getDungeonTiles().getSprite(1, 2);
                    }
                    if(distance<12){
                        return TileHandler.i().getDungeonTiles().getSprite(2, 2);
                    }
                    if(distance<16){
                        return TileHandler.i().getDungeonTiles().getSprite(3, 2);
                    }
                    if(distance<20){
                        return TileHandler.i().getDungeonTiles().getSprite(4, 2);
                    }
                    else {
                        return TileHandler.i().getDungeonTiles().getSprite(5, 2);
                    }
                case 3:
                    if(distance<4){
                        return TileHandler.i().getDungeonTiles().getSprite(0, 3);
                    }
                    if(distance<8){
                        return TileHandler.i().getDungeonTiles().getSprite(1, 3);
                    }
                    if(distance<12){
                        return TileHandler.i().getDungeonTiles().getSprite(2, 3);
                    }
                    if(distance<16){
                        return TileHandler.i().getDungeonTiles().getSprite(3, 3);
                    }
                    if(distance<20){
                        return TileHandler.i().getDungeonTiles().getSprite(4, 3);
                    }
                    else {
                        return TileHandler.i().getDungeonTiles().getSprite(5, 3);
                    }
                default: return TileHandler.i().getDungeonTiles().getSprite(10, 10);
            }
        }
    }
}
