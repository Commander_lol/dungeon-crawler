package code.handlers;

import code.Game;

import java.util.HashMap;
import java.util.Map;

import java.io.IOException;

import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.DirectoryStream;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 * Loads and stores all the images at the start of the game to improve speed/
 * rendering time during the game. Also provides a single location to retrieve
 * all images from
 * @author Commander lol
 */
public class ImageLoader {
    
    private HashMap<String, String> pathMap;
    private HashMap<String, Image> imageMap;
    
    private ImageLoader() {
        System.out.println("[IMAGE LOADER] Creating the image loader");
        pathMap = new HashMap<>();
        imageMap = new HashMap<>();
        try{
            pathMap = FileLoader.i().getFromDirWithExt(Game.SRC + "data/", ".png", false);
            
        } catch(IOException ioe){
            System.out.println("[IMAGE LOADER] Couldn't grab an image map from the file system");
            System.err.println(ioe);
            System.out.println("[IMAGE LOADER] Quitting the program now to prevent further errors");
            System.exit(40473);
        }
        for(Map.Entry<String, String> entry : pathMap.entrySet()){
            try{
                imageMap.put(entry.getKey(), new Image(entry.getValue()));
            } catch (SlickException se){
                System.out.println("[IMAGE LOADER] Couldn't create the image " + entry.getKey() + "from the path " + entry.getValue());
                System.err.println(se);
                System.out.println("[IMAGE LOADER] Quitting the program now to prevent further errors");
                System.exit(40474);
            } 
        }
        System.out.println("[IMAGE LOADER] Image loader has finished loading");
    }

    public Image getImage(String imageName){
        return imageMap.get(imageName);
    }
    public static ImageLoader i() {
        return ImageLoaderHolder.INSTANCE;
    }
    /**
     * @param directory A path object that represents the directory to 
     * recursively search using a depth-first algorithm
     * @return A map containing the name of each file as well as the associated
     * image object
     * @throws IOException
     * @throws SlickException
     * @deprecated This function is too specific and the implementation is 
     * similar to the more advanced function 
     * {@link FileLoader#getFromDirWithExt(java.lang.String, java.lang.String) FileLoader.getFromDirWithExt(directory, ".png")}
     */
    private HashMap<String, Image> grabImages(Path directory)throws IOException, SlickException {
        HashMap<String, Image> returnMap = new HashMap<>();
        DirectoryStream<Path> imageFinder = Files.newDirectoryStream(directory, "*.png");
        for(Path file : imageFinder){
            System.out.println("[IMAGE LOADER] Trying " + file.toString());
            if(Files.isDirectory(file)){
                HashMap<String, Image> newMap = (grabImages(file));
                returnMap.putAll(newMap);
            } else {
                String fileName = file.getFileName().toString();
                System.out.println("[IMAGE LOADER] Found the image " + fileName);
                returnMap.put(fileName, new Image(file.toString()));
            }
        }
        return null;
    }
    private static class ImageLoaderHolder {
        private static final ImageLoader INSTANCE = new ImageLoader();
    }
}
