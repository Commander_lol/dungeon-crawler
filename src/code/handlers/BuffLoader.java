/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package code.handlers;

import code.Game;
import code.items.Buff;
import code.items.Modifier;
import code.items.Type;
import code.util.DicePair;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import org.ini4j.Ini;
import org.ini4j.Profile.Section;

/**
 *
 * @author Commander lol
 */
public class BuffLoader {
    
    private HashMap<String, String> files = new HashMap<>();
    private HashMap<String, Modifier> prefixes = new HashMap<>();
    private HashMap<String, Modifier> suffixes = new HashMap<>();
    private static Random buffRand;

    private BuffLoader() {
        
        buffRand = new Random();
        
        try{
            files = FileLoader.i().getFromDirWithExt(Game.SRC + "data/", ".buff", true);
        } catch(IOException ioe){
            System.out.println("[BUFF LOADER] Shit went down trying to load up some sweet buffs, check the logs");
            System.err.println(ioe);
        }
        
        for(String s : files.values()){
            Ini curFile = null;
            try{
                curFile = new Ini(Paths.get(s).toFile());
            } catch (IOException ioe){
                System.out.println("[BUFF LOADER] Shit went down trying to load " + s + ", might be a corrupt or incorrectly formatted file");
                System.err.println(ioe);
                System.exit(507);
            }
            
            Section mainInfo = curFile.get("Buff");
            String type = mainInfo.get("type");
            
            ArrayList<Buff> buffs = new ArrayList<>();
            String buffNames = mainInfo.get("effects");
            System.out.println(buffNames);
            if(buffNames!= null){
                for(String b : buffNames.split(",")){
                    b = b.trim();
                    System.out.println(b);
                    
                    Section buffInfo = curFile.get(b);
                    if(buffInfo!=null){
                        String id = buffInfo.get("id");
                        Type effectType = Type.valueOf(buffInfo.get("type"));
                        String stat = buffInfo.get("stat");
                        int flatBonus = new Integer(buffInfo.get("flat").trim()).intValue();
                        int varNum = new Integer(buffInfo.get("varNum")).intValue();
                        int dieNum = new Integer(buffInfo.get("varDie")).intValue();
                        DicePair die = new DicePair(varNum, dieNum);
                        buffs.add(new Buff(id, effectType, stat, flatBonus, die));
                    }
                }
            }
            System.out.println(mainInfo.get("value"));
            System.out.println(mainInfo.get("name"));
            if(buffs.size()>0){  
                Buff[] buffArray = new Buff[buffs.size()];
                for(int i = 0; i<buffs.size(); i++){
                    buffArray[i] = buffs.get(i);
                }
                if(type.equalsIgnoreCase("Suffix")){
                    suffixes.put(mainInfo.get("name"), new Suffix(mainInfo.get("name"), new Integer(mainInfo.get("value")).intValue(), buffArray));
                } else if(type.equalsIgnoreCase("Prefix")){
                    prefixes.put(mainInfo.get("name"), new Prefix(mainInfo.get("name"), new Integer(mainInfo.get("value")).intValue(), buffArray));
                } else {
                   System.out.println("[BUFF LOADER] Unrecognised modifier type " + type);
                } 
            } else {
                if(type.equalsIgnoreCase("Suffix")){
                    suffixes.put(mainInfo.get("name"), new Suffix(mainInfo.get("name"), new Integer(mainInfo.get("value")).intValue()));
                } else if(type.equalsIgnoreCase("Prefix")){
                    prefixes.put(mainInfo.get("name"), new Prefix(mainInfo.get("name"), new Integer(mainInfo.get("value")).intValue()));
                } else {
                   System.out.println("[BUFF LOADER] Unrecognised modifier type " + type);
                } 
            }
        }
    }

    public Modifier getBuff(String type, String name){
        if(type.equalsIgnoreCase("Suffix")){
            return suffixes.get(name);
        } else if(type.equalsIgnoreCase("Prefix")){
            return prefixes.get(name);
        } else {
            System.out.println("[BUFF LOADER] Unrecognised modifier type " + type);
            return null;
        }
    }
    public Modifier getRandomBuff(String type){
        Modifier[] list;
        
        if(type.equalsIgnoreCase("Suffix")){
            System.out.println(suffixes);
            int modNum = suffixes.values().size(), count = 0;
            int pickerNum = buffRand.nextInt(modNum*2);
            System.out.println(pickerNum);
            for(Modifier m : suffixes.values()){
                if(count == pickerNum){
                    return m;
                 } else {
                    count++;
                }
            }
        } else if(type.equalsIgnoreCase("Prefix")){
            int modNum = prefixes.values().size(), count = 0;
            int pickerNum = buffRand.nextInt(modNum);
            System.out.println(pickerNum);
            for(Modifier m : prefixes.values()){
                if(count == pickerNum){
                    return m;
                 } else {
                    count++;
                }
            }
        } else {
            System.out.println("[BUFF LOADER] Unrecognised modifier type " + type); 
        }
        return null;
    }        
    
    public static BuffLoader i() {
        return BuffLoaderHolder.INSTANCE;
    }

    private static class BuffLoaderHolder {
        private static final BuffLoader INSTANCE = new BuffLoader();
    }
    private static class Prefix implements Modifier{

        private String name;
        private int rarityValue;
        private Buff[] effects;
        
        public Prefix(String name, int value, Buff... effects){
            this.name = name;
            rarityValue = value;
            this.effects = effects;
        }
        
        @Override
        public String getName() {
            return name;
        }

        @Override
        public int getValue() {
            return rarityValue;
        }

        @Override
        public Buff[] getEffects() {
            return effects;
        }
        
    }

    private static class Suffix implements Modifier {

        private String name;
        private int rarityValue;
        private Buff[] effects;

        public Suffix(String name, int value, Buff... effects){
            this.name = name;
            rarityValue = value;
            this.effects = effects;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public int getValue() {
            return rarityValue;
        }

        @Override
        public Buff[] getEffects() {
            return effects;
        }
    }
 }
