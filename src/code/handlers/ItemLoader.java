package code.handlers;

import code.Game;
import code.actors.BasicActor;
import code.actors.Player;
import code.items.Item;
import code.items.Modifier;
import code.items.Type;
import java.awt.Point;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import org.newdawn.slick.Image;
import org.ini4j.Ini;
import org.ini4j.Profile.Section;

/**
 * @author Commander lol
 */
public class ItemLoader {

    private HashMap<String, String> itemFiles = new HashMap<>();
    private HashMap<String, HashMap<String, Item>> items = new HashMap<>();
    private Ini defFiles;
    
    private ItemLoader() {
        
        
        try{
            defFiles = new Ini(Paths.get(Game.SRC + "data/defs/itemDef.its").toFile());
            for(Section s : defFiles.values()){
                for(String e : s.values()){
                    itemFiles.putAll(FileLoader.i().getFromDirWithExt(Game.SRC + "data/", e, true));
                }
            }
        } catch (IOException ioe){
            System.err.println(ioe);
        }
        
        for(String filePath : itemFiles.values()){
            Ini curFile = null;
            
            try{
                curFile = new Ini(Paths.get(filePath).toFile());
            } catch(IOException ioe){
                System.out.println("[Item Loader] Had trouble reading from file " + filePath);
                System.err.println(ioe);
            }
            
            Section fileInfo = curFile.get("info");
            switch(fileInfo.get("type")){
                case "hat":
                    if(!items.containsKey("hat")){
                        items.put("hat", new HashMap<String, Item>());
                        System.out.println("AOSINDAOSNDOAINSD    ASODNAOSDN    CREATED HAT TYPE");
                    }
                    Section hatData = curFile.get("stats");
                    ArrayList<Modifier> prefixes = new ArrayList<>();
                    ArrayList<Modifier> suffixes = new ArrayList<>();
                    items.get("hat").put(fileInfo.get("name"), new Hat(fileInfo.get("name"), fileInfo.get("image"), Type.valueOf(hatData.get("type")), prefixes, suffixes));
                    break;
                default:
                    break;
            }
        }
    }

    
    public HashMap<String, Item> getItemsOfType(String itemType){
        return items.get(itemType);
    }
    public Item getItem(String itemType, String itemName){
        if(items.containsKey(itemType)){
            if(items.get(itemType).containsKey(itemName)){
                Item ite = new Hat((Hat)items.get(itemType).get(itemName));
                ((Hat)ite).addSuf(BuffLoader.i().getRandomBuff("Suffix"));
                return ite;
            } else {
                System.out.println("[ITEM LOADER] Could not find item " + itemName);
                return null;
            }
        } else {
            System.out.println("[ITEM LOADER] Could not find type " + itemType);
            return null;
        }
    }
    
    public static ItemLoader i() {
        return ItemLoaderHolder.INSTANCE;
    }

    private static class ItemLoaderHolder {
        private static final ItemLoader INSTANCE = new ItemLoader();
    }
    private class Hat implements Item{

        private Type type;
        private String imageName;
        private String hatName;
        private Image hatGraphic;
        private ArrayList<Modifier> prefixes;
        private ArrayList<Modifier> suffixes;
        
        
        public Hat(String name, String imageName, Type type, ArrayList<Modifier> prefixes, ArrayList<Modifier> suffixes){
            this.hatName = name;
            this.imageName = imageName;
            this.type = type;
            hatGraphic = ImageLoader.i().getImage(imageName);
            this.prefixes = prefixes;
            this.suffixes = suffixes;
        }
        public Hat(Hat master){
            type = master.getType();
            imageName = master.getImageName();
            hatName = master.getName();
            hatGraphic = master.getImage();
            prefixes = new ArrayList<>();
            suffixes = new ArrayList<>();
        }
        
        public void addPre(Modifier newPrefix){
            if(newPrefix!=null){
                prefixes.add(newPrefix);
            }
        }
        public void addSuf(Modifier newSuffix){
            if(newSuffix!=null){
                suffixes.add(newSuffix);
            }
        }
        public ArrayList<Modifier> getPrefixes(){
            return prefixes;
        }
        public ArrayList<Modifier> getSuffixes(){
            return suffixes;
        }
        @Override
        public Type getType() {
            return type;
        }

        @Override
        public boolean use() {
            Player.i().give(this);
            return true;
        }

        @Override
        public String getImageName() {
            return imageName;
        }
        
        @Override
        public String getName() {
            return hatName;
        }
        
        @Override
        public Image getImage() {
            return hatGraphic;
        }

        @Override
        public Modifier[] getBuffs() {
            Modifier[] returnArray = new Modifier[suffixes.size()+prefixes.size()];
            int count = 0;
            
            for(Modifier m : prefixes){
                returnArray[count] = m;
                count++;
            }
            for(Modifier m : suffixes){
                returnArray[count] = m;
                count++;
            }
            
            return returnArray;
        }
        
        @Override
        public boolean draw(BasicActor target) {
            hatGraphic.getSubImage(target.getFacing()*32, (target.getBodyType()+1)*32, 32, 32).draw(target.getPosition().x*32, target.getPosition().y*32);
            return true;
        }

        @Override
        public boolean draw(BasicActor target, Point offset) {
            hatGraphic.getSubImage(target.getFacing()*32, (target.getBodyType()+1)*32, 32, 32).draw((target.getPosition().x - offset.x)*32, (target.getPosition().y - offset.y)*32);
            return true;
        }

        @Override
        public boolean drawIcon(Point location) {
            hatGraphic.getSubImage(0, 0, 32, 32).draw(location.x, location.y);
            return true;
        }

        @Override
        public boolean drawIcon(Point location, Point offset) {
            hatGraphic.getSubImage(0, 0, 32, 32).draw(location.x - offset.x, location.y - offset.y);
            return true;
        }

        @Override
        public boolean drawDrop(Point location, Point offset) {
            hatGraphic.getSubImage(0, 0, 32, 32).draw((location.x - offset.x)*32, (location.y - offset.y)*32);
            return true;
        }

        @Override
        public String getPrefix() {
            StringBuilder prefix = new StringBuilder();
            
            for(int i = 0; i<prefixes.size()-1; i++){
//                prefix.append(prefixes.get(i).)
            }
            
            return prefix.toString();
        }

        @Override
        public String getSuffix() {
            StringBuilder suffix = new StringBuilder();
            
            int count = 0;
            for(Modifier m : suffixes){
                suffix.append(m.getName());
                if(count<suffixes.size()-2){
                    suffix.append(", ");
                } else if(count<suffixes.size()-1 && suffixes.size()!=1){
                    suffix.append(" and ");
                }
                count++;
            }
            
            if(suffix.length()!=0){
                suffix.insert(0, "of ");
            }
            
            return suffix.toString();
        }

        @Override
        public String getRarity() {
            int rarity = 0;
            for(Modifier m : prefixes){
                rarity += m.getValue();
            }
            for(Modifier m : suffixes){
                if(m!=null){
                    rarity += m.getValue();
                }
            }
            if(rarity>=1200){
                return "Unique";
            } else if(rarity>=800){
                return "Super Rare";
            } else if(rarity>=600){
                return "Rare";
            } else if(rarity>=400){
                return "Almost Rare";
            } else if(rarity>=200){
                return "Uncommon";
            } else {
                return "Common";
            }
        }
    }
 }
