package code.handlers;

import java.util.HashMap;

import java.io.FileNotFoundException;
import java.io.IOException;

import java.io.File;
import java.io.Serializable;

import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * A singleton class that holds, saves and loads all of the game options from 
 * a local file when the game starts up. If no file is found, it creates one
 * and it will save each option after any change. All option values are global
 * so that they can be referred to at any point in the code.
 * @author Commander lol
 */
public class Options {
    private OptionsWrapper optValues;
    /**
     * A list of the names of different controls that the user can customise 
     * in the options menu
     */
    private String[] controlIdentifiers = new String[]{ "moveUp", 
                                                        "moveDown", 
                                                        "moveLeft", 
                                                        "moveRight",
                                                        "openMap",
                                                        "openInventory",
                                                        "openMenu",
                                                        "openCharacter",
                                                        "hotkey1",
                                                        "hotkey2",
                                                        "hotkey3",
                                                        "hotkey4",
                                                        "hotkey5",
                                                        "hotkey6",
                                                        "hotkey7",
                                                        "hotkey8",
                                                        "hotkey9",
                                                        "hotkey0",
                                                        "contextAction"};
    
    /**
     * Create the Options object and attempt to load from the options file at
     * the same time
     */    
    private Options() {
        optValues = new OptionsWrapper(controlIdentifiers);
        loadFromFile(true);
    }
    /**
     * Allows access to the only available instance of the options class
     * @return The single instance of the options class
     */
    public static Options i() {
        return OptionsHolder.INSTANCE;
    }
    /**
     * Checks to see if there is a pre-existing options file and if so, loads
     * data from the file. If the file doesn't exist, then a set of default 
     * values is loaded.
     * @param createIfMissing If this is set to true, an options file will be
     * created and populated with default options if it is not found to already
     * exist
     */
    public final void loadFromFile(boolean createIfMissing){
        File optionsFile = new File("options.dat");
        ObjectInputStream optionIn;
        FileInputStream optionStream;
        if(optionsFile.exists()){
            try{
                optionStream = new FileInputStream(optionsFile);
                optionIn = new ObjectInputStream(optionStream);
            } catch(FileNotFoundException fnfe){
                System.out.println("[Options] Could not find or create an options file");
                System.err.println(fnfe);
                optValues.setDefaults();
                return;
            } catch(IOException ioe){
                System.out.println("[Options] Could not read options file properly");
                System.err.println(ioe);
                optValues.setDefaults();
                return;
            }
            try{
                optValues = (OptionsWrapper)optionIn.readObject();
            } catch(IOException | ClassNotFoundException e){
                System.out.println("[Options] Couldn't properly read options object");
                System.err.println(e);
                optValues.setDefaults();
            }
                
        } else if(createIfMissing){
            optValues.setDefaults();
            saveToFile();
        }
    }
    /**
     * Saves the current set of options to a file
     */
    public final void saveToFile(){
        File optionsFile = new File("options.dat");
        ObjectOutputStream optionOut;
        FileOutputStream optionStream;
        
        try{
            optionsFile.createNewFile();
            optionStream = new FileOutputStream(optionsFile);
            optionOut = new ObjectOutputStream(optionStream);
        } catch(FileNotFoundException fnfe){
            System.out.println("[Options] Could not find or create an options file");
            System.err.println(fnfe);
            return;
        } catch(IOException ioe){
            System.out.println("[Options] Could not write options file properly");
            System.err.println(ioe);
            return;
        }
        try{
            optionOut.writeObject(optValues);
        } catch(IOException ioe){
            System.out.println("[Options] Couldn't write to the options file");
            System.err.println(ioe);
        }
    }
    
    /**
     * Sets the volume of the music in the game. Will return a value depending
     * on the success of the operation.
     * @param newVolume The volume to attempt to set the music level to
     * @return Whether or not the music volume was successfully set to a new
     * value
     */
    public boolean musicVolume(int newVolume){
        return optValues.musicVolume(newVolume);
    }
    /**
     * Sets the volume of sound effects in the game. Will return a value 
     * depending on the success of the operation.
     * @param newVolume The volume to attempt to set the sound level to
     * @return Whether or not the sound effect volume was successfully set to 
     * a new value
     */
    public boolean soundVolume(int newVolume){
        return optValues.soundVolume(newVolume);
    }
    /**
     * Sets the value of the fullscreen flag
     * @param isFullscreen Whether or not the game should display in fullscreen
     * mode
     * @return The value of the fullscreen flag
     */
    public boolean fullscreen(boolean isFullscreen){
        return optValues.fullscreen(isFullscreen);
    }
    /**
     * @return The volume level for sound effects in-game
     */
    public int soundVolume(){return optValues.soundVolume();}
    /**
     * @return The volume level for music in-game
     */
    public int musicVolume(){return optValues.musicVolume();}
    public boolean fullscreen(){return optValues.fullscreen();}
    public int screenWidth(){return optValues.screenWidth();}
    public int screenHeight(){return optValues.screenHeight();}
    /**
     * A class that creates a private constant holding the {@link Options options}
     * class. It is accessed by the Options class to provide singleton 
     * functionality
     */
    private static class OptionsHolder {
        private static final Options INSTANCE = new Options();
    }
    private class OptionsWrapper implements Serializable{
        /**
        * The volume for the games music to play at, separate from the volume of
        * sound effects
        */
        private int musicVolume;
        /**
        * The volume for the games sound effects to play at, separate from the 
        * volume of music
        */
        private int soundVolume;
        /**
        * Whether or not to play sound effects in the game
        */
        private boolean soundOn;
        /**
        * Whether or not to play music in the game
        */
        private boolean musicOn;
        /**
        * If the game should be fullscreen or windowed
        */
        private boolean fullscreen;

        private int screenWidth;
        private int screenHeight;
        /**
        * A map containing the key value that corresponds to each control
        */
        private HashMap<String, Integer> controls;
        
        public OptionsWrapper(String[] controlSet){
            controls = new HashMap<>();
            for(String identifier : controlSet){
                controls.put(identifier, Integer.valueOf(0));
            }
            setDefaults();
        }
        
        public final void setDefaults(){
            musicVolume = 100;
            soundVolume = 100;

            soundOn = true;
            musicOn = true;

            fullscreen = true;
            
//            screenWidth = 1024;
//            screenHeight = 768;
            
            screenWidth = 1280;
            screenHeight = 720;
        }
        
        /**
         * Sets the volume of the music in the game. Will return a value depending
         * on the success of the operation.
         * @param newVolume The volume to attempt to set the music level to
         * @return Whether or not the music volume was successfully set to a new
         * value
         */
        public boolean musicVolume(int newVolume){
            if(newVolume>=0 || newVolume<=100){
                musicVolume = newVolume;
                return true;
            } else {
                return false;
            }
        }
        /**
         * Sets the volume of sound effects in the game. Will return a value 
         * depending on the success of the operation.
         * @param newVolume The volume to attempt to set the sound level to
         * @return Whether or not the sound effect volume was successfully set to 
         * a new value
         */
        public boolean soundVolume(int newVolume){
            if(newVolume>=0 || newVolume<=100){
                soundVolume = newVolume;
                return true;
            } else {
                return false;
            }
        }
        /**
         * Sets the value of the fullscreen flag
         * @param isFullscreen Whether or not the game should display in fullscreen
         * mode
         * @return The value of the fullscreen flag
         */
        public boolean fullscreen(boolean isFullscreen){
            fullscreen = isFullscreen;
            return fullscreen;
        }
        /**
         * @return The volume level for sound effects in-game
         */
        public int soundVolume(){return soundVolume;}
        /**
         * @return The volume level for music in-game
         */
        public int musicVolume(){return musicVolume;}
        public boolean fullscreen(){return fullscreen;}
        public int screenWidth(){return screenWidth;}
        public int screenHeight(){return screenHeight;}
    }
}
