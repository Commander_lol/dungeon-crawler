package code.handlers;

import code.util.FileUtil;

import java.util.HashMap;
import java.util.regex.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.nio.file.DirectoryStream;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
/**
 * A class to handle the discovery of files, providing static methods that can
 * be used in many places but only have to be updated in one
 * @author Commnder lol
 */
public class FileLoader {
    /**
     * A map that stores the results of searches, to reduce the processing 
     * overhead associated with heavy IO usage
     */
    private static HashMap<String, HashMap<String, String>>resultCache = new HashMap<>();
    
    /**
     * Recursively searches a directory for any files with a given extension and
     * adds them to a {@link HashMap HashMap} with the {@literal <K, V>} pair 
     * {@literal <Filename, File path>}
     * @param directory A string representing the path to the directory
     * @param extension the file extension, including dot, to search for. Only 
     * files of this type will be added to the return map
     * @return A {@literal <String, String>} map containing the name and 
     * location of every resource that has the given file extension
     */
    public HashMap<String, String> getFromDirWithExt(String directory, String extension, boolean useCache) throws IOException{
        if(useCache){
            if(isCached(extension, directory)){
                return getFromCache(extension, directory);
            } else {
                return getFromDirWithExt(directory, extension, false);
            }
        } else {
            String escapedExtension = FileUtil.escapeRegex(extension);
            HashMap<String, String> returnMap = new HashMap<>();
            Pattern fileExt = Pattern.compile(".+" + escapedExtension + "$");

            DirectoryStream<Path> dirStream = Files.newDirectoryStream(Paths.get(directory));
            for(Path file : dirStream){
                if(Files.isDirectory(file)){
                    System.out.println("[FILE LOADER] Searching the directory " + file.toString());
                    returnMap.putAll(getFromDirWithExt(file, extension, useCache));
                } else if(fileExt.matcher(file.getFileName().toString()).matches()){
                    String fileName = file.getFileName().toString().replaceAll(escapedExtension, "");
                    returnMap.put(fileName, file.toString());
                    System.out.println("[FILE LOADER] Added reference for new file: " + fileName + ", " + file.toString());
                }
            }
            cacheResult(extension, directory.toString(), returnMap);
            return returnMap;
        }
    }
    /**
     * Recursively searches a directory for any files with a given extension and
     * adds them to a {@link HashMap HashMap} with the {@literal <K, V>} pair 
     * {@literal <Filename, File path>}
     * @param directory A path object holding the path to the directory
     * @param extension the file extension, including dot, to search for. Only 
     * files of this type will be added to the return map
     * @return A {@literal <String, String>} map containing the name and 
     * location of every resource that has the given file extension
     */
    public HashMap<String, String> getFromDirWithExt(Path directory, String extension, boolean useCache) throws IOException{
        if(useCache){
            if(isCached(extension, directory.toString())){
                return getFromCache(extension, directory.toString());
            } else {
                return getFromDirWithExt(directory, extension, false);
            }
        } else {
            String escapedExtension = FileUtil.escapeRegex(extension);
            HashMap<String, String> returnMap = new HashMap<>();
            Pattern fileExt = Pattern.compile(".+" + escapedExtension + "$");

            DirectoryStream<Path> dirStream = Files.newDirectoryStream(directory);
            for(Path file : dirStream){
                if(Files.isDirectory(file)){
                    System.out.println("[FILE LOADER] Searching the directory " + file.toString());
                    returnMap.putAll(getFromDirWithExt(file, extension, useCache));
                } else if(fileExt.matcher(file.getFileName().toString()).matches()){
                    String fileName = file.getFileName().toString().replaceAll(escapedExtension, "");
                    returnMap.put(fileName, file.toString());
                    System.out.println("[FILE LOADER] Added reference for new file: " + fileName + ", " + file.toString());
                }
            }
            cacheResult(extension, directory.toString(), returnMap);
            return returnMap;
        }
    }
    
    public HashMap<String, String> getFromDirWithPre(String directory, String prefix, boolean useCache) throws IOException{
        String prefixDot = prefix + ".";
        
        if(useCache){
            if(isCached(prefixDot, directory)){
                return getFromCache(prefixDot, directory);
            } else {
                return getFromDirWithPre(directory, prefixDot, false);
            }
        } else {
            String escapedExtension = FileUtil.escapeRegex(prefixDot);
            HashMap<String, String> returnMap = new HashMap<>();
            Pattern fileExt = Pattern.compile("^" + escapedExtension + ".+");

            DirectoryStream<Path> dirStream = Files.newDirectoryStream(Paths.get(directory));
            for(Path file : dirStream){
                if(Files.isDirectory(file)){
                    System.out.println("[FILE LOADER] Searching the directory " + file.toString());
                    returnMap.putAll(getFromDirWithPre(file, prefix, useCache));
                } else if(fileExt.matcher(file.getFileName().toString()).matches()){
                    String fileName = file.getFileName().toString().replaceAll(file.getFileName().toString().substring(file.getFileName().toString().lastIndexOf("."), file.getFileName().toString().length()-1), "");
                    returnMap.put(fileName, file.toString());
                    System.out.println("[FILE LOADER] Added reference for new file: " + fileName + ", " + file.toString());
                }
            }
            cacheResult(prefixDot, directory.toString(), returnMap);
            return returnMap;
        }
    }
    public HashMap<String, String> getFromDirWithPre(Path directory, String prefix, boolean useCache) throws IOException{
        String prefixDot = prefix + ".";
        
        if(useCache){
            if(isCached(prefixDot, directory.toString())){
                return getFromCache(prefixDot, directory.toString());
            } else {
                return getFromDirWithExt(directory, prefixDot, false);
            }
        } else {
            String escapedExtension = FileUtil.escapeRegex(prefixDot);
            HashMap<String, String> returnMap = new HashMap<>();
            Pattern fileExt = Pattern.compile("^" + escapedExtension + ".+");

            DirectoryStream<Path> dirStream = Files.newDirectoryStream(directory);
            for(Path file : dirStream){
                if(Files.isDirectory(file)){
                    System.out.println("[FILE LOADER] Searching the directory " + file.toString());
                    returnMap.putAll(getFromDirWithPre(file, prefix, useCache));
                } else if(fileExt.matcher(file.getFileName().toString()).matches()){
                    String fileName = file.getFileName().toString().replaceAll(file.getFileName().toString().substring(file.getFileName().toString().lastIndexOf("."), file.getFileName().toString().length()-1), "");
                    returnMap.put(fileName, file.toString());
                    System.out.println("[FILE LOADER] Added reference for new file: " + fileName + ", " + file.toString());
                }
            }
            cacheResult(prefixDot, directory.toString(), returnMap);
            return returnMap;
        }
    }
    
    private void cacheResult(String extension, String dir, HashMap<String, String> result){
        String queryHash = getHashOf(extension, dir);
        resultCache.put(queryHash, result);
    }
    private boolean isCached(String extension, String dir){
        String queryHash = getHashOf(extension, dir);
        return resultCache.containsKey(queryHash);
    }
    private HashMap<String, String> getFromCache(String extension, String dir){
        String queryHash = getHashOf(extension, dir);
        return resultCache.get(queryHash);
    }
    /**
     * Gets a string representation of the MD5 hash of the provided strings. 
     * Encoding is assumed to be UTF-8
     * @param strings A series of strings that will be concatenated before being
     * hashed. The function does not add spaces, all strings are used as 
     * provided.
     * @return The MD5 hash of the provided strings, converted from a byte array
     * to a hex string
     */
    private String getHashOf(String... strings){
        StringBuilder hashable = new StringBuilder();
        StringBuilder hexString = new StringBuilder();
        
        MessageDigest md = null;
        byte[] hash = null;
        
        for(int i = 0; i<strings.length; i++){
            hashable.append(strings[i]);
        }
        
        try{
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException nsae){
            System.err.println(nsae);
        }
        try{
            hash = md.digest(hashable.toString().getBytes("UTF-8"));
        } catch(UnsupportedEncodingException uee){
            System.err.println(uee);
        }
        
        for (int i = 0; i < hash.length; i++) {
            if ((0xff & hash[i]) < 0x10) {
                hexString.append("0");
            }
            hexString.append(Integer.toHexString(0xFF & hash[i]));
        }
        
        return hexString.toString();
    }
    /**
     * Gets the singleton instance of the FileLoader
     * @return The FileLoader object
     */
    public static FileLoader i(){
        return FileLoaderHolder.INSTANCE;
    }
    private static class FileLoaderHolder {
        private static final FileLoader INSTANCE = new FileLoader();
    }
}
